package fr.compil.jej;

import fr.compil.jej.core.PetitJavaEnvironment;
import fr.compil.jej.core.StreamChars;
import fr.compil.jej.core.Token;
import fr.jamailun.consoleargs.JamConsoleArgs;
import fr.jamailun.consoleargs.JamConsoleArgsBuilder;
import fr.jamailun.consoleargs.WrongArgumentException;
import fr.jamailun.jamlogger.JamLogger;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class Main {

    private static final String DEFAULT_ARGS = "test.pj -debug-out -debug-classes";
    private static final String HELP_MESSAGE = " * Transpilation tool from 'petit-java' to C *" +
            "\n * Created in 2022 by Timothé ROSAZ and Alexandre SABADDIN *\n" +
            "\nParameters : <input> [args]" +
            "\nArguments :" +
            "\n\t- \"-o <output>\" : set the output file. If not given, will use the input and use a '.c' copy of it." +
            "\n\t- \"-debug-out\" : print the output in the console" +
            "\n\t- \"-debug-classes\" : print the classes in the console" +
            "\n\t- \"-silent\" : Remove all outputs" +
            "\n\n * The program will now terminate *";
    
    boolean apply(String input) {
        // Set output if not defined
        if(output == null) output = input.split("\\.")[0] + ".c";
        
        // TOKENIZATION
        JamLogger.log("Tokenization started.");
        List<Token> tokens = Tokenizer.tokenize(new StreamChars(new File(input)));
        if(tokens.isEmpty())
            return false;
        JamLogger.info("Tokenization is successful. Loaded " + tokens.size() + " tokens.");
    
        // PARSING
        JamLogger.log("Parsing started.");
        PetitJavaEnvironment env = Parser.parse(tokens);
        if(env.isEmpty())
            return false;
        JamLogger.info("Parsing is successful. Parsed " + env.size() + " classes and interfaces.");
    
        // TYPE CHECKING
        JamLogger.log("Type checking started.");
        if(!TypeValidation.validate(env)) {
            JamLogger.error("Error during type checking. Please, check errors below.");
            return false;
        }
        JamLogger.info("Type checking is successful.");
    
        if(debugClasses) {
            System.out.println();
            JamLogger.info("CLASSES et INTERFACES typées :");
            env.getClasses().forEach(n -> JamLogger.info("c-> " + n));
            env.getInterfaces().forEach(n -> JamLogger.info("i-> " + n));
        }
    
        //TRANSPILING
        JamLogger.log("Transpilation started.");
        try {
            Transcompiler.transcompile(env, output, debugOut);
        } catch(IOException e) {
            JamLogger.error("Error during transpilation : " + e.getMessage());
            return false;
        }
        JamLogger.success("Successfully transpiled '" + input + "' to '" + output + "'.");
        return true;
    }
    
    public static void main(String[] args) {
        Main main = new Main();
        JamConsoleArgs jamConsole = new JamConsoleArgsBuilder()
                .setHelpMessage(HELP_MESSAGE)
                .addOptionalParametrable("o", main::setOutput)
                .addOptional("debug-out", main::toggleDebugOut)
                .addOptional("debug-classes", main::toggleDebugClasses)
                .addOptional("silent", main::toggleSilent)
                .build();
        
        List<String> inputs;
        try {
            inputs = jamConsole.accept(args.length > 0 ? args : DEFAULT_ARGS.split(" "));
            if(jamConsole.hasUsedHelp()) {
                System.exit(0);
                return;
            }
        } catch (WrongArgumentException e) {
            JamLogger.error("Error with those arguments : " + e.getMessage());
            System.exit(-1);
            return;
        }
        
        if(inputs.isEmpty()) {
            JamLogger.error("Error, please specify an input file.");
            System.exit(-1);
            return;
        }
        
        if(!main.apply(inputs.get(0)))
            System.exit(-2);
    }
    
    // Main stuff
    private String output = null;
    void setOutput(String output) {
        this.output = output;
    }
    private boolean debugOut = false;
    void toggleDebugOut() {
        debugOut = !debugOut;
    }
    private boolean debugClasses = false;
    void toggleDebugClasses() {
        debugClasses = !debugClasses;
    }
    void toggleSilent() {
        JamLogger.silent();
    }

}
