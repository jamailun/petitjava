package fr.compil.jej.std;

import fr.compil.jej.compilers.Visiteur;
import fr.compil.jej.core.TokenType;
import fr.compil.jej.core.Type;
import fr.compil.jej.core.Visibility;
import fr.compil.jej.nodes.MethodNode;
import fr.compil.jej.nodes.Variable;
import fr.compil.jej.nodes.expressions.ExpressionNode;

import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import static java.util.Map.entry;

public final class FakeStringMethods {
	private FakeStringMethods() {}
	
	public final static Map<String, MethodNode> STR_FAKE_METHS = Map.ofEntries(
			entry("length", new FakeStringLength()),
			entry("substring", new FakeStringSubstring())
	);
	
	public static class FakeStringLength extends MethodNode implements FakeMethod {
		public FakeStringLength() {
			super(Visibility.PUBLIC, false, Type.Primitive.INT.asType(), "length", STD.FAKE_METHOD_OWNER);
			super.signature.setFake(this);
		}
		@Override
		public void acceptCustom(ExpressionNode called, List<ExpressionNode> params, Consumer<String> print, Visiteur v) {
			called.accept(v);
			print.accept("->length");
		}
	}
	
	public static class FakeStringSubstring extends MethodNode implements FakeMethod {
		public FakeStringSubstring() {
			super(Visibility.PUBLIC, false, Type.Primitive.STRING.asType(), "substring", STD.FAKE_METHOD_OWNER);
			super.signature.addArguments(List.of(
					new Variable(Type.Primitive.INT.asType(), "from"),
					new Variable(Type.Primitive.INT.asType(), "to")
			));
			super.signature.setFake(this);
		}
		@Override
		public void acceptCustom(ExpressionNode called, List<ExpressionNode> params, Consumer<String> print, Visiteur v) {
			print.accept("String_substring_fromto(");
			called.accept(v);
			print.accept(",");
			params.get(0).accept(v);
			print.accept(",");
			params.get(1).accept(v);
			print.accept(")");
		}
	}
}
