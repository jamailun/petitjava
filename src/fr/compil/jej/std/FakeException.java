package fr.compil.jej.std;

import fr.compil.jej.compilers.NativeHandler;
import fr.compil.jej.compilers.Visiteur;
import fr.compil.jej.core.Type;
import fr.compil.jej.core.Visibility;
import fr.compil.jej.nodes.ClassNode;
import fr.compil.jej.nodes.ConstructorNode;
import fr.compil.jej.nodes.MethodNode;
import fr.compil.jej.nodes.Variable;
import fr.compil.jej.nodes.expressions.ExpressionNode;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;

public class FakeException extends ClassNode {
	
	public static final FakeException FAKE_EXCEPTION = new FakeException();
	
	public FakeException() {
		super(Visibility.PUBLIC, "Exception", FakeObject.FAKE_OBJECT, true);
		super.setFieldsAndMethods(List.of(), List.of(
				new GetMessage(this),
				new EmptyConstructor(this),
				new MessageConstructor(this)
		));
	}
	
	static class EmptyConstructor extends ConstructorNode implements FakeMethod {
		public EmptyConstructor(ClassNode owner) {
			super(Visibility.PUBLIC, Collections.emptyList(), Collections.emptyList(), owner);
			super.signature.setFake(this);
		}
		@Override
		public void acceptCustom(ExpressionNode called, List<ExpressionNode> params, Consumer<String> printMethod, Visiteur v) {}
	}
	static class MessageConstructor extends ConstructorNode implements FakeMethod {
		public MessageConstructor(ClassNode owner) {
			super(Visibility.PUBLIC, Collections.singletonList(new Variable(Type.Primitive.STRING.asType(), "msg")), Collections.emptyList(),  owner);
			super.signature.setFake(this);
		}
		@Override
		public void acceptCustom(ExpressionNode called, List<ExpressionNode> params, Consumer<String> printMethod, Visiteur v) {
			printMethod.accept(NativeHandler.NEXCEP_GLOBAL_ERROR + " = ");
			params.get(0).accept(v);
			printMethod.accept(";\t");
		}
	}
	
	static class GetMessage extends MethodNode implements FakeMethod {
		
		GetMessage(ClassNode owner) {
			super(Visibility.PUBLIC, false, Type.Primitive.STRING.asType(), "getMessage", owner);
			super.signature.setFake(this);
		}
		
		@Override
		public String cName() {
			return "getMessage";
		}
		
		@Override
		public void acceptCustom(ExpressionNode called, List<ExpressionNode> params, Consumer<String> printMethod, Visiteur v) {
			printMethod.accept(NativeHandler.NEXCEP_GLOBAL_ERROR);
		}
	}
	
}
