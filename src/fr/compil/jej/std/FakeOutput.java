package fr.compil.jej.std;

import fr.compil.jej.compilers.Visiteur;
import fr.compil.jej.core.Type;
import fr.compil.jej.core.Visibility;
import fr.compil.jej.nodes.ClassNode;
import fr.compil.jej.nodes.MethodNode;
import fr.compil.jej.nodes.Variable;
import fr.compil.jej.nodes.expressions.ExpressionNode;

import java.util.List;
import java.util.function.Consumer;

public class FakeOutput extends ClassNode {
	
	public static final FakeOutput FAKE_OUTPUT = new FakeOutput("printf(");
	public static final FakeOutput FAKE_ERRPUT = new FakeOutput("fprintf(stderr,");
	
	private final String cFunc;
	
	public FakeOutput(String cFunc) {
		super(Visibility.PUBLIC, "Outstream", FakeObject.FAKE_OBJECT, true);
		this.cFunc = cFunc;
		super.setFieldsAndMethods(List.of(), List.of(
				new PrintlnMethodString(this),
				new PrintlnMethodInt(this),
				new PrintlnMethodDouble(this),
				new PrintlnMethodBoolean(this),
				new PrintlnMethodChar(this),

				new PrintMethodString(this),
				new PrintMethodInt(this),
				new PrintMethodDouble(this),
				new PrintMethodBoolean(this),
				new PrintMethodChar(this)
		));
	}
	
	private static abstract class AbstractPrintlnMethod extends MethodNode implements FakeMethod {
		private final String formatter;
		private final String cFunc;
		public AbstractPrintlnMethod(FakeOutput owner, Type inputType, String formatter) {
			super(Visibility.PUBLIC, false, Type.TYPE_VOID, "println", owner);
			cFunc = owner.cFunc;
			super.signature.setFake(this);
			this.formatter = formatter;
			super.signature.addArgument(new Variable(inputType, "input"));
		}
		@Override
		public void acceptCustom(ExpressionNode called, List<ExpressionNode> params, Consumer<String> printMethod, Visiteur v) {
			printMethod.accept(cFunc + "\"" + formatter + "\\n\", ");
			params.get(0).accept(v);
			if(formatter.equals("%s"))
				printMethod.accept("->chars");
			printMethod.accept(")");
		}
	}
	
	private static class PrintlnMethodString extends AbstractPrintlnMethod {
		public PrintlnMethodString(FakeOutput owner) {
			super(owner, Type.Primitive.STRING.asType(), "%s");
		}
	}
	private static class PrintlnMethodInt extends AbstractPrintlnMethod {
		public PrintlnMethodInt(FakeOutput owner) {
			super(owner, Type.Primitive.INT.asType(), "%d");
		}
	}
	private static class PrintlnMethodDouble extends AbstractPrintlnMethod {
		public PrintlnMethodDouble(FakeOutput owner) {
			super(owner, Type.Primitive.DOUBLE.asType(), "%f");
		}
	}
	private static class PrintlnMethodBoolean extends AbstractPrintlnMethod {
		public PrintlnMethodBoolean(FakeOutput owner) {
			super(owner, Type.Primitive.BOOLEAN.asType(), "%d");
		}
	}
	private static class PrintlnMethodChar extends AbstractPrintlnMethod {
		public PrintlnMethodChar(FakeOutput owner) {
			super(owner, Type.Primitive.CHAR.asType(), "%c");
		}
	}
	
	// PRINT
	
	private static abstract class AbstractPrintMethod extends MethodNode implements FakeMethod {
		private final String formatter;
		private final String cFunc;
		public AbstractPrintMethod(FakeOutput owner, Type inputType, String formatter) {
			super(Visibility.PUBLIC, false, Type.TYPE_VOID, "print", owner);
			cFunc = owner.cFunc;
			super.signature.setFake(this);
			this.formatter = formatter;
			super.signature.addArgument(new Variable(inputType, "input"));
		}
		@Override
		public void acceptCustom(ExpressionNode called, List<ExpressionNode> params, Consumer<String> printMethod, Visiteur v) {
			printMethod.accept(cFunc + "\"" + formatter + "\", ");
			params.get(0).accept(v);
			if(formatter.equals("%s"))
				printMethod.accept("->chars");
			printMethod.accept(")");
		}
	}
	
	private static class PrintMethodString extends AbstractPrintMethod {
		public PrintMethodString(FakeOutput owner) {
			super(owner, Type.Primitive.STRING.asType(), "%s");
		}
	}
	private static class PrintMethodInt extends AbstractPrintMethod {
		public PrintMethodInt(FakeOutput owner) {
			super(owner, Type.Primitive.INT.asType(), "%d");
		}
	}
	private static class PrintMethodDouble extends AbstractPrintMethod {
		public PrintMethodDouble(FakeOutput owner) {
			super(owner, Type.Primitive.DOUBLE.asType(), "%f");
		}
	}
	private static class PrintMethodBoolean extends AbstractPrintMethod {
		public PrintMethodBoolean(FakeOutput owner) {
			super(owner, Type.Primitive.BOOLEAN.asType(), "%d");
		}
	}
	private static class PrintMethodChar extends AbstractPrintMethod {
		public PrintMethodChar(FakeOutput owner) {
			super(owner, Type.Primitive.CHAR.asType(), "%c");
		}
	}
	
}
