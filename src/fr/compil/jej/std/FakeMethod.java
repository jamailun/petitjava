package fr.compil.jej.std;

import fr.compil.jej.compilers.Visiteur;
import fr.compil.jej.nodes.expressions.ExpressionNode;
import java.util.List;
import java.util.function.Consumer;

/**
 * Interface used by fake-method to handle their behaviour.
 */
public interface FakeMethod {
    
    /**
     * Write the behaviour in C of the fake-method.
     * @param called the expression node owning the method.
     * @param params the parameters passed to the fake method.
     * @param printMethod a Visiteur-method used to print in a outstream.
     * @param visitor the Visiteur to be able to handle the parameters.
     */
    void acceptCustom(ExpressionNode called, List<ExpressionNode> params, Consumer<String> printMethod, Visiteur visitor);
}
