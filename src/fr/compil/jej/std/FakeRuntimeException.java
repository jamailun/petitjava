package fr.compil.jej.std;

import fr.compil.jej.core.Visibility;
import fr.compil.jej.nodes.ClassNode;

import java.util.List;

public class FakeRuntimeException extends ClassNode {
	
	public static final FakeRuntimeException FAKE_RUNTIME_EXCEPTION = new FakeRuntimeException();
	
	public FakeRuntimeException() {
		super(Visibility.PUBLIC, "RuntimeException", FakeException.FAKE_EXCEPTION, true);
		super.setFieldsAndMethods(List.of(), List.of(
				new FakeException.GetMessage(this),
				new FakeException.EmptyConstructor(this),
				new FakeException.MessageConstructor(this)
		));
	}
	
}
