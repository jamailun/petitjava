package fr.compil.jej.std;

import fr.compil.jej.compilers.Visiteur;
import fr.compil.jej.core.MethodSignature;
import fr.compil.jej.core.Typable;
import fr.compil.jej.core.TypesContext;
import fr.compil.jej.core.Visibility;
import fr.compil.jej.nodes.ClassNode;
import fr.compil.jej.nodes.MethodsOwner;
import fr.jamailun.jamlogger.JamLogger;

import java.util.List;

/**
 * Classe statique simulant le comportement de la STD de java.
 * Pour la plupart de ces classes, il sera utilisé des méthodes du C et de ses librairies usuelles.
 */
public final class STD {
	private STD() {}
	
	public final static List<ClassNode> FAKE_CLASSES = List.of(
			FakeSystem.FAKE_SYSTEM,
			FakeOutput.FAKE_OUTPUT,
			FakeOutput.FAKE_ERRPUT,
			FakeInput.FAKE_INPUT,
			FakeObject.FAKE_OBJECT,
			FakeException.FAKE_EXCEPTION,
			FakeRuntimeException.FAKE_RUNTIME_EXCEPTION
	);
	
	/**
	 * Charger les classes de la STD
	 * @param context contexte où ajouter ces méthodes
	 */
	public static void load(TypesContext context) {
		context.registerClasses(FAKE_CLASSES);
	}
	
	final static ClassNode FAKE_METHOD_OWNER = new FakeMethodOwner();
	private final static class FakeMethodOwner extends ClassNode {
		public FakeMethodOwner() {
			super(Visibility.PUBLIC, "NULL", FakeObject.FAKE_OBJECT, true);
		}
		
		@Override
		public boolean isSubtype(MethodsOwner parent) {
			return false;
		}
		
		@Override
		public MethodSignature getMethod(String name, List<? extends Typable> params) {
			return null;
		}
		
		@Override
		public void accept(Visiteur visiteur) {
			JamLogger.warning("accept fake method owner ?");
		}
	}
}
