package fr.compil.jej.std;
import fr.compil.jej.core.Visibility;
import fr.compil.jej.nodes.ClassNode;

import java.util.List;

public class FakeObject extends ClassNode {
	
	public static final FakeObject FAKE_OBJECT = new FakeObject();
	
	public FakeObject() {
		super(Visibility.PUBLIC, "Object", (ClassNode) null, true);
		super.setFieldsAndMethods(List.of(), List.of());
	}
	
}
