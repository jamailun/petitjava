package fr.compil.jej.std;

import fr.compil.jej.compilers.Visiteur;
import fr.compil.jej.core.Type;
import fr.compil.jej.core.Visibility;
import fr.compil.jej.nodes.ClassNode;
import fr.compil.jej.nodes.FieldNode;
import fr.compil.jej.nodes.MethodNode;
import fr.compil.jej.nodes.Variable;
import fr.compil.jej.nodes.expressions.ExpressionNode;

import java.util.List;
import java.util.function.Consumer;

public class FakeSystem extends ClassNode {
	
	public final static FakeSystem FAKE_SYSTEM = new FakeSystem();
	
	public FakeSystem() {
		super(Visibility.PUBLIC, "System", FakeObject.FAKE_OBJECT, true);
		super.setFieldsAndMethods(List.of(
				new FieldNode(Visibility.PUBLIC, true, true, new Type(FakeOutput.FAKE_OUTPUT), "out", this),
				new FieldNode(Visibility.PUBLIC, true, true, new Type(FakeOutput.FAKE_ERRPUT), "err", this),
				new FieldNode(Visibility.PUBLIC, true, true, new Type(FakeInput.FAKE_INPUT), "in", this)
		), List.of(
				new ExitMethod(this)
		));
	}
	
	private static class ExitMethod extends MethodNode implements FakeMethod {
		ExitMethod(ClassNode owner) {
			super(Visibility.PUBLIC, true, Type.TYPE_VOID, "exit", owner);
			super.signature.setFake(this);
			super.signature.addArgument(new Variable(Type.Primitive.INT.asType(), "code"));
		}

		@Override
		public String cName() {
			return "exit";
		}

		@Override
		public void acceptCustom(ExpressionNode called, List<ExpressionNode> params, Consumer<String> printMethod, Visiteur v) {
			printMethod.accept("exit(");
			params.get(0).accept(v);
			printMethod.accept(")");
		}
	}
	
}
