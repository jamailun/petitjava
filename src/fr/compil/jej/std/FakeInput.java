package fr.compil.jej.std;

import fr.compil.jej.compilers.Visiteur;
import fr.compil.jej.core.Type;
import fr.compil.jej.core.Visibility;
import fr.compil.jej.nodes.ClassNode;
import fr.compil.jej.nodes.MethodNode;
import fr.compil.jej.nodes.expressions.ExpressionNode;

import java.util.List;
import java.util.function.Consumer;

public class FakeInput extends ClassNode {
	
	private static final String SCANNER = "STDIN_SCANNER";
	public static final String STD_IN_INIT_C = SCANNER + "=(Scanner*)malloc(sizeof(Scanner));";
	
	public static final FakeInput FAKE_INPUT = new FakeInput();
	
	public FakeInput() {
		super(Visibility.PUBLIC, "InputStream", FakeObject.FAKE_OBJECT, true);
		super.setFieldsAndMethods(List.of(), List.of(
				new HasNextLineMethod(this),
				new NextLineMethod(this),
				new HasNextIntMethod(this),
				new NextIntMethod(this)
		));
	}
	
	private static abstract class SimpleMethod extends MethodNode implements FakeMethod {
		private final String cName;
		public SimpleMethod(FakeInput owner, Type.Primitive output, String javaName, String cName) {
			super(Visibility.PUBLIC, false, output.asType(), javaName, owner);
			super.signature.setFake(this);
			this.cName = cName;
		}
		@Override
		public void acceptCustom(ExpressionNode called, List<ExpressionNode> params, Consumer<String> printMethod, Visiteur v) {
			printMethod.accept(cName + "(" + SCANNER + ")");
		}
	}
	
	private static class HasNextLineMethod extends SimpleMethod {
		public HasNextLineMethod(FakeInput owner) {
			super(owner, Type.Primitive.BOOLEAN, "hasNextLine", "Scanner_has_next_line");
		}
	}
	
	private static class NextLineMethod extends SimpleMethod {
		public NextLineMethod(FakeInput owner) {
			super(owner, Type.Primitive.STRING, "nextLine", "Scanner_next_line");
		}
	}
	
	private static class HasNextIntMethod extends SimpleMethod {
		public HasNextIntMethod(FakeInput owner) {
			super(owner, Type.Primitive.BOOLEAN, "hasNextInt", "Scanner_has_next_int");
		}
	}
	
	private static class NextIntMethod extends SimpleMethod {
		public NextIntMethod(FakeInput owner) {
			super(owner, Type.Primitive.INT, "nextInt", "Scanner_next_int");
		}
	}
	
}
