package fr.compil.jej.std;

import fr.compil.jej.compilers.Visiteur;
import fr.compil.jej.core.ArrayType;
import fr.compil.jej.core.Type;
import fr.compil.jej.core.Visibility;
import fr.compil.jej.nodes.FieldNode;
import fr.compil.jej.nodes.expressions.ExpressionNode;

import java.util.List;
import java.util.function.Consumer;

public class FakeLengthArray extends FieldNode implements FakeMethod {
	
	public static final String FIELD_NAME = "length";
	
	private final ArrayType owner;
	
	public FakeLengthArray(ArrayType owner) {
		super(Visibility.PUBLIC, true, false, Type.Primitive.INT.asType(), FIELD_NAME, owner.getLinkedClass());
		this.owner = owner;
	}
	
	public ExpressionNode get() {
		return owner.getLength().get(0);
	}

	@Override
	public void acceptCustom(ExpressionNode called, List<ExpressionNode> params, Consumer<String> print, Visiteur v) {
		// called->length
		called.accept(v);
		print.accept("->length");
	}
}
