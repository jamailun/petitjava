package fr.compil.jej.core;

public interface Printable {

	String niceString(int indent);
	
}
