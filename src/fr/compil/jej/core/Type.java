package fr.compil.jej.core;

import fr.compil.jej.compilers.NativeHandler;
import fr.compil.jej.compilers.VisiteurC;
import fr.compil.jej.errors.BadTokenError;
import fr.compil.jej.nodes.ClassNode;
import fr.compil.jej.nodes.MethodsOwner;
import fr.compil.jej.std.FakeException;
import fr.jamailun.jamlogger.JamLogger;

import java.util.Arrays;
import java.util.Locale;
import java.util.Objects;

public class Type {
	
	public final static Type TYPE_VOID = new Type("void");
	
	public enum Primitive {
		INT(TokenType.TYPE_INT, true, NativeHandler.NSTRING_TOSTRING_INT),
		DOUBLE(TokenType.TYPE_DOUBLE, true, NativeHandler.NSTRING_TOSTRING_DOUBLE),
		STRING(TokenType.TYPE_STRING, false, NativeHandler.NSTRING_NEWSTRING),
		CHAR(TokenType.TYPE_CHAR, false, NativeHandler.NSTRING_TOSTRING_CHAR),
		BOOLEAN(TokenType.TYPE_BOOLEAN, false, NativeHandler.NSTRING_TOSTRING_BOOL);
		private final TokenType link;
		private final boolean numeric;
		public final String toStringMethod;
		Primitive(TokenType type, boolean numeric, String toStringMethod) {
			link = type;
			this.numeric = numeric;
			this.toStringMethod = toStringMethod;
		}
		public boolean isNumeric() {
			return numeric;
		}
		public static Primitive from(Token token) {
			return Arrays.stream(values()).filter(p -> p.link == token.getType()).findFirst().orElse(null);
		}
		public Type asType() {
			return new Type(this);
		}
		public String cName() {
			if(this == STRING)
				return NativeHandler.NSTRING_STRING;
			return name().toLowerCase(Locale.ROOT);
		}
	}

	public Type(Primitive p) {
		primitive = true;
		primitiveType = p;
	}
	public Type(String typeName) {
		primitive = false;
		className = typeName;
	}
	public Type(ClassNode classNode) {
		this(classNode.getName());
		linkClass(classNode);
	}
	public Type(Type clone) {
		this.primitive = clone.primitive;
		this.primitiveType = clone.primitiveType;
		this.className = clone.className;
		this.linkedClass = clone.linkedClass;
		this.isFinal = clone.isFinal;
	}

	public ArrayType toArray() {
		return new ArrayType(this);
	}

	public Type toNotArray() {
		throw new RuntimeException("Type " + this + " is NOT an array : cannot call '#toNotArray()'.");
	}

	private boolean isFinal = false;
	private final boolean primitive;
	private Primitive primitiveType;
	
	private String className;
	private MethodsOwner linkedClass;
	
	public boolean isArray() {
		return false;
	}
	
	public boolean shouldBeLinked() {
		return !isPrimitive() && this != TYPE_VOID && getLinkedMethodsOwner() == null;
	}

	public boolean isFinal() {
		return isFinal;
	}

	public void setFinal() {
		isFinal = true;
	}

	public static Type generate(Token token) throws BadTokenError {
		if(token.getType() == TokenType.TYPE_VOID)
			return Type.TYPE_VOID;
		Primitive p = Primitive.from(token);
		if(p != null) // Type primitif
			return new Type(p);
		if(token.getType() == TokenType.IDENTIFIER)
			return new Type(token.getValString());
		throw new BadTokenError(token, "Expected token IDENTIFIER or TYPE_X to a type.");
	}
	
	public MethodsOwner getLinkedMethodsOwner() {
		return linkedClass;
	}
	public ClassNode getLinkedClass() {
		if(linkedClass instanceof ClassNode)
			return (ClassNode) linkedClass;
		return null;
	}
	
	public void linkClass(MethodsOwner classNode) {
		if(isPrimitive())
			throw new RuntimeException("Cannot link a class to a primitive (" + this + ")");
		if(linkedClass != null)
			throw new RuntimeException("Link to a class already exists (" + this + ")");
		linkedClass = classNode;
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Type type = (Type) o;
		if(primitive != type.primitive)
			return false;
		if(isArray() != type.isArray())
			return false;
		if(linkedClass != null && type.linkedClass != null)
			return linkedClass.isSubtype(type.linkedClass);
		return (primitive && primitiveType == type.primitiveType) || (!primitive && Objects.equals(className, type.className));
	}
	
	public boolean isPrimitive() {
		return primitive;
	}
	
	public Primitive getPrimitiveType() {
		return primitiveType;
	}
	
	public String getClassName() {
		return className;
	}

	public String cName() {
		return isPrimitive() ? primitiveType.cName() : getClassName();
	}
	
	public String cType() {
		return isArray() ? VisiteurC.ARRAY + "*" : cName() + (isPrimitive() ? "" : "*");
	}

	public boolean isValidExceptionType() {
		if(!getLinkedMethodsOwner().isSubtype(FakeException.FAKE_EXCEPTION)) {
			JamLogger.error("Type " + this + " should inherits of an error, but it's not the case.");
			return false;
		}
		return true;
	}
	
	@Override
	public String toString() {
		if(this == TYPE_VOID)
			return "VOID";
		if(primitive)
			return (isFinal?"FINAL_":"") + primitiveType.name() + (isArray()?"[]":"");
		if(linkedClass == null)
			return "Type{"+(isFinal?"FINAL ":"")+"className='" + className + "'"+ (isArray()?"[]":"")+"}";
		return "Type{"+(isFinal?"FINAL ":"")+"class_linked=" + linkedClass.getName() + (isArray()?"[]":"") + "}";
	}
}
