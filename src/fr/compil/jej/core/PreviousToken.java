package fr.compil.jej.core;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This annotation is used during parsing. Please, note that it won't do anything.
 * It's only meant to help us during the project development, knowing what a parsing-method requires to be called.
 */
@Retention(RetentionPolicy.SOURCE)
@Target({ElementType.METHOD})
public @interface PreviousToken {

	TokenType[] allowed() default {};

}
