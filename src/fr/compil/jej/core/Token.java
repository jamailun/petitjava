package fr.compil.jej.core;

/**
 * A token represents a symbol.
 * It can contain a value, as a String or any primitive.
 * @see TokenType
 * @see StreamTokens
 */
public class Token {

    private final TokenType type;

    private String valString;
    private int valInt;
    private double valDouble;
    
    private final int linePos, charPos;

    public Token(DocumentPosition pos, TokenType type) {
        this.type = type;
        linePos = pos.getLineNumber(); charPos = pos.getCharPos();
    }
    public Token(DocumentPosition pos, TokenType type, String val) {
        this.type = type;
        this.valString = val;
        linePos = pos.getLineNumber(); charPos = pos.getCharPos();
    }
    public Token(DocumentPosition pos, TokenType type, int val) {
        this.type = type;
        this.valInt = val;
        linePos = pos.getLineNumber(); charPos = pos.getCharPos();
    }
    public Token(DocumentPosition pos, TokenType type, double val) {
        this.type = type;
        this.valDouble = val;
        linePos = pos.getLineNumber(); charPos = pos.getCharPos();
    }
    public Token(Token clone) {
        this.type = clone.type;
        this.valInt = clone.valInt;
        this.valDouble = clone.valDouble;
        this.valString = clone.valString;
        this.linePos = clone.linePos; charPos = clone.charPos;
    }
    
    /**
     * Get the type of the token
     * @return the TokenType corresponding to the token
     */
    public TokenType getType() {
        return type;
    }
    
    /**
     * Get the string value corresponding to the token
     * @return null if the token does not contain a string.
     */
    public String getValString() {
        return valString;
    }
    
    /**
     * Get the int value corresponding to the token
     * @return 0 if the token does not contain an integer.
     */
    public int getValInt() {
        return valInt;
    }
    
    /**
     * Get the double value corresponding to the token
     * @return 0 if the token does not contain a double.
     */
    public double getValDouble() {
        return valDouble;
    }
    
    /**
     * Get the corresponding char position in the line
     * @return the left position of the token in the original file.
     */
    public int getCharPos() {
        return charPos;
    }
    
    /**
     * Get the corresponding line position in the file
     * @return the top position of the token in the original file.
     */
    public int getLinePos() {
        return linePos;
    }
    
    @Override
    public String toString() {
        String s = type.name() + "(" + linePos + ":" + charPos + ")";
        if(type == TokenType.IDENTIFIER || type == TokenType.VALUE_STRING)
            s += "{" + valString + "}";
        else if(type == TokenType.VALUE_INT)
            s += "{" + valInt + "}";
        else if(type == TokenType.VALUE_DOUBLE)
            s += "{" + valDouble + "}";
        return s;
    }
    
    public void mutateStringValue(String s) {
        if(valString == null)
            throw new RuntimeException("Cannot mutate string value of token " + this + " if it does not have one.");
        this.valString = s;
    }
}
