package fr.compil.jej.core;

import fr.compil.jej.nodes.expressions.*;
import fr.jamailun.jamlogger.JamLogger;

import java.util.*;

/**
 * Utility class with static-only methods.
 */
public final class Utils {
	private Utils() {}
	
	private static int ID_PROVIDER = 1;
	
	/**
	 * Used when a unique int must be created.
	 * @return a new int.
	 */
	public static int CREATE_ID() {
		return ID_PROVIDER++;
	}
	
	public static class Ref {
		public final String name;
		public final List<ExpressionNode> params;
		public final List<ExpressionNode> arrayIndex;
		public Ref(String name, List<ExpressionNode> params, List<ExpressionNode> arrayIndex) {
			this.name = name;
			this.params = params;
			this.arrayIndex = arrayIndex;
		}
		@Override
		public String toString() {
			if(name == null && !arrayIndex.isEmpty())
				return "ArrayRef[" + Arrays.toString(arrayIndex.toArray()) + "]";
			if(params == null) {
				return "Ref{'" + name + "'}";
			}
			return "Ref{'" + name + ", params=" + Arrays.toString(params.toArray()) + '}';
		}
		public CallableNode convert() {
			if(name == null && !arrayIndex.isEmpty())
				return new ArrayGetterExpression(arrayIndex);
			if(params == null) {
				return new FieldCallExpression(name);
			} else {
				return new MethodCallExpression(name, params);
			}
		}
	}
	
	public static String appendStuff(int indentLevel, String name, Map.Entry<String, List<?>> object) {
		return appendStuff(indentLevel, name, null, Collections.singletonList(object));
	}
	
	public static String appendStuff(int indentLevel, String name, String data, Map.Entry<String, List<?>> object) {
		return appendStuff(indentLevel, name, data, Collections.singletonList(object));
	}
	
	public static String appendStuff(int indentLevel, String name, String data, List<Map.Entry<String, List<?>>> objects) {
		StringBuilder sb = new StringBuilder();
		String indent = createIndentation(indentLevel);
		String newLine = "\n" + indent + "\t";
		sb.append(name).append("{").append(objects.isEmpty() ? "" : newLine);
		if(data != null && ! data.isBlank())
			sb.append("data=(").append(data).append(")");
		if(objects.isEmpty()) {
			sb.append("}");
			return sb.toString();
		}
		if(data != null && ! data.isBlank())
			sb.append(",").append(newLine);
		boolean first = true;
		String childPreIndent = createIndentation(indentLevel + 2);
		for(Map.Entry<String, List<?>> entry : objects) {
			if(first) first = false; else sb.append(",").append(newLine);
			sb.append(entry.getKey()).append("=[");
			if(entry.getValue().isEmpty()) {
				sb.append("]");
			} else {
				sb.append("\n");
				boolean subFirst = true;
				for(Object o : entry.getValue()) {
					if(subFirst) subFirst = false; else sb.append(",\n");
					sb.append(childPreIndent).append("- ");
					if(o instanceof Printable)
						sb.append(((Printable)o).niceString(indentLevel + 2));
					else
						sb.append(o);
				}
				sb.append(newLine).append("]");
			}
		}
		sb.append("\n").append(indent).append("}");
		return sb.toString();
	}
	
	public static String createIndentation(int indent) {
		if(indent < 1)
			return "";
		return "\t".repeat(indent);
	}
	
	/**
	 * Hash stuff with a garantie of obtaining a positive number.
	 * @param objs the objects to hash
	 */
	public static int hash(List<?> objs) {
		return hash(objs.toArray());
	}
	/**
	 * Hash stuff with a garantie of obtaining a positive number.
	 * @param objs the objects to hash
	 */
	public static int hash(Object... objs) {
		// turn the bit-sign off to avoid '-' in functions signatures.
		return Objects.hash(objs) & 0xfffffff;
	}
	
	public static boolean garantieTrue(boolean b, String error) {
		if(!b)
			JamLogger.error(error);
		return b;
	}
}
