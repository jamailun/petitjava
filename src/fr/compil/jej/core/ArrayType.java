package fr.compil.jej.core;

import fr.compil.jej.nodes.expressions.ExpressionNode;
import fr.jamailun.jamlogger.JamLogger;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class ArrayType extends Type {

	private int dimension;
	private final List<ExpressionNode> lengths = new LinkedList<>();

	public ArrayType(Type type) {
		super(type);
		dimension = 1;
	}

	public void addLength(ExpressionNode length) {
		this.lengths.add(length);
		if(getDimension() < this.lengths.size())
			addDimension();
	}

	public List<ExpressionNode> getLength() {
		return lengths;
	}

	public int getDimension() {
		return dimension;
	}

	public void addDimension() {
		dimension++;
	}

	@Override
	public ArrayType toArray() {
		return this;
	}

	@Override
	public boolean isArray() {
		return true;
	}
	
	@Override
	public Type toNotArray() {
		if (getDimension() == 1) {
			return new Type(this);
		} else {
			ArrayType type = new ArrayType(this);
			type.dimension = getDimension() - 1;
			return type;
		}
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		ArrayType type = (ArrayType) o;
		if(isPrimitive() != type.isPrimitive())
			return false;
		if(getDimension() != type.getDimension())
			return false;
		if(getLinkedClass() != null && type.getLinkedClass() != null)
			return getLinkedClass().isSubclass(type.getLinkedClass());
		return (isPrimitive() && getPrimitiveType() == type.getPrimitiveType()) || (!isPrimitive() && Objects.equals(getClassName(), type.getClassName()));
	}

	@Override
	public String toString() {
		String dim = "[]".repeat(Math.max(0, getDimension()));
		if(getDimension() == 0)
			JamLogger.error("DIMENSION == 0 !!");
		if(isPrimitive())
			return getPrimitiveType().name() + dim;
		if(getLinkedClass() == null)
			return "Type{className='" + getClassName() + "'"+ dim +"}";
		return "Type{class_linked=" + getLinkedClass().getName() + dim + "}";
	}
}
