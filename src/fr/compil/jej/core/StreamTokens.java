package fr.compil.jej.core;

import fr.compil.jej.errors.BadTokenError;
import fr.compil.jej.nodes.expressions.Operator;
import fr.compil.jej.nodes.expressions.logicparse.LogicOperator;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class StreamTokens {
    
    private final List<Token> tokens;
    private int index = 0;

    public StreamTokens(List<Token> tokens) {
        this.tokens = Collections.unmodifiableList(tokens);
    }

    public void reverseAndSelf() {
        index--;
        if(index < 0)
            throw new RuntimeException("reverse() call wen into negative !");
    }
    
    public Set<TokenType> getAllTypes() {
        return tokens.stream().map(Token::getType).collect(Collectors.toSet());
    }
    
    public Token peek() {
        return tokens.get(index);
    }

    public boolean hasNext() {
        return index < tokens.size() && peek().getType() != TokenType.EOF;
    }

    public Token next() {
        return tokens.get(index++);
    }

    public void junk() {
        index++;
    }
    
    public boolean isFuture(TokenType type) {
        return hasNext() && peek().getType() == type;
    }
    public boolean isNotFuture(TokenType type) {
        return hasNext() && peek().getType() != type;
    }
    
    public void junkOrThrow(TokenType type, String errorMessage) throws BadTokenError {
        if(peek().getType() != type)
            throw new BadTokenError(peek(), errorMessage);
        junk();
    }

    public boolean isPostPreviousFinal() {
        if(index < 2)
            return false;
        return tokens.get(index - 2).getType() == TokenType.FINAL;
    }
    
    public boolean doesFollow(TokenType... types) {
        int index = this.index;
        if(index + types.length >= this.tokens.size())
            return false;
        for(int i = index; i < index + types.length; i++) {
            if(tokens.get(i).getType() != types[i - index])
                return false;
        }
        return true;
    }

    public boolean isFutureAMathOperator() {
        if(!hasNext())
            return false;
        try {
            Operator.from(peek());
            return true;
        } catch(BadTokenError ignored) {
            return false;
        }
    }
    
    public boolean isFutureALogicOperator() {
        if(!hasNext())
            return false;
        try {
            LogicOperator.from(peek());
            return true;
        } catch(BadTokenError ignored) {
            return false;
        }
    }
    
    public boolean isFutureConsume(TokenType type) {
        if(isFuture(type)) {
            junk();
            return true;
        }
        return false;
    }
    
    public boolean canFindTokenBeforeAnother(TokenType first, TokenType... seconds) {
        List<TokenType> others = Arrays.asList(seconds);
        int indexCp = index;
        while(indexCp < tokens.size()) {
            TokenType current = tokens.get(indexCp).getType();
            if(current == first)
                return true;
            if(others.contains(current))
                return false;
            indexCp++;
        }
        return false;
    }
    
    public String readIdentifier() throws BadTokenError {
        Token t = next();
        if(t.getType() != TokenType.IDENTIFIER)
            throw new BadTokenError(t, "expected identifier.");
        return t.getValString();
    }
	
	public void changeAll(Map<String, String> identifiersTransformations) {
        for(int i = 0; i < tokens.size(); i++) {
            if(tokens.get(i).getType() == TokenType.IDENTIFIER) {
                String key = tokens.get(i).getValString();
                if(identifiersTransformations.containsKey(key)) {
                    tokens.get(i).mutateStringValue(identifiersTransformations.get(key));
                }
            }
        }
	}
}
