package fr.compil.jej.core;

/**
 * Enumerates all existing token types.
 * @see Token
 */
public enum TokenType {

    IDENTIFIER,     // generic keyword

    VALUE_INT,      // value
    VALUE_STRING,   // value
    VALUE_DOUBLE,   // value
    VALUE_CHAR,     // value
    VALUE_TRUE,
    VALUE_FALSE,

    PUBLIC,
    PRIVATE,
    PROTECTED,
    
    RETURN,
    CLASS,
    INTERFACE,
    IMPLEMENTS,
    EXTENDS,
    STATIC,
    FINAL,
    NEW,

    OR,             // ||
    AND,            // &&
    NOT,            // !

    TRY,
    CATCH,
    FINALLY,
    THROW,
    THROWS,
    
    WHILE,
    DO,
    IF,
    ELSE,
    FOR,

    NULL,
    THIS,

    TYPE_VOID,
    TYPE_INT,
    TYPE_DOUBLE,
    TYPE_BOOLEAN,
    TYPE_STRING,
    TYPE_CHAR,

    LEFT_BRACKET,
    RIGHT_BRACKET,

    LEFT_PAREN,
    RIGHT_PAREN,

    LEFT_BRACES,
    RIGHT_BRACES,

    DOT,            // .
    SEMICOLON,      // ;
    COMMA,          // ,
    EQUAL,          // =

    DOUBLE_ADD,     // ++
    DOUBLE_SUB,     // --

    MATH_ADD,
    MATH_SUB,
    MATH_MUL,
    MATH_DIV,
    MATH_GET,       // >=
    MATH_LET,       // <=
    MATH_GT,        // >
    MATH_LT,        // <
    MATH_EQUAL,     // ==
    MATH_NEQUAL,    // !=

    EOF;

    public Token convert(DocumentPosition pos) {
        return new Token(pos, this);
    }

}
