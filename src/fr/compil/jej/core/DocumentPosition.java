package fr.compil.jej.core;

public class DocumentPosition {
	
	private int lineNumber = 0;
	private int charPos;
	
	public DocumentPosition() {
		newLine();
	}
	
	public void newLine() {
		lineNumber++;
		charPos = 0;
	}
	
	public int getLineNumber() {
		return lineNumber;
	}
	
	public void progressChar() {
		charPos++;
	}
	
	public int getCharPos() {
		return charPos;
	}
}
