package fr.compil.jej.core;

import fr.compil.jej.errors.BadTokenError;

import java.util.Arrays;

public enum Visibility {

    PUBLIC(TokenType.PUBLIC),
    PRIVATE(TokenType.PRIVATE),
    PACKAGE_PROTECTED,
    PROTECTED(TokenType.PROTECTED);

    private final TokenType linkedType;
    Visibility() {
        linkedType = TokenType.EOF;
    }
    Visibility(TokenType type) {
        linkedType = type;
    }

    public static Visibility from(Token token) throws BadTokenError {
        return Arrays.stream(values())
                .filter(v -> v.linkedType == token.getType())
                .findFirst()
                .orElseThrow(() -> new BadTokenError(token, "Wrong visibility token."));
    }
    
    public static Visibility optionalFrom(StreamTokens tokens) {
        try {
            Visibility v = from(tokens.peek());
            tokens.junk();
            return v;
        } catch(BadTokenError ignored) {
            // Le prochain token n'est PAS un token de visibilité. On renvoie package protected
            return Visibility.PACKAGE_PROTECTED;
        }
    }

}
