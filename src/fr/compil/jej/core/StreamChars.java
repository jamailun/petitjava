package fr.compil.jej.core;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class StreamChars {

    private final char[] chars;
    private int index = 0;
    private final DocumentPosition position = new DocumentPosition();

    public StreamChars(String string) {
        this.chars = string.toCharArray();
    }
    
    public StreamChars(File file) {
        try(BufferedReader r = new BufferedReader(new FileReader(file))) {
            StringBuilder b = new StringBuilder();
            String line;
            while((line = r.readLine()) != null) {
                b.append(line).append('\n');
            }
            this.chars = b.toString().toCharArray();
        } catch (IOException e) {
            throw new IllegalArgumentException("Could not open file " + e.getMessage());
        }
    }

    //TODO optimiser en lisant le fichier ligne par ligne (pour réduire la complexité en mémoire)

    public char peek() {
        return chars[index];
    }

    public boolean hasNext() {
        return index < chars.length;
    }

    public char next() {
        position.progressChar();
        return chars[index++];
    }

    public void junk() {
        position.progressChar();
        index++;
    }

    public DocumentPosition pos() {
        return position;
    }
    
}
