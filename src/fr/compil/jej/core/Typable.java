package fr.compil.jej.core;

/**
 * Represents an entity owning a type.
 */
public interface Typable {
	Type getType();
}
