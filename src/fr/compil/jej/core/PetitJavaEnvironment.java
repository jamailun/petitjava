package fr.compil.jej.core;

import fr.compil.jej.errors.BadEnvironmentError;
import fr.compil.jej.nodes.ClassNode;
import fr.compil.jej.nodes.InterfaceNode;
import fr.compil.jej.nodes.Node;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PetitJavaEnvironment {
	
	private final List<ClassNode> classes = new LinkedList<>();
	private final List<InterfaceNode> interfaces = new LinkedList<>();
	
	private final boolean containsTry, containsStrings;
	
	public PetitJavaEnvironment(StreamTokens tokens) {
		Set<TokenType> set = tokens.getAllTypes();
		containsTry = set.contains(TokenType.TRY);
		containsStrings = set.contains(TokenType.VALUE_STRING) || set.contains(TokenType.TYPE_STRING);
	}
	
	public boolean containsTry() {
		return containsTry;
	}
	public boolean containsStrings() {
		return containsStrings;
	}
	
	public List<ClassNode> getClasses() {
		return classes;
	}
	
	public List<InterfaceNode> getInterfaces() {
		return interfaces;
	}
	
	public void linkInterfaces() throws BadEnvironmentError {
		for(InterfaceNode in : interfaces)
			in.linkWithInterfaces(this);
		for(ClassNode cn : classes)
			cn.linkWithInterfaces(this);
	}
	
	public boolean isEmpty() {
		return classes.isEmpty() && interfaces.isEmpty();
	}
	
	public int size() {
		return classes.size() + interfaces.size();
	}
	
	public InterfaceNode getInterface(Node source, String interfaceName) throws BadEnvironmentError {
		return interfaces.stream()
				.filter(i -> i.getName().equals(interfaceName))
				.findFirst()
				.orElseThrow(() -> new BadEnvironmentError("Unknown implemented interface name '" + interfaceName + "' for '" + source.getName() + "'."));
	}
	
	public void addClasse(ClassNode classe) throws BadEnvironmentError {
		if(classes.stream().anyMatch(e -> e.getName().equals(classe.getName())) || interfaces.stream().anyMatch(e -> e.getName().equals(classe.getName())))
			throw new BadEnvironmentError("Cannot register the same classe or interface name twice.");
		classes.add(classe);
	}
	public void addInterface(InterfaceNode interfaceNode) throws BadEnvironmentError {
		if(classes.stream().anyMatch(e -> e.getName().equals(interfaceNode.getName())) || interfaces.stream().anyMatch(e -> e.getName().equals(interfaceNode.getName())))
			throw new BadEnvironmentError("Cannot register the same classe or interface name twice.");
		interfaces.add(interfaceNode);
	}
	
	public List<String> getNames() {
		return Stream.concat(classes.stream(), interfaces.stream())
				.map(Node::getName)
				.collect(Collectors.toList());
	}
}
