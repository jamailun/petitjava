package fr.compil.jej.core;

import fr.compil.jej.nodes.ClassNode;
import fr.compil.jej.nodes.FieldNode;
import fr.compil.jej.nodes.InterfaceNode;
import fr.compil.jej.nodes.Node;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * The context of all the types, at some point of the parsing.
 * @see Type
 * @see fr.compil.jej.Parser
 * @see fr.compil.jej.TypeValidation
 */
public class TypesContext {
	
	public enum VariableContext {
		DECLARATION,    // declaration or parameter
		FIELD,          // field declaration
		CLASS,          // literally a class
	}
	
	private static class VariableData {
		Type blockType;
		final Type fieldType;
		public final boolean isClass;
		FieldNode staticFieldRef;
		VariableData(Type type, VariableContext context) {
			isClass = context == VariableContext.CLASS;
			if(context == VariableContext.DECLARATION) {
				blockType = type;
				fieldType = null;
			} else {
				blockType = null;
				fieldType = type;
			}
		}
		Type getType() {
			if(blockType == null)
				return fieldType;
			return blockType;
		}
		boolean isField() {
			return blockType == null;
		}
		@Override
		public String toString() {
			return "VariableData{" + getType() + "}";
		}
	}
	
	private final Map<String, VariableData> foundTypes = new HashMap<>();
	private final ClassNode currentClass;
	
	
	public TypesContext(ClassNode currentClass) {
		this.currentClass = currentClass;
	}
	
	public ClassNode getCurrentClass() {
		return currentClass;
	}
	
	public void registerClasses(List<ClassNode> classes) {
		classes.forEach(c -> {
			Type tc = new Type(c.getName());
			tc.linkClass(c);
			register(c.getName(), tc, VariableContext.CLASS);
		});
	}
	public void registerInterfaces(List<InterfaceNode> interfaces) {
		interfaces.forEach(i -> {
			Type ti = new Type(i.getName());
			ti.linkClass(i);
			register(i.getName(), ti, VariableContext.CLASS);
		});
	}
	
	public void register(String s, Type t, VariableContext context) {
		if(has(s)) {
			if(foundTypes.get(s).blockType != null)
				throw new RuntimeException("Cannot register an already defined block-variable : '" + s + "'.");
			foundTypes.get(s).blockType = t;
			return;
		}
		foundTypes.put(s, new VariableData(t, context));
	}
	public void registerField(FieldNode field) {
		if(has(field.getName())) {
			if(foundTypes.get(field.getName()).blockType != null)
				throw new RuntimeException("Cannot register an already defined block-variable. (var=" + field.getName() + ", type=" + foundTypes.get(field.getName()).blockType + ")");
			foundTypes.get(field.getName()).blockType = field.getType();
		} else {
			foundTypes.put(field.getName(), new VariableData(field.getType(), VariableContext.FIELD));
		}
		foundTypes.get(field.getName()).staticFieldRef = field.isStatic() ? field : null;
	}
	
	private void register(String s, VariableData vd) {
		foundTypes.put(s, vd);
	}
	
	public FieldNode getStaticFieldRef(String name) {
		if(has(name))
			return foundTypes.get(name).staticFieldRef;
		return null;
	}
	
	private boolean has(String s) {
		return foundTypes.containsKey(s);
	}
	public boolean hasVariable(String var) {
		return getType(var) != null;
	}
	
	public Type getType(String s) {
		if(has(s))
			return foundTypes.get(s).getType();
		return null;
	}
	
	public boolean isField(String s) {
		if(has(s))
			return foundTypes.get(s).isField() && ! foundTypes.get(s).isClass;
		return false;
	}
	public boolean isStaticField(String s) {
		if(has(s))
			return foundTypes.get(s).isField() && foundTypes.get(s).staticFieldRef != null;
		return false;
	}
	
	public boolean canBeRedefinedInBlock(String s) {
		if(has(s))
			return foundTypes.get(s).blockType == null;
		return true;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("TypesContext{");

		sb.append("\n\tCONTEXT=").append(currentClass.getName()).append(";\n");
		foundTypes.forEach((s,t) -> sb.append("\t-'").append(s).append("' : ").append(t).append("\n"));
		return sb.append('}').toString();
	}
	
	public TypesContext inherit() {
		TypesContext child = new TypesContext(currentClass);
		foundTypes.forEach(child::register);
		return child;
	}
	
	public List<String> getAllSubclasses(final Type type) {
		return foundTypes.values().stream()
				.filter(v -> v.isClass)
				.map(v -> v.getType().getLinkedMethodsOwner())
				.filter(t ->
						t.isSubtype(type.getLinkedMethodsOwner()) &&
						! t.equals(type.getLinkedMethodsOwner())
				)
				.map(Node::getName)
				.collect(Collectors.toList());
	}
}
