package fr.compil.jej.core;

/**
 * A method (or a method signature for interfaces) needs to be identified by a v-table id.
 */
public interface VtableIdentifiable {
	
	/**
	 * Get the current v-table identifier
	 * @return -1 if the value is not set.
	 */
	int getVTableId();
	
	/**
	 * Change the v-table identifier
	 * @param vtableId the new v-table identifier.
	 */
	void setVtableId(int vtableId);
}
