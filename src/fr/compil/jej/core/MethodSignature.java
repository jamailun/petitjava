package fr.compil.jej.core;

import fr.compil.jej.errors.BadTokenError;
import fr.compil.jej.nodes.InterfaceNode;
import fr.compil.jej.nodes.MethodsOwner;
import fr.compil.jej.nodes.Variable;
import fr.compil.jej.std.FakeMethod;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class MethodSignature implements VtableIdentifiable {
	
	private static int VTABLE_IDS_PROVIDER = 0;
	private final static Map<MethodSignature, Integer> VTABLE_IDS = new HashMap<>();
	private final static List<MethodSignature> TO_HANDLE = new LinkedList<>();
	
	public static void CALCULATE_VTABLEIDS() {
		TO_HANDLE.forEach(MethodSignature::determineVtableID);
	}
	
	protected final Visibility visibility;
	protected final boolean isStatic;
	protected final Type returnType;
	protected final List<Variable> arguments;
	private final String name;
	private final MethodsOwner owner;
	private FakeMethod fake = null;
	
	private int hash;
	
	public MethodSignature(Visibility visibility, boolean isStatic, Type returnType, String name, MethodsOwner owner) {
		this.visibility = visibility;
		this.isStatic = isStatic;
		this.returnType = returnType;
		this.name = name;
		this.arguments = new LinkedList<>();
		this.owner = owner;
		reHash();
		// Register this instance in the static collection.
		if(!owner.isFake())
			TO_HANDLE.add(this);
	}
	
	public String cName() {
		return (isStatic()?"stc_":"") + owner.getName() + "_" + getName() + "_" + getHash();
	}
	
	public boolean isMainMethod() {
		return isStatic
				&& name.equals("main")
				&& returnType == Type.TYPE_VOID
				&& (arguments.isEmpty() || (arguments.size() == 1 && arguments.get(0).getType().equals(new Type(Type.Primitive.STRING.asType()).toArray())));
	}
	
	public String getName() {
		return name;
	}
	
	private void reHash() {
		hash = Utils.hash(arguments.stream().map(a -> a.getType().cName()).collect(Collectors.toList()));
	}
	
	public void addArgument(Variable argument) {
		this.arguments.add(argument);
		reHash();
	}
	public void addArguments(List<Variable> arguments) {
		this.arguments.addAll(arguments);
		reHash();
	}
	
	public void setFake(FakeMethod fake) {
		this.fake = fake;
	}
	
	public boolean isFake() {
		return fake != null;
	}
	
	public FakeMethod getFakeMethod() {
		return fake;
	}
	
	public void determineVtableID() {
		if(VTABLE_IDS.containsKey(this)) {
			setVtableId(VTABLE_IDS.get(this));
			return;
		}
		int id = VTABLE_IDS_PROVIDER++;
		VTABLE_IDS.put(this, id);
		setVtableId(id);
	}
	
	public MethodsOwner getOwner() {
		return owner;
	}
	
	public int getHash() {
		return hash;
	}
	
	public boolean isStatic() {
		return isStatic;
	}
	
	public Visibility getVisibility() {
		return visibility;
	}
	
	public Type getReturnType() {
		return returnType;
	}
	
	public List<Variable> getArguments() {
		return arguments;
	}
	
	public boolean areParametersValid(List<? extends Typable> arguments) {
		if(getArguments().size() != arguments.size())
			return false;
		for(int i = 0; i < getArguments().size(); i++) {
			if( ! arguments.get(i).getType().equals(getArguments().get(i).getType()))
				return false;
		}
		return true;
	}
	
	// Used for comparison in a Map
	@Override
	public int hashCode() {
		return Objects.hash(isStatic, returnType, name, hash);
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		MethodSignature that = (MethodSignature) o;
		return isStatic == that.isStatic && hash == that.hash && Objects.equals(returnType, that.returnType) && Objects.equals(name, that.name);
	}
	
	@Override
	public String toString() {
		return "{#" + getName() + " -> " + getReturnType() + "; args=[" + arguments.stream().map(a -> a.getType().cName()).collect(Collectors.joining(", ")) + "]}";
	}
	
	public String getDisplayName() {
		return owner.getName() + "#" + name;
	}
	
	public static MethodSignature generate(StreamTokens tokens, InterfaceNode owner) throws BadTokenError {
		Visibility visibility = Visibility.optionalFrom(tokens);
		boolean isStatic = tokens.isFutureConsume(TokenType.STATIC);
		Type returnType = Type.generate(tokens.next());
		String name = tokens.readIdentifier();
		tokens.junkOrThrow(TokenType.LEFT_PAREN, "expected '(' after method name.");
		
		MethodSignature signature = new MethodSignature(visibility, isStatic, returnType, name, owner);
		
		List<Variable> arguments = new LinkedList<>();
		while(tokens.isNotFuture(TokenType.RIGHT_PAREN)) {
			if(!arguments.isEmpty())
				tokens.junkOrThrow(TokenType.COMMA, "expected ',' between two arguments.");
			boolean isFinal = tokens.isFutureConsume(TokenType.FINAL);
			Type argType = Type.generate(tokens.next());
			if(tokens.isFutureConsume(TokenType.LEFT_BRACKET)) {
				tokens.junkOrThrow(TokenType.RIGHT_BRACKET, "Expected a ']' after 'TYPE['.");
				argType = argType.toArray();
			}
			String argName = tokens.readIdentifier();
			Variable arg = new Variable(argType, argName);
			if(isFinal)
				arg.forceFinal();
			arguments.add(arg);
		}
		tokens.junkOrThrow(TokenType.RIGHT_PAREN, "expected ')' after method arguments.");
		
		signature.addArguments(arguments);
		return signature;
	}
	
	private int vtableId = 0;
	
	@Override
	public int getVTableId() {
		if(vtableId == -1)
			throw new RuntimeException("VtableId not set for method " + this.getName());
		return vtableId;
	}
	
	@Override
	public void setVtableId(int vtableId) {
		this.vtableId = vtableId;
	}
}
