package fr.compil.jej;

import fr.compil.jej.core.StreamChars;
import fr.compil.jej.core.Token;
import fr.compil.jej.core.TokenType;
import fr.compil.jej.errors.SyntaxError;
import fr.compil.jej.nodes.expressions.Operator;
import fr.jamailun.jamlogger.JamLogger;

import java.util.*;

import static java.util.Map.entry;

/**
 * Classe permettant la tokenisation de caractères
 * @see Tokenizer#start()
 */
public final class Tokenizer {
    
    /**
     * Caractères identifiables à un token directement.
     */
    private final static Map<Character, TokenType> SIMPLE_TOKENS = Map.ofEntries(
            entry('.', TokenType.DOT),
            entry(',', TokenType.COMMA),
            entry(';', TokenType.SEMICOLON),
            entry('(', TokenType.LEFT_PAREN),
            entry(')', TokenType.RIGHT_PAREN),
            entry('[', TokenType.LEFT_BRACKET),
            entry(']', TokenType.RIGHT_BRACKET),
            entry('{', TokenType.LEFT_BRACES),
            entry('}', TokenType.RIGHT_BRACES)
    );
    
    /**
     * Chaînes de caractères correspondant à un token.
     */
    private final static Map<String, TokenType> KEYWORD_TOKEN= Map.ofEntries(
            entry("public", TokenType.PUBLIC),
            entry("private", TokenType.PRIVATE),
            entry("protected", TokenType.PROTECTED),
            entry("try", TokenType.TRY),
            entry("catch", TokenType.CATCH),
            entry("finally", TokenType.FINALLY),
            entry("throw", TokenType.THROW),
            entry("throws", TokenType.THROWS),
            entry("static", TokenType.STATIC),
            entry("void", TokenType.TYPE_VOID),
            entry("boolean", TokenType.TYPE_BOOLEAN),
            entry("String", TokenType.TYPE_STRING),
            entry("double", TokenType.TYPE_DOUBLE),
            entry("int", TokenType.TYPE_INT),
            entry("char", TokenType.TYPE_CHAR),
            entry("class", TokenType.CLASS),
            entry("interface", TokenType.INTERFACE),
            entry("return", TokenType.RETURN),
            entry("final", TokenType.FINAL),
            entry("new", TokenType.NEW),
            entry("null", TokenType.NULL),
            entry("this", TokenType.THIS),
            entry("extends", TokenType.EXTENDS),
            entry("implements", TokenType.IMPLEMENTS),
            entry("for", TokenType.FOR),
            entry("if", TokenType.IF),
            entry("else", TokenType.ELSE),
            entry("do", TokenType.DO),
            entry("while", TokenType.WHILE),
            entry("true", TokenType.VALUE_TRUE),
            entry("false", TokenType.VALUE_FALSE)
    );

    private final static char[] ALPHA = "ABCDEFGHIJKLMNOPQRSTUVWXYZ_".toCharArray();
    private final static char[] DIGITS_DOT = "0123456789.".toCharArray();
    private final static char[] DIGITS = "0123456789".toCharArray();
    private final static char[] ALPHANUMERICS = "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ_".toCharArray();
    private final static char[] JUNK = { ' ', '\t', '\r' };
    
    /**
     * Méthode publique permettant la tokenization
     * @param chars stream de caractères à tokeniser
     * @return une liste de Token
     * @see StreamChars
     * @see Token
     */
    public static List<Token> tokenize(StreamChars chars) {
        try {
            return new Tokenizer(chars).start();
        } catch (SyntaxError e) {
            JamLogger.error("Error during tokenization :");
            e.printStackTrace();
            return new ArrayList<>(0);
        }
    }

    private List<Token> tokens;
    private final StreamChars chars;

    private Tokenizer(StreamChars chars) {
        this.chars = chars;
    }

    private List<Token> start() throws SyntaxError {
        tokens = new LinkedList<>();
        while(chars.hasNext()) {
            char current = chars.next();
            if(contains(ALPHA, current)) {
                // identifier ou token
                String identifier = composeIdentifier(current);
                if(KEYWORD_TOKEN.containsKey(identifier)) {
                    tokens.add(KEYWORD_TOKEN.get(identifier).convert(chars.pos()));
                } else {
                    tokens.add(new Token(chars.pos(), TokenType.IDENTIFIER, identifier));
                }
            }
            else if(contains(DIGITS, current)) {
                // Nombre indéterminé
                tokens.add(composeNumber(current));
            }
            else if(current == '/') {
                // Dans le cas d'un '/', ça peut être le début d'un commentaire ou une simple division
                if(ignoreCommentOrDiv())
                    tokens.add(TokenType.MATH_DIV.convert(chars.pos()));
            }
            else if(current == '\n') {
                // Nouvelle ligne
                chars.pos().newLine();
            }
            else if(current == '\"') {
                // Début d'un string
                tokens.add(composeStringValueToken());
            }
            else if(current == '\'') {
                // Début d'un char
                char c = chars.next();
                String cc = String.valueOf(c);
                if(c == '\\')
                    cc += chars.next();
                if(chars.next() != '\'')
                    throw new SyntaxError(chars.pos().getLineNumber(), "Char '"+cc+"' must be followed by a \"'\".");
                tokens.add(new Token(chars.pos(), TokenType.VALUE_CHAR, cc));
            }
            else if(!contains(JUNK, current)) {
                // tout le reste
                tokens.add(composeCharToken(current));
            }
        }
        tokens.add(TokenType.EOF.convert(chars.pos()));
        return tokens;
    }
    
    /**
     * Nombres commençant par un 0.
     */
    private Token composeNumberZero() throws SyntaxError {
        if(chars.hasNext() && contains(ALPHANUMERICS, chars.peek())) {
            throw new SyntaxError(chars.pos().getLineNumber(), "Unexpected char '0' here.");
        }
        if(chars.hasNext() && chars.peek() == '.') {
            chars.junk();
            StringBuilder digits = new StringBuilder("0.");
            char current;
            while(chars.hasNext() && contains(DIGITS, current = chars.peek())) {
                digits.append(current);
                chars.junk();
            }
            return new Token(chars.pos(), TokenType.VALUE_DOUBLE, Double.parseDouble(digits.toString()));
        } else {
            return new Token(chars.pos(), TokenType.VALUE_INT, 0);
        }
    }
    
    /**
     * Chaînes de caractères
     */
    private Token composeStringValueToken() {
        StringBuilder sb = new StringBuilder();
        boolean escapeNext = false;
        while(chars.hasNext()) {
            char current = chars.next();
            if(escapeNext) {
                escapeNext = false;
                sb.append(current);
            }
            else if(current == '\\') {
                escapeNext = true;
                sb.append(current);
            }
            else if(current == '\"') {
                break;
            }
            else {
                sb.append(current);
            }
        }
        return new Token(chars.pos(), TokenType.VALUE_STRING, sb.toString());
    }
    
    /**
     * Tokens formés de plusieurs caractères non alphanumériques
     */
    private Token composeCharToken(char first) throws SyntaxError {
        if(SIMPLE_TOKENS.containsKey(first)) {
            return SIMPLE_TOKENS.get(first).convert(chars.pos());
        }
        switch (first) {
            case '<':
                if(chars.peek() == '=') {
                    chars.junk();
                    return TokenType.MATH_LET.convert(chars.pos());
                }
                return TokenType.MATH_LT.convert(chars.pos());
            case '&':
                if(chars.peek() != '&') {
                    throw new SyntaxError(chars.pos().getLineNumber(), "Unexpected character : '"+chars.peek()+"' after '&' (wants '&&').");
                }
                chars.junk();
                return TokenType.AND.convert(chars.pos());
            case '|':
                if(chars.peek() != '|') {
                    throw new SyntaxError(chars.pos().getLineNumber(), "Unexpected character : '"+chars.peek()+"' after '|' (wants '||').");
                }
                chars.junk();
                return TokenType.OR.convert(chars.pos());
            case '+':
                if(chars.peek() == '+') {
                    chars.junk();
                    return TokenType.DOUBLE_ADD.convert(chars.pos());
                }
            case '-':
                if(first == '-' && chars.peek() == '-') {
                    chars.junk();
                    return TokenType.DOUBLE_SUB.convert(chars.pos());
                }
            case '*':
                if( chars.peek() != '=') {
                    return Operator.from(first).linkedType.convert(chars.pos());
                }
                return buildOperatorEqual(first);
            case '>':
                if(chars.peek() == '=') {
                    chars.junk();
                    return TokenType.MATH_GET.convert(chars.pos());
                }
                return TokenType.MATH_GT.convert(chars.pos());
            case '=':
                if(chars.peek() == '=') {
                    chars.junk();
                    return TokenType.MATH_EQUAL.convert(chars.pos());
                }
                return TokenType.EQUAL.convert(chars.pos());
            case '!':
                if(chars.peek() == '=') {
                    return TokenType.MATH_NEQUAL.convert(chars.pos());
                }
                return TokenType.NOT.convert(chars.pos());
        }
        throw new SyntaxError(chars.pos().getLineNumber(), "Unexpected character : '" + first + "'.");
    }
    
    /**
     * Transformation des "macros" de type { a += b } en { a = a + b }
     */
    private Token buildOperatorEqual(char first) throws SyntaxError {
        if(tokens.isEmpty())
            throw new SyntaxError("Cannot have a '" + first + "=' symbol to begin the code.");
        chars.junk();
        Token previous = tokens.get(tokens.size() - 1);
        if(previous.getType() != TokenType.IDENTIFIER)
            throw new SyntaxError("Cannot have a '" + first +"=' after something else than a IDENTIFIER.");
        //tokens.remove(tokens.size() - 1);
        tokens.add(TokenType.EQUAL.convert(chars.pos()));
        tokens.add(new Token(previous));
        return Operator.from(first).linkedType.convert(chars.pos());
    }
    
    /**
     * Construction des identifiers
     */
    private String composeIdentifier(char first) {
        char current;
        StringBuilder sb = new StringBuilder().append(first);
        while(chars.hasNext() && contains(ALPHANUMERICS, current = chars.peek())) {
            sb.append(current);
            chars.junk();
        }
        return sb.toString();
    }
    
    /**
     * Construction de tous les nombres
     */
    private Token composeNumber(char first) throws SyntaxError {
        if(first == '0')
            return composeNumberZero();
        char current;
        StringBuilder number = new StringBuilder().append(first);
        boolean decimal = false;
        while(chars.hasNext() && contains(DIGITS_DOT, current = chars.peek())) {
            chars.junk();
            if(current == '.') {
                if(decimal)
                    throw new SyntaxError(chars.pos().getLineNumber(), "Incorrect number format. Unexpected '.'.");
                decimal = true;
                number.append(".");
            } else {
                number.append(current);
            }
        }
        if(decimal)
            return new Token(chars.pos(), TokenType.VALUE_DOUBLE, Double.parseDouble(number.toString()));
        return new Token(chars.pos(), TokenType.VALUE_INT, Integer.parseInt(number.toString()));
    }
    
    /**
     * @return true if it was a div symbol.
     */
    private boolean ignoreCommentOrDiv() throws SyntaxError {
        if(!chars.hasNext())
            return true;
        if(chars.peek() == '=') {
            tokens.add(buildOperatorEqual('/'));
            return false;
        }
        if(chars.peek() == '/') {
            while(chars.hasNext() && chars.next() != '\n') {/* empty while block because we do everything in the condition*/}
            chars.pos().newLine();
            return false;
        } else if(chars.peek() == '*') {
            while(chars.hasNext()) {
                char current = chars.next();
                if(current == '\n') {
                    chars.pos().newLine();
                } else if(current == '*') {
                    if(chars.peek() == '/') {
                        chars.junk();
                        return false;
                    }
                }
            }
            return false;
        }
        return true;
    }
    
    /**
     * Check if a characters array contains a specific character
     * @param array char array
     * @param c character to test the existence of
     * @return true if the array contains the specific character
     */
    private static boolean contains(char[] array, char c) {
        char cx = Character.toUpperCase(c);
        for(char a : array)
            if(a == cx)
                return true;
        return false;
    }

}
