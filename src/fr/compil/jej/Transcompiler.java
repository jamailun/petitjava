package fr.compil.jej;

import fr.compil.jej.compilers.Visiteur;
import fr.compil.jej.compilers.VisiteurC;
import fr.compil.jej.core.PetitJavaEnvironment;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

/**
 * The last step for the transpilation.
 * From an environment, writes in a file
 */
public final class Transcompiler {
	
	private final PetitJavaEnvironment env;
	private final PrintStream output;
	private Transcompiler(PetitJavaEnvironment env, String output, boolean debug) throws IOException {
		this.env = env;
		if(output == null) {
			this.output = System.out;
			return;
		}
		File outputFile = new File(output);
		if(!outputFile.exists()) {
			if (!outputFile.createNewFile())
				throw new IOException("Could not create file '" + output + "'.");
		}
		if(debug)
			this.output = new DebugPrintStream(new FileOutputStream(outputFile));
		else
			this.output = new PrintStream(outputFile);
	}
	
	/**
	 * Do the stuff
	 */
	private void start() {
		// Here, we create the C-Visitor
		Visiteur visiteur = new VisiteurC(output);
		// Pré-traitement (defines, signatures de méthodes, def de classes, ...)
		visiteur.preHandleData(env);
		// Traitement réel (corps des méthodes des classes)
		env.getClasses().forEach(clazz -> clazz.accept(visiteur));
		// Fermeture du fichier
		end();
	}
	
	private void end() {
		if(output != System.out) {
			output.flush();
			output.close();
		}
	}
	
	/**
	 * Public method to transcompile the program.
	 * @param env the environment created by a Parser
	 * @param output the path to the .c file to create
	 * @param debug if the output needs to be printed in the standard output.
	 * @throws IOException if the output is not valid.
	 * @see Parser
	 * @see TypeValidation
	 * @see PetitJavaEnvironment
	 */
	public static void transcompile(PetitJavaEnvironment env, String output, boolean debug) throws IOException {
		new Transcompiler(env, output, debug).start();
	}
	
	/**
	 * Useful class to debug : print stuff in a specific OutputStream and in the standard one at the same time.
	 */
	public static class DebugPrintStream extends PrintStream {
		public DebugPrintStream(OutputStream out) {
			super(out);
		}
		@Override
		public void print(String s) {
			super.print(s);
			System.out.print(s);
		}
		@Override
		public void println(String s) {
			super.println(s);
			System.out.println(s);
		}
		@Override
		public void print(Object o) {
			super.print(o);
			System.out.print(o);
		}
		@Override
		public void println(Object o) {
			super.println(o);
			System.out.println(o);
		}
		@Override
		public void print(int o) {
			super.print(o);
			System.out.print(o);
		}
		@Override
		public void println(int o) {
			super.println(o);
			System.out.println(o);
		}
		@Override
		public void print(double o) {
			super.print(o);
			System.out.print(o);
		}
		@Override
		public void println(double o) {
			super.println(o);
			System.out.println(o);
		}
	}
	
}
