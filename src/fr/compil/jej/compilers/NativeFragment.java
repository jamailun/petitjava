package fr.compil.jej.compilers;

public enum NativeFragment {
	COMMON,
	STRINGS,
	STDIN,
	EXCEPTIONS
	//SYNCHRONIZED(COMMON)
	;
	
	@Override
	public String toString() {
		return name().toLowerCase() + ".nat";
	}
}
