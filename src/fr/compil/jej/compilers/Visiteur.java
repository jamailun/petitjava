package fr.compil.jej.compilers;

import fr.compil.jej.core.PetitJavaEnvironment;
import fr.compil.jej.nodes.ClassNode;
import fr.compil.jej.nodes.MethodNode;
import fr.compil.jej.nodes.expressions.*;
import fr.compil.jej.nodes.expressions.logicparse.LogicElementComplex;
import fr.compil.jej.nodes.expressions.logicparse.LogicElementEquality;
import fr.compil.jej.nodes.expressions.logicparse.LogicElementSimple;
import fr.compil.jej.nodes.expressions.mathparse.MathsElementComplex;
import fr.compil.jej.nodes.statements.*;

/**
 * A visitor to explore a petit-java environment.
 * The main (and currently only) implementation is VisiteurC to produce the C-langage.
 * @see PetitJavaEnvironment
 * @see VisiteurC
 */
public interface Visiteur {
    
    /**
     * Initialize the Visitor
     * @param environment the PetitJava environment to initialize the visitor with.
     */
    void preHandleData(PetitJavaEnvironment environment);
    
    /*** GENERAL ***/

    void acceptMethod(MethodNode node);
    void acceptClass(ClassNode node);

    /*** STATEMENTS ***/

    void acceptAssignmentStatement(AssignmentStatement statement);
    void acceptDeclarationStatement(DeclarationStatement statement);
    void acceptDeclarationAssignmentStatement(DeclarationAssignmentStatement statement);
    void acceptVoidStatement(VoidExecuteStatement statement);
    void acceptIfStatement(IfStatement statement);
    void acceptElseStatement(ElseStatement statement);
    void acceptWhileStatement(WhileStatement statement);
    void acceptForStatement(ForStatement statement);
    void acceptReturnStatement(ReturnStatement statement);
    void acceptNewStatement(NewStatement statement);
    void acceptAssignmentFieldStatement(AssignmentFieldStatement statement);
    void acceptArrayAssignmentStatement(ArrayAssignmentStatement statement);
    void acceptTryCatchStatement(TryCatchStatement statement);
    void acceptCatchStatement(CatchStatement statement);
    void acceptThrowStatement(ThrowStatement statement);

    /*** EXPRESSIONS ***/

    void acceptNumberExpression(NumberExpression expression);
    void acceptStringExpression(StringExpression expression);
    void acceptThisExpression(ThisExpression expression);
    void acceptMathElementComplexExpression(MathsElementComplex expression);
    void acceptLogicElementSimpleExpression(LogicElementSimple expression);
    void acceptLogicElementComplexExpression(LogicElementComplex expression);
    void acceptLogicElementEqualityExpression(LogicElementEquality expression);
    void acceptObjectReferenceExpression(ObjectRefExpression expression);
    void acceptFieldCallExpression(FieldCallExpression expression);
    void acceptBoolExpression(BoolExpression expression);
    void acceptMethodCall(MethodCallExpression expression);
    void acceptNewExpression(NewExpression expression);
    void acceptArrayGetterExpression(ArrayGetterExpression expression);
    void acceptHardArrayExpression(HardArrayExpression expression);
    void acceptDoubleOperatedExpression(DoubleOperatedExpression expression);
    void acceptNullExpression(NullExpression expression);

}
