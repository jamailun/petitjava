package fr.compil.jej.compilers;

import fr.compil.jej.core.PetitJavaEnvironment;
import fr.compil.jej.core.Type;
import fr.compil.jej.core.Utils;
import fr.compil.jej.nodes.ClassNode;
import fr.compil.jej.nodes.ConstructorNode;
import fr.compil.jej.nodes.FieldNode;
import fr.compil.jej.nodes.InterfaceNode;
import fr.compil.jej.nodes.MethodNode;
import fr.compil.jej.nodes.Variable;
import fr.compil.jej.nodes.expressions.*;
import fr.compil.jej.nodes.expressions.logicparse.LogicElementComplex;
import fr.compil.jej.nodes.expressions.logicparse.LogicElementEquality;
import fr.compil.jej.nodes.expressions.logicparse.LogicElementSimple;
import fr.compil.jej.nodes.expressions.mathparse.MathsElement;
import fr.compil.jej.nodes.expressions.mathparse.MathsElementComplex;
import fr.compil.jej.nodes.statements.*;
import fr.compil.jej.std.FakeException;
import fr.compil.jej.std.FakeInput;
import fr.compil.jej.std.FakeMethod;
import fr.compil.jej.std.STD;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class VisiteurC implements Visiteur {
    
    private final static String SELF = "this";
    private final static String OF = "->";
    private final static String VTABLE = "vtable";
    private final static String TMP = "tmp_";
    private final static String NULL = "NULL";
    public final static String ARRAY = "_" + Utils.CREATE_ID();

    private final List<FieldNode> toInitFields = new ArrayList<>();
    private final PrintStream output;
    int tabIndex = 0;
    private boolean isFor = false; // During a for-loop, statement must NOT write ';'.

    public VisiteurC(PrintStream output) {
        this.output = output;
    }

    private void print(String line) {
        output.print(line);
    }
    private void print(StringBuilder line) {
        output.print(line);
    }
    private void printBlock(List<StatementNode> statements) {
        tabIndex++;
        for(StatementNode stt : statements) {
            tabN();
            stt.accept(this);
        }
        tabIndex--;
    }

    private void tab() {
        for(int i = 0; i < tabIndex; i++)
            output.print("\t");
    }
    private void tabN() {
        output.print("\n");
        tab();
    }
    private void semicolon() {
        if(!isFor)
            print(";");
    }
    
    private void getClassStruct(StringBuilder sb, ClassNode clazz) {
        sb.append("struct ").append(clazz.getName()).append(" {\n");
        sb.append("\tvoid* (**" + VTABLE + ")();\n");
        for(FieldNode field : clazz.getFields()) {
            sb.append("\t").append(field.getType().cType()).append(" ").append(field.getName()).append(";\n");
        }
        sb.append("};\n");
    }
    private String getInterfaceStruct(InterfaceNode inter) {
        return "struct " + inter.getName() + " {\n"
            + "\tvoid* (**" + VTABLE + ")();\n"
            + "};\n";
    }

    private void getMethodSignature(StringBuilder sb, MethodNode node) {
        sb.append("void* ").append(node.cName()).append("(");
        boolean first = true;
        if(!node.isStatic()) {
            sb.append(node.getClassOwner().getName()).append("* " + SELF);
            first = false;
        }
        for(Variable param : node.getArguments()) {
            if(first) first = false; else sb.append(", ");
            sb.append(param.getType().cType());
            sb.append(" ").append(param.getName());
        }
        sb.append(")");
    }
    private void getConstructorSignature(StringBuilder sb, ConstructorNode node) {
        sb.append(node.getClassOwner().getName()).append("* ").append(node.cName()).append("(");
        boolean first = true;
        for(Variable param : node.getArguments()) {
            if(first) first = false; else sb.append(", ");
            sb.append(param.getType().cType());
            sb.append(" ").append(param.getName());
        }
        sb.append(")");
    }
    
    private void writeDefinesExceptions(PetitJavaEnvironment env) {
        List<ClassNode> classes = new LinkedList<>(env.getClasses());
        classes.addAll(STD.FAKE_CLASSES);
        classes.stream()
                .filter(c -> c.isSubtype(FakeException.FAKE_EXCEPTION))
                .forEach(c -> print("#define " + c.getName() + " (" + Utils.hash(c.getName()) + ")\n") );
    }
    
    @Override
    public void preHandleData(PetitJavaEnvironment env) {
        toInitFields.clear();
        NativeHandler.printNative(output, NativeFragment.COMMON);
        if(env.containsStrings())
            NativeHandler.printNative(output, NativeFragment.STRINGS);
        if(env.containsTry())
            NativeHandler.printNative(output, NativeFragment.EXCEPTIONS);
        NativeHandler.printNative(output, NativeFragment.STDIN);
        writeDefinesExceptions(env);
        List<ClassNode> classNodes = env.getClasses();
        print("\n// Classes definition\n");
        // 1. Write all structures
        print("typedef struct " + ARRAY + " " + ARRAY + ";\n");
        print("struct " + ARRAY + " {\n\tvoid* array;\n\tint length;\n};\n");
        for(String typeName : env.getNames())
            print("typedef struct " + typeName + " " + typeName + ";\n");
        for(ClassNode clazz : classNodes) {
            StringBuilder sb = new StringBuilder();
            getClassStruct(sb, clazz);
            print(sb);
        }
        for(InterfaceNode inter : env.getInterfaces()) {
            print(getInterfaceStruct(inter));
        }
        StringBuilder sb = new StringBuilder();
        sb.append("\n// Signatures of methods\n");
        // 2. Write all signatures
        classNodes.stream()
                .flatMap(c -> c.getMethods().stream())
                .filter(m -> !m.isMainMethod())
                .forEach(methodNode -> {
                    getMethodSignature(sb, methodNode);
                    sb.append(";\n");
                });
        classNodes.stream()
                .flatMap(c -> c.getConstructors().stream())
                .forEach(constructorNode -> {
                    getConstructorSignature(sb, constructorNode);
                    sb.append(";\n");
                });
        sb.append("\n// Static fields\n");
        classNodes.stream()
                .flatMap(c -> c.getFields().stream())
                .filter(FieldNode::isStatic)
                .forEach(field -> {
                    sb.append(field.getType().cType()).append(" ");
                    sb.append(field.cName()).append(";\n");
                    if(field.hasAssignment())
                        toInitFields.add(field);
                });
        sb.append("\n// Vtables\n");
        for(ClassNode clazz : classNodes) {
            List<MethodNode> vTableRefs = clazz.getMethodsAsVtable();
            sb.append("void* (*").append(clazz.getName()).append("_"+VTABLE).append("[])() = {");
            boolean first = true;
            for(MethodNode mn : vTableRefs) {
                if(first) first = false; else sb.append(", ");
                sb.append(mn == null ? NULL : mn.cName());
            }
            sb.append("};\n");
        }
        // New method
        sb.append("\n// 'new' methods\n\n");
        print(sb);

        // Classes
        for(ConstructorNode cn : classNodes.stream().flatMap(c -> c.getConstructors().stream()).collect(Collectors.toList())) {
            // Signature du new
            StringBuilder sbT = new StringBuilder();
            getConstructorSignature(sbT, cn);
            print(sbT);
            print(" {");
            tabIndex++;
            // malloc
            tabN();
            String type = new Type(cn.getClassOwner()).cType();
            print(type + " " + SELF + " = (" + type + ") malloc( sizeof(" + cn.getClassOwner().getName() + ") );");
            // Set la vtable
            tabN();
            print(SELF + OF + VTABLE + " = " + cn.getClassOwner().getName() +  "_" + VTABLE + ";");
            // initialiser les fields
            for (FieldNode field : cn.getClassOwner().getFields()) {
                if(!field.isStatic() && field.hasAssignment()) {
                    tabN();
                    print(SELF + OF + field.cName() + " = ");
                    field.getAssignment().accept(this);
                    semicolon();
                }
            }
            // reste des statements
            cn.getStatements().forEach(stt -> {
                tabN();
                stt.accept(this);
            });
            // return statement
            tabN();
            print("return " + SELF + ";");
            // Fin du new
            tabIndex--;
            tabN();
            print("}\n");
        }
        // End
        print("\n// Real code\n");
    }

    /*===============================================================================================================*\
    |*===                                                                                                        ====*|
    |*===                                               STATEMENTS                                               ====*|
    |*===                                                                                                        ====*|
    \*===============================================================================================================*/
    
    @Override
    public void acceptMethod(MethodNode node) {
        StringBuilder sb = new StringBuilder();
        if(node.isMainMethod()) {
            sb.append("int main(");
            if(!node.getArguments().isEmpty()) {
                sb.append("int argc, char* ")
                        .append(node.getArguments().get(0).getName())
                        .append("[]");
            }
            sb.append(")");
        } else {
            getMethodSignature(sb, node);
        }
        sb.append(" {");
        print(sb);
        tabIndex++;
        // STDIN SCANNER
        tabN();
        print(FakeInput.STD_IN_INIT_C);
        if(node.isMainMethod() && !toInitFields.isEmpty()) {
            // Dans la méthode main, on initialize les champs statiques.
            tabN();
            print("// Static fields initialization.");
            toInitFields.forEach(field -> {
                tabN();
                print(field.cName() + " = ");
                field.getAssignment().accept(this);
                semicolon();
            });
            tabN();
            print("// Main method's code");
        }
        for(StatementNode statementNode : node.getStatements()) {
            tabN();
            statementNode.accept(this);
        }
        tabIndex--;
        print("\n}\n");
    }
    
    @Override
    public void acceptClass(ClassNode node) {
        // Les struct des classes sont déjà générées par le prétraitement
        for(MethodNode method : node.getMethods()) {
            method.accept(this);
        }
    }
    
    @Override
    public void acceptAssignmentStatement(AssignmentStatement statement) {
        print(statement.getAssignedVariable().getName() + " = ");
        statement.getAssignation().accept(this);
        semicolon();
    }
    
    @Override
    public void acceptDeclarationStatement(DeclarationStatement statement) {
        String str = statement.getDeclaredVariable().getType().cType();
        print(str + " " + statement.getName());
        semicolon();
    }
    
    @Override
    public void acceptDeclarationAssignmentStatement(DeclarationAssignmentStatement statement) {
        print(statement.getDeclaredVariable().getType().cType() + " " + statement.getName());
        print(" = ");
        statement.getAssignation().accept(this);
        semicolon();
    }

    @Override
    public void acceptVoidStatement(VoidExecuteStatement statement) {
        statement.getExpression().accept(this);
        semicolon();
    }
    
    @Override
    public void acceptIfStatement(IfStatement statement) {
        print("if(");
        statement.getCondition().accept(this);
        print(") {");
        printBlock(statement.getStatements());
        tabN();
        print("}");
        if(statement.hasChild()) {
            print(" ");
            statement.getChild().accept(this);
        }
    }
    
    @Override
    public void acceptElseStatement(ElseStatement statement) {
        print("else {");
        printBlock(statement.getStatements());
        tabN();
        print("}");
    }
    
    @Override
    public void acceptWhileStatement(WhileStatement statement) {
        if(statement.isDoForm()) {
            print("do {");
        } else {
            print("while(");
            statement.getCondition().accept(this);
            print(") {");
        }
        printBlock(statement.getStatements());
        tabN();
        print("}");
        if(statement.isDoForm()) {
            print(" while(");
            statement.getCondition().accept(this);
            print(");");
        }
    }
    
    @Override
    public void acceptForStatement(ForStatement statement) {
        print("for(");
        isFor = true;
        statement.getBeginStatement().accept(this);
        print("; ");
        statement.getCondition().accept(this);
        print("; ");
        statement.getLoopExpression().accept(this);
        isFor = false;
        print(") {");
        printBlock(statement.getStatements());
        tabN();
        print("}");
    }
    
    @Override
    public void acceptReturnStatement(ReturnStatement statement) {
        print("return");
        if(!statement.isEmpty()) {
            print(" (void*)");
            statement.getReturnedValue().accept(this);
        }
        print(";");
    }
    
    
    @Override
    public void acceptNewStatement(NewStatement statement) {
        statement.getNewExpression().accept(this);
        semicolon();
    }

    @Override
    public void acceptAssignmentFieldStatement(AssignmentFieldStatement statement) {
        statement.getAssignedFieldCall().accept(this);
        print(" = ");
        statement.getAssignation().accept(this);
        semicolon();
    }

    @Override
    public void acceptArrayAssignmentStatement(ArrayAssignmentStatement statement) {
        statement.getAssignedArrayValue().accept(this);
        print(" = ");
        statement.getAssignation().accept(this);
        semicolon();
    }
    
    @Override
    public void acceptTryCatchStatement(TryCatchStatement statement) {
        print(NativeHandler.NEXCEP_TRY + " {");
        printBlock(statement.getStatements());
        tabN();
        print("}");
        statement.getCatches().forEach(c -> c.accept(this));
        print(" " + NativeHandler.NEXCEP_FINALLY + " {");
        printBlock(statement.getFinallyContent());
        if(!statement.getFinallyContent().isEmpty()) tabN();
        print("} " + NativeHandler.NEXCEP_END_TRY + ";");
    }
    
    @Override
    public void acceptCatchStatement(CatchStatement statement) {
        print(" " + NativeHandler.NEXCEP_CATCH + "(" + statement.getCValue() + ")");
        for(String otherCatch : statement.getSubclassesToCatch()) {
            print(" " + NativeHandler.NEXCEP_ORCATCH + "(" + otherCatch + ")");
        }
        print("{");
        printBlock(statement.getStatements());
        tabN();
        print("}");
    }
    
    @Override
    public void acceptThrowStatement(ThrowStatement statement) {
        statement.getThrowedExpression().accept(this);
        String cValue = statement.getThrowedExpression().getType().getLinkedClass().getName();
        print(NativeHandler.NEXCEP_END_THROW + "(" + cValue + ")");
        semicolon();
    }
    
    /*===============================================================================================================*\
    |*===                                                                                                        ====*|
    |*===                                              EXPRESSIONS                                               ====*|
    |*===                                                                                                        ====*|
    \*===============================================================================================================*/
    
    @Override
    public void acceptNumberExpression(NumberExpression expression) {
        if(expression instanceof IntExpression)
            print("" + ((IntExpression) expression).getRaw());
        else if(expression instanceof DoubleExpression)
            print("" + ((DoubleExpression) expression).getRaw());
        else if(expression instanceof CharExpression)
            print("'" + ((CharExpression) expression).getRaw() + "'");
        else
            throw new RuntimeException("NumberExpression pas implémentée >:(");
    }
    
    
    @Override
    public void acceptStringExpression(StringExpression expression) {
        print(NativeHandler.NSTRING_NEWSTRING + "(\"" + expression.getRawValue() + "\")");
    }
    
    @Override
    public void acceptThisExpression(ThisExpression expression) {
        print(SELF);
    }
    
    private void handleMathElementPartInString(MathsElement element) {
        Type elementType = element.getType();
        boolean subCall = !( elementType.equals(Type.Primitive.STRING.asType()) );
        if(subCall) {
            if(elementType.isPrimitive()) {
                print(elementType.getPrimitiveType().toStringMethod + "(");
            } else {
                print(NativeHandler.NSTRING_TOSTRING_OBJECT + "(");
            }
        }
        element.accept(this);
        if(subCall)
            print(")");
    }
    
    @Override
    public void acceptMathElementComplexExpression(MathsElementComplex expression) {
        if(expression.canBeString()) {
            // STRING MODE
            print(NativeHandler.NSTRING_CONCATENATE + "(");
            handleMathElementPartInString(expression.getLeft());
            //TODO déréférencer directement les Strings concaténés ?
            print(", ");
            handleMathElementPartInString(expression.getRight());
            print(")");
            return;
        }
        // MATHS MODE
        print("(");
        expression.getLeft().accept(this);
        print(" " + expression.getOperator() + " ");
        expression.getRight().accept(this);
        print(")");
    }
    
    @Override
    public void acceptLogicElementSimpleExpression(LogicElementSimple expression) {
        if(expression.isNegative())
            print("! ");
        expression.getWrapped().accept(this);
    }
    
    @Override
    public void acceptLogicElementComplexExpression(LogicElementComplex expression) {
        if(expression.isNegative())
            print("! ");
        print("(");
        for(int i = 0; i < expression.getWrapped().size() - 1; i++) {
            expression.getWrapped().get(i).accept(this);
            print(" " + expression.getOperators().get(i));
            print(" ");
        }
        expression.getWrapped().get(expression.getWrapped().size() - 1).accept(this);
        print(")");
    }
    
    @Override
    public void acceptLogicElementEqualityExpression(LogicElementEquality expression) {
        if(expression.isNegative())
            print("! ");
        print("(");
        expression.getLeft().accept(this);
        print(" " + expression.getOperator() + " ");
        expression.getRight().accept(this);
        print(")");
    }
    
    @Override
    public void acceptObjectReferenceExpression(ObjectRefExpression expression) {
        if(expression.getStaticFieldRef() != null)
            print(expression.getStaticFieldRef().cName());
        else
           print(expression.getName());
    }

    @Override
    public void acceptFieldCallExpression(FieldCallExpression expression) {
        if(expression.getLinkedField() instanceof FakeMethod) {
            ((FakeMethod)expression.getLinkedField()).acceptCustom(expression.getCallable(), Collections.emptyList(), this::print, this);
            return;
        }
        expression.getCallable().accept(this);
        print(OF);
        print(expression.getLinkedField().getName());
    }

    @Override
    public void acceptBoolExpression(BoolExpression expression) {
        print(expression.getRaw() ? "TRUE" : "FALSE");
    }

    @Override
    public void acceptMethodCall(MethodCallExpression expression) {
        // Fake-STD method
        if(expression.getLinkedMethod().isFake()) {
            expression.getLinkedMethod().getFakeMethod().acceptCustom(expression.getCallable(), expression.getParams(), this::print, this);
            return;
        }
        boolean hasCalled = expression.getCallable() instanceof NewExpression || expression.getCallable() instanceof CallableNode;
        String leftOf = null;
        if(hasCalled) {
            print("({ ");
            String tmp = TMP + Utils.CREATE_ID();
            print(expression.getCallable().getType().cType());
            print(" " + tmp + " = ");
            expression.getCallable().accept(this);
            print("; ");
            leftOf = tmp;
        }

        print("(" + expression.getLinkedMethod().getReturnType().cType() + ")");

        if(expression.getLinkedMethod().isStatic()) {
            print(expression.getLinkedMethod().cName() + "(");
        } else {
            if(hasCalled)
                print(leftOf);
            else
                expression.getCallable().accept(this);
            print(OF + VTABLE + "[" + expression.getLinkedMethod().getVTableId() + "](" );
        }
        boolean first = true;
        if(!expression.getLinkedMethod().isStatic()) {
            first = false;
            expression.getCallable().accept(this);
        }
        for(ExpressionNode param : expression.getParams()) {
            if(first) first = false; else print(", ");
            param.accept(this);
        }
        print(")");
        if(hasCalled)
            print("; })");
    }
    
    private void initLoopArray(String expressionName, List<ExpressionNode> sizes) {
        ExpressionNode _size = sizes.get(0);
        sizes.remove(0);
        String _tmp = TMP+Utils.CREATE_ID();
        print("({" + ARRAY + "* " + _tmp + " = (" + ARRAY + "*) malloc( sizeof(" + ARRAY + ") ); " + _tmp + OF + "array = calloc(");
        _size.accept(this);
        print(", sizeof(" + expressionName + ")); " + _tmp + OF + "length = ");
        _size.accept(this);
        print(";");
        if(sizes.isEmpty()) {
            print("; " + _tmp + ";})");
            return;
        }
        String _i = "_i" + TMP + Utils.CREATE_ID();
        print("for(int "+_i+"=0;"+_i+"<");
        _size.accept(this);
        print("; "+_i+"++){");
        print("(("+ARRAY+"**)("+_tmp+OF+"array))["+_i+"] = ");
        initLoopArray(expressionName, sizes);
        print(";};" + _tmp + ";})");
    }
    
    @Override
    public void acceptNewExpression(NewExpression expression) {
        if(expression.getLinkedConstructor() instanceof FakeMethod) {
            ((FakeMethod)expression.getLinkedConstructor()).acceptCustom(expression, expression.getParams(), this::print, this);
            return;
        }
        StringBuilder sb = new StringBuilder();
        if(expression.isHardArray()) {
            throw new RuntimeException("A hard-array should be called from somewhere else.");
        } else if(expression.isArray()) {
            initLoopArray(expression.getType().cName(), new ArrayList<>(expression.getArraySize()));
        } else {
            //sb.append("(").append(expression.getLinkedConstructor().getClassOwner().getName()).append("*)");
            sb.append(expression.getLinkedConstructor().cName()).append("(");
            print(sb);
            boolean first = true;
            for(ExpressionNode param : expression.getParams()) {
                if(first) first = false; else print(", ");
                param.accept(this);
            }
            print(")");
        }
    }

    private void arrayGetterLoop(ArrayGetterExpression expression, int size) {
        if (size <= 1) {
            expression.getCallable().accept(this);
        } else {
            size--;
            print("((" + ARRAY + "**)(");
            arrayGetterLoop(expression, size);
            print(")" + OF + "array)");
            print("[");
            expression.getIndexes().get(size - 1).accept(this);
            print("]");
        }

    }

    @Override
    public void acceptArrayGetterExpression(ArrayGetterExpression expression) {
        // (((int*)(((Array_63537721**)(tab)->array)[0])->array)[0]) = 10;
        int size = expression.getIndexes().size();
        print("((");
        if (size < expression.getDimension()) {
            print("(" + ARRAY + "**)(");
        } else {
            print("(" + expression.getType().cName());
            if (!expression.getType().isPrimitive()) print("*");
            print("*)(");
        }
        arrayGetterLoop(expression, size);
        print(")" + OF + "array)");
        print("[");
        expression.getIndexes().get(size - 1).accept(this);
        print("]");
        print(")");
    }

    @Override
    public void acceptHardArrayExpression(HardArrayExpression expression) {
        // ({ int* tmp_ = calloc(10, sizeof(int)); tmp_[0] = 1;  tmp_[1] = 2;  tmp_[2] = 3; tmp_;});
        print("({" + ARRAY + "* " + TMP + " = ");
        print("(" + ARRAY + "*) malloc( sizeof(" + ARRAY + ") ); ");
        print(TMP + OF + "array = calloc(" + expression.getSize());
        print(", sizeof(" + expression.getType().cName() + ")); ");
        int i = 0;
        for(ExpressionNode child : expression.getExpressions()) {
            //print(TMP + OF + "array" + "[" + i + "] = ");
            print("((");
            if (child instanceof HardArrayExpression) {
                print("(" + ARRAY + "*");
            } else {
                print("(" + expression.getType().cName());
                if (!expression.getType().isPrimitive()) print("*");
            }
            print("*)(" + TMP);
            print(")" + OF + "array");
            print(")[" + i);
            print("]) = ");
            child.accept(this);
            print("; ");
            i++;
        }
        print(TMP + OF + "length = " + expression.getSize() + "; " + TMP + ";})");
    }

    @Override
    public void acceptDoubleOperatedExpression(DoubleOperatedExpression expression) {
        if(expression.isBeforeExpression())
            print(expression.getOperator());
        print("(");
        expression.getTarget().accept(this);
        print(")");
        if(!expression.isBeforeExpression())
            print(expression.getOperator());
    }
    
    @Override
    public void acceptNullExpression(NullExpression expression) {
        print("NULL");
    }

}
