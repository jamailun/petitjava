package fr.compil.jej.compilers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;

public class NativeHandler {
	private NativeHandler() {}
	
	public static void printNative(PrintStream output, NativeFragment nat) {
		File nativeFile = new File("native/" + nat);
		if(!nativeFile.exists())
			throw new RuntimeException("Unknown native file : '" + nat + "'.");
		if(!nativeFile.canRead())
			throw new RuntimeException("Cannot read native file : '" + nativeFile + "'.");
		try(BufferedReader reader = new BufferedReader(new FileReader(nativeFile))) {
			String line;
			while((line = reader.readLine()) != null)
				output.println(line);
		} catch (IOException ioe) {
			throw new RuntimeException("Error while reading file '" + nat + "' : " + ioe.getMessage());
		}
	}

	public static final String NSTRING_RAW = "String";
	public static final String NSTRING_STRING = "String*";
	public static final String NSTRING_NEWSTRING = "new_String";
	public static final String NSTRING_TOSTRING_INT = "toString_int";
	public static final String NSTRING_TOSTRING_LONG = "toString_long";
	public static final String NSTRING_TOSTRING_CHAR = "toString_char";
	public static final String NSTRING_TOSTRING_DOUBLE = "toString_double";
	public static final String NSTRING_TOSTRING_FLOAT = "toString_float";
	public static final String NSTRING_TOSTRING_BOOL = "toString_bool";
	public static final String NSTRING_TOSTRING_OBJECT = "toString_object";
	public static final String NSTRING_CONCATENATE = "_string_concatenate";
	
	public static final String NEXCEP_TRY = "TRY";
	public static final String NEXCEP_CATCH = "CATCH";
	public static final String NEXCEP_ORCATCH = "ORCATCH";
	public static final String NEXCEP_FINALLY = "FINALLY";
	public static final String NEXCEP_END_TRY = "ETRY";
	public static final String NEXCEP_END_THROW = "THROW";
	public static final String NEXCEP_GLOBAL_ERROR = "_GL_error_msg";
	
}
