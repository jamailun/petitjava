package fr.compil.jej.errors;

/**
 * Error threw when the tokenizer or the parser encounters a wrong syntax.
 * @see fr.compil.jej.Tokenizer
 * @see fr.compil.jej.Parser
 */
public class SyntaxError extends Exception {

    public SyntaxError(int line, String error) {
        super("SyntaxError line " + line + " : " + error);
    }
    
    public SyntaxError(String error) {
        super(error);
    }

}
