package fr.compil.jej.errors;

import fr.compil.jej.core.Token;

/**
 * Error threw when the parser encounters an unexpected token.
 * @see Token
 * @see fr.compil.jej.Parser
 */
public class BadTokenError extends Exception {

    public BadTokenError(Token token, String error) {
        super("BadTokenError with token " + token + ". " + error);
    }

}
