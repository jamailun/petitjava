package fr.compil.jej.errors;

/**
 * Error threw when the type validation or the transcompiler encounters an error with the environment.
 * @see fr.compil.jej.TypeValidation
 * @see fr.compil.jej.Transcompiler
 * @see fr.compil.jej.core.PetitJavaEnvironment
 */
public class BadEnvironmentError extends Exception {

    public BadEnvironmentError(String error) {
        super(error);
    }

}
