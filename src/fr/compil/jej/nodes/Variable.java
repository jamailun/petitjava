package fr.compil.jej.nodes;

import fr.compil.jej.core.Typable;
import fr.compil.jej.core.Type;
import fr.jamailun.jamlogger.JamLogger;

import java.util.Objects;

/**
 * A variable, represented with a Type and a name.
 */
public class Variable implements Typable {

	private final String name;
	private Type type;
	private boolean isFinal;
	
	public Variable(Type type, String name) {
		this.type = type;
		this.name = name;
		isFinal = false;
	}

	public boolean isFinal() {
		if(isFinal)
			return true;
		if(type == null)
			return false;
		return type.isFinal();
	}

	public void forceFinal() {
		isFinal = true;
	}

	public String getName() {
		return name;
	}
	
	public boolean isTypeKnown() {
		return type != null;
	}
	
	public void setType(Type type) {
		if(Objects.equals(this.type, type))
			return;
		if(isTypeKnown())
			JamLogger.warning("Tried to change the type of an already-known variable. " + name + " has type " + this.type + ", wanted to set to " + type);
		else
			this.type = type;
	}
	
	@Override
	public Type getType() {
		if(isFinal && type != null)
			type.setFinal();
		return type;
	}
	
	@Override
	public String toString() {
		return "Var<'" + name + "', " + (isTypeKnown() ? "type="+type:"?") + ">";
	}
}
