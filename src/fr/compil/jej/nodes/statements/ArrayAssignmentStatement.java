package fr.compil.jej.nodes.statements;

import fr.compil.jej.compilers.Visiteur;
import fr.compil.jej.core.TypesContext;
import fr.compil.jej.nodes.expressions.ArrayGetterExpression;
import fr.compil.jej.nodes.expressions.ExpressionNode;
import fr.compil.jej.nodes.expressions.NullExpression;
import fr.jamailun.jamlogger.JamLogger;

public class ArrayAssignmentStatement extends StatementNode {

	private final ArrayGetterExpression arrayGetter;
	private final ExpressionNode assignation;

	public ArrayAssignmentStatement(ArrayGetterExpression arrayGetter, ExpressionNode assignation) {
		this.arrayGetter = arrayGetter;
		this.assignation = assignation;
	}
	
	public ArrayGetterExpression getAssignedArrayValue() {
		return arrayGetter;
	}
	
	public ExpressionNode getAssignation() {
		return assignation;
	}
	
	@Override
	public String toString() {
		return "ArraySET{" + arrayGetter + " <-- " + assignation + "}";
	}
	
	@Override
	public boolean validateTypes(TypesContext context) {
		// Validate index and check type of index is INTEGER
		if(!arrayGetter.validateType(context))
			return false;
		// Validate assignation
		if(!assignation.validateType(context))
			return false;
		// check type
		if(assignation != NullExpression.NULL) {
			if(!arrayGetter.getType().equals(assignation.getType())) {
				JamLogger.error("Type of assignation " + assignation + " is " + assignation.getType() + " but should be " + arrayGetter.getType() + ".");
				return false;
			}
		}
		return true;
	}

	@Override
	public void accept(Visiteur visiteur) {
		visiteur.acceptArrayAssignmentStatement(this);
	}
}
