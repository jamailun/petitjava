package fr.compil.jej.nodes.statements;

import fr.compil.jej.compilers.Visiteur;
import fr.compil.jej.core.TypesContext;
import fr.compil.jej.core.Type;
import fr.compil.jej.nodes.Variable;
import fr.jamailun.jamlogger.JamLogger;

public class DeclarationStatement extends StatementNode {
	
	private final Variable declaredVariable;
	
	public DeclarationStatement(boolean isFinal, Type type, String name) {
		this.declaredVariable = new Variable(type, name);
		if(isFinal)
			declaredVariable.getType().setFinal();
	}
	public DeclarationStatement(Variable declaredVariable) {
		this.declaredVariable = declaredVariable;
	}
	
	public Variable getDeclaredVariable() {
		return declaredVariable;
	}
	
	@Override
	public String getName() {
		return declaredVariable.getName();
	}
	
	@Override
	public String toString() {
		return "Declaration{" + declaredVariable + "}";
	}
	
	@Override
	public boolean validateTypes(TypesContext context) {
		if(declaredVariable.getType().shouldBeLinked()) {
			Type linkedType = context.getType(declaredVariable.getType().getClassName());
			if(linkedType == null) {
				JamLogger.error("Unknown type : '" + declaredVariable.getType().getClassName() + "'.");
				return false;
			}
			declaredVariable.getType().linkClass(linkedType.getLinkedMethodsOwner());
		}
		if(!context.canBeRedefinedInBlock(declaredVariable.getName())) {
			JamLogger.error("Could not declare variable " + declaredVariable.getName() + " : it already has been declared !");
			return false;
		}
		context.register(declaredVariable.getName(), declaredVariable.getType(), TypesContext.VariableContext.DECLARATION);
		return true; // déclaration == on connait les types
	}

	@Override
	public void accept(Visiteur visiteur) {
		visiteur.acceptDeclarationStatement(this);
	}
}
