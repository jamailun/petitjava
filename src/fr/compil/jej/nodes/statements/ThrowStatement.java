package fr.compil.jej.nodes.statements;

import fr.compil.jej.compilers.Visiteur;
import fr.compil.jej.core.TypesContext;
import fr.compil.jej.nodes.expressions.ExpressionNode;
import fr.compil.jej.std.FakeException;
import fr.jamailun.jamlogger.JamLogger;

public class ThrowStatement extends StatementNode {
	
	private final ExpressionNode throwedExpression;
	
	public ThrowStatement(ExpressionNode throwedExpression) {
		this.throwedExpression = throwedExpression;
	}
	
	public ExpressionNode getThrowedExpression() {
		return throwedExpression;
	}
	
	@Override
	public boolean validateTypes(TypesContext context) {
		if(!throwedExpression.validateType(context)) {
			JamLogger.error("Could not validate expression of throwed element.");
			return false;
		}
		if(!throwedExpression.getType().getLinkedMethodsOwner().isSubtype(FakeException.FAKE_EXCEPTION)) {
			JamLogger.error("'Throw' statement must have a type deriving from Exception.");
			return false;
		}
		return true;
	}
	
	@Override
	public void accept(Visiteur visiteur) {
		visiteur.acceptThrowStatement(this);
	}
	
	@Override
	public String toString() {
		return "THROW{" + throwedExpression + "}";
	}
}
