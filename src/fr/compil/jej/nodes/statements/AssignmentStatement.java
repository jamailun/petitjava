package fr.compil.jej.nodes.statements;

import fr.compil.jej.compilers.Visiteur;
import fr.compil.jej.core.TypesContext;
import fr.compil.jej.core.Type;
import fr.compil.jej.nodes.expressions.ExpressionNode;
import fr.compil.jej.nodes.Variable;
import fr.compil.jej.nodes.expressions.FieldCallExpression;
import fr.compil.jej.nodes.expressions.NullExpression;
import fr.compil.jej.nodes.expressions.ThisExpression;
import fr.jamailun.jamlogger.JamLogger;

public class AssignmentStatement extends StatementNode {
	
	private final Variable assignedVariable;
	private final ExpressionNode assignation;

	// En fait, on peut se rendre compte que même sans THIS, le bail le réfère.
	private AssignmentFieldStatement sameAsField;
	
	public AssignmentStatement(Variable assignedVariable, ExpressionNode assignation) {
		name = "assign_" + assignedVariable.getName();
		this.assignedVariable = assignedVariable;
		this.assignation = assignation;
	}
	
	public Variable getAssignedVariable() {
		return assignedVariable;
	}
	
	public ExpressionNode getAssignation() {
		return assignation;
	}
	
	@Override
	public String toString() {
		return "Assignment{" + assignedVariable + " <-- " + assignation + "}";
	}
	
	@Override
	public boolean validateTypes(TypesContext context) {
		// 1 : récupérer la variable assignée
		if(!context.hasVariable(assignedVariable.getName())) {
			JamLogger.error("Unknown assigned variable : '" + assignedVariable.getName() + "'.");
			return false;
		}
		Type assignedType = context.getType(assignedVariable.getName());
		if(context.isField(assignedVariable.getName())) {
			FieldCallExpression sameAsFieldCall = new FieldCallExpression(getAssignedVariable().getName());
			sameAsFieldCall.setCallable(new ThisExpression());
			sameAsField = new AssignmentFieldStatement(sameAsFieldCall, getAssignation());
			if(!sameAsField.validateTypes(context)) {
				JamLogger.error("This is not supposed to happen in AssignementStatement#validateTypes : " + this);
				return false;
			}
		}
		assignedVariable.setType(assignedType);

		if(assignedVariable.isFinal()) {
			JamLogger.error("Variable " + assignedVariable.getName() + " has been declared as 'final', but an assignment exists : " + this + ".");
			return false;
		}
		
		// 2 : vérifier le type de l'expression assignante.
		if(!assignation.validateType(context)) // on ajoute vérifie que l'expression utilise des champs connus
			return false;
		// on vérifie que l'expression ait le bon type
		if(assignation != NullExpression.NULL) {
			if (!assignation.getType().equals(assignedType)) {
				JamLogger.error("Type of assigned variable " + assignedVariable.getName() + " should be " + assignedType + ", but is " + assignation.getType());
				return false;
			}
		}
		return true;
	}

	@Override
	public void accept(Visiteur visiteur) {
		if(sameAsField  != null) {
			visiteur.acceptAssignmentFieldStatement(sameAsField);
		} else {
			visiteur.acceptAssignmentStatement(this);
		}
	}
}
