package fr.compil.jej.nodes.statements;

import fr.compil.jej.compilers.Visiteur;
import fr.compil.jej.core.Type;
import fr.compil.jej.core.TypesContext;
import fr.compil.jej.core.Utils;
import fr.jamailun.jamlogger.JamLogger;
import java.util.List;
import java.util.Map;

public class CatchStatement extends BlockStatements {
	
	private final String exceptionName;
	private String cExceptionValue;
	private final String variableName;
	private List<String> subclassesToCatch;
	
	public CatchStatement(String exceptionName, String variableName, List<StatementNode> statements) {
		super(statements);
		this.exceptionName = exceptionName;
		this.variableName = variableName;
	}
	
	public String getExceptionName() {
		return exceptionName;
	}
	
	public String getCValue() {
		return cExceptionValue;
	}
	
	public List<String> getSubclassesToCatch() {
		return subclassesToCatch;
	}
	
	@Override
	protected boolean validateSelfType(TypesContext context) {
		Type exceptionType = context.getType(exceptionName);
		if(exceptionType == null || exceptionType.getLinkedClass() == null) {
			JamLogger.error("Could not identify catch type '" + exceptionName + "'.");
			return false;
		}
		if(!exceptionType.isValidExceptionType()) {
			JamLogger.error("CATCH '" + exceptionName + "' is not valid.");
			return false;
		}
		cExceptionValue = exceptionType.getLinkedClass().getName();
		subclassesToCatch = context.getAllSubclasses(exceptionType);
		context.register(variableName, exceptionType, TypesContext.VariableContext.DECLARATION);
		return true;
	}
	
	@Override
	public String niceString(int indent) {
		return Utils.appendStuff(
				indent,
				"CATCH",
				"exception="+exceptionName,
				Map.entry("content", super.getStatements())
		);
	}
	
	@Override
	public void accept(Visiteur visiteur) {
		visiteur.acceptCatchStatement(this);
	}
}
