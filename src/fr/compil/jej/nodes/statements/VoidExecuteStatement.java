package fr.compil.jej.nodes.statements;

import fr.compil.jej.compilers.Visiteur;
import fr.compil.jej.core.TypesContext;
import fr.compil.jej.nodes.expressions.ExpressionNode;

public class VoidExecuteStatement extends StatementNode {
	
	private final ExpressionNode expression;
	
	public VoidExecuteStatement(ExpressionNode expression) {
		this.name = "_execution_line";
		this.expression = expression;
	}
	
	public ExpressionNode getExpression() {
		return expression;
	}
	
	@Override
	public String toString() {
		return "Expr{" + expression + '}';
	}
	
	@Override
	public boolean validateTypes(TypesContext context) {
		return expression.validateType(context);
	}

	@Override
	public void accept(Visiteur visiteur) {
		visiteur.acceptVoidStatement(this);
	}
}
