package fr.compil.jej.nodes.statements;

import fr.compil.jej.compilers.Visiteur;
import fr.compil.jej.core.StreamTokens;
import fr.compil.jej.core.TokenType;
import fr.compil.jej.core.TypesContext;
import fr.compil.jej.core.Type;
import fr.compil.jej.errors.BadTokenError;
import fr.compil.jej.errors.SyntaxError;
import fr.compil.jej.nodes.expressions.ExpressionNode;

public class ReturnStatement extends StatementNode {
	
	private final ExpressionNode returnedValue;
	
	public ReturnStatement() {
		returnedValue = null;
	}
	public ReturnStatement(ExpressionNode returnedValue) {
		this.returnedValue = returnedValue;
	}
	
	public boolean isEmpty() {
		return returnedValue == null;
	}
	
	public ExpressionNode getReturnedValue() {
		return returnedValue;
	}
	
	public Type getReturnedType() {
		if(isEmpty())
			return Type.TYPE_VOID;
		return getReturnedValue().getType();
	}
	
	@Override
	public String toString() {
		return "RETURN{" + (isEmpty()?"":returnedValue) + "}";
	}
	@Override
	public boolean validateTypes(TypesContext context) {
		if(returnedValue == null)
			return true;
		return returnedValue.validateType(context);
	}

	public static ReturnStatement generate(StreamTokens tokens) throws BadTokenError, SyntaxError {
		if(tokens.isFuture(TokenType.SEMICOLON)) {
			tokens.junk();
			return new ReturnStatement();
		}
		ExpressionNode returnedValue = ExpressionNode.generate(tokens);
		tokens.junkOrThrow(TokenType.SEMICOLON, "Expected ';' after RETURN <expression>.");
		return new ReturnStatement(returnedValue);
	}
	
	@Override
	public void accept(Visiteur visiteur) {
		visiteur.acceptReturnStatement(this);
	}
}
