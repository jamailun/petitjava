package fr.compil.jej.nodes.statements;

import fr.compil.jej.compilers.Visiteur;
import fr.compil.jej.core.PreviousToken;
import fr.compil.jej.core.StreamTokens;
import fr.compil.jej.core.TokenType;
import fr.compil.jej.core.Type;
import fr.compil.jej.core.TypesContext;
import fr.compil.jej.core.Utils;
import fr.compil.jej.errors.BadTokenError;
import fr.compil.jej.errors.SyntaxError;
import fr.compil.jej.nodes.expressions.ExpressionNode;
import fr.compil.jej.nodes.expressions.logicparse.LogicParser;
import fr.jamailun.jamlogger.JamLogger;

import java.util.Collections;
import java.util.List;
import java.util.Map;

public class ForStatement extends BlockStatements {
	
	private final StatementNode beginStatement;
	private final ExpressionNode condition;
	private final StatementNode loopStatement;
	
	public ForStatement(StatementNode beginStatement, ExpressionNode condition, StatementNode loopStatement, List<StatementNode> statements) {
		super(statements);
		this.beginStatement = beginStatement;
		this.condition = condition;
		this.loopStatement = loopStatement;
	}
	
	public StatementNode getBeginStatement() {
		return beginStatement;
	}
	
	public ExpressionNode getCondition() {
		return condition;
	}
	
	public StatementNode getLoopExpression() {
		return loopStatement;
	}
	
	@PreviousToken(allowed = {TokenType.FOR})
	public static ForStatement generate(StreamTokens tokens) throws BadTokenError, SyntaxError {
		// Base du while
		tokens.junkOrThrow(TokenType.LEFT_PAREN, "Expected a '(' after the 'for' keyword.");
		StatementNode forBegin = StatementNode.generate(tokens);
		ExpressionNode forCondition = LogicParser.parse(tokens);
		tokens.junkOrThrow(TokenType.SEMICOLON, "Expected a semicolon after the condition-statement of the for loop.");
		StatementNode forEnd = StatementNode.generate(tokens);
		tokens.junkOrThrow(TokenType.RIGHT_PAREN, "Expected a ')' after a for block.");
		// Contenu du while
		List<StatementNode> content;
		if(tokens.isFuture(TokenType.LEFT_BRACES)) {
			tokens.junk();
			content = StatementNode.readStatementsBlock(tokens);
		} else {
			content = Collections.singletonList(StatementNode.generate(tokens));
		}
		return new ForStatement(forBegin, forCondition, forEnd, content);
	}
	
	@Override
	protected boolean validateSelfType(TypesContext context) {
		if(!beginStatement.validateTypes(context))
			return false;
		if(!loopStatement.validateTypes(context))
			return false;
		if(!condition.validateType(context))
			return false;
		if(!condition.getType().equals(Type.Primitive.BOOLEAN.asType())) {
			JamLogger.error("2nd statement in for loop has to be a condition (boolean). Current type : " + condition.getType());
			return false;
		}
		return true;
	}
	
	@Override
	public String niceString(int indent) {
		return Utils.appendStuff(
				indent,
				"FOR",
				getBeginStatement() + ", "+getCondition() + ", " + getLoopExpression(),
				Map.entry("content", super.getStatements())
		);
	}
	
	@Override
	public void accept(Visiteur visiteur) {
		visiteur.acceptForStatement(this);
	}
	
}
