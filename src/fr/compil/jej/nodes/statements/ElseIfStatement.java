package fr.compil.jej.nodes.statements;

import fr.compil.jej.nodes.expressions.ExpressionNode;

import java.util.List;

public class ElseIfStatement extends IfStatement {
	
	public ElseIfStatement(ExpressionNode condition, List<StatementNode> statements) {
		super(condition, statements);
	}
	
}
