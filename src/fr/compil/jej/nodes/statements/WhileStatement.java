package fr.compil.jej.nodes.statements;

import fr.compil.jej.compilers.Visiteur;
import fr.compil.jej.core.PreviousToken;
import fr.compil.jej.core.StreamTokens;
import fr.compil.jej.core.TokenType;
import fr.compil.jej.core.Utils;
import fr.compil.jej.errors.BadTokenError;
import fr.compil.jej.errors.SyntaxError;
import fr.compil.jej.nodes.expressions.ExpressionNode;
import fr.compil.jej.nodes.expressions.logicparse.LogicParser;

import java.util.Collections;
import java.util.List;
import java.util.Map;

public class WhileStatement extends IfStatement {
	
	private final boolean doForm;
	
	public WhileStatement(ExpressionNode condition, List<StatementNode> statements, boolean doForm) {
		super(condition, statements);
		this.doForm = doForm;
	}
	
	public boolean isDoForm() {
		return doForm;
	}
	
	@PreviousToken(allowed = {TokenType.WHILE})
	public static WhileStatement generateWhile(StreamTokens tokens) throws BadTokenError, SyntaxError {
		// Base du while
		tokens.junkOrThrow(TokenType.LEFT_PAREN, "Expected a '(' after the 'while' keyword.");
		ExpressionNode whileCondition = LogicParser.parse(tokens);
		tokens.junkOrThrow(TokenType.RIGHT_PAREN, "Expected a ')' after a while condition.");
		// Contenu du while
		List<StatementNode> content;
		if(tokens.isFuture(TokenType.LEFT_BRACES)) {
			tokens.junk();
			content = StatementNode.readStatementsBlock(tokens);
		} else {
			content = Collections.singletonList(StatementNode.generate(tokens));
		}
		return new WhileStatement(whileCondition, content, false);
	}
	
	@PreviousToken(allowed = {TokenType.DO})
	public static WhileStatement generateDo(StreamTokens tokens) throws BadTokenError, SyntaxError {
		// Do block
		List<StatementNode> content;
		if(tokens.isFuture(TokenType.LEFT_BRACES)) {
			tokens.junk();
			content = StatementNode.readStatementsBlock(tokens);
		} else {
			content = Collections.singletonList(StatementNode.generate(tokens));
		}
		// 'do' keyword
		tokens.junkOrThrow(TokenType.WHILE, "Expected a 'while' keyword after a 'do' block.");
		// while condition
		tokens.junkOrThrow(TokenType.LEFT_PAREN, "Expected a '(' after the 'while' keyword.");
		ExpressionNode whileCondition = LogicParser.parse(tokens);
		tokens.junkOrThrow(TokenType.RIGHT_PAREN, "Expected a ')' after a while condition.");
		return new WhileStatement(whileCondition, content, true);
	}
	
	@Override
	public String niceString(int indent) {
		String ifString = Utils.appendStuff(
				indent,
				doForm ? "DO+":"" + "WHILE",
				"condition="+getCondition(),
				Map.entry("content", super.getStatements())
		);
		if(hasChild())
			ifString += " " + getChild().niceString(indent);
		return ifString;
	}
	
	@Override
	public void accept(Visiteur visiteur) {
		visiteur.acceptWhileStatement(this);
	}
	
}
