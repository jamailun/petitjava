package fr.compil.jej.nodes.statements;

import fr.compil.jej.compilers.Visiteur;
import fr.compil.jej.core.PreviousToken;
import fr.compil.jej.core.StreamTokens;
import fr.compil.jej.core.TokenType;
import fr.compil.jej.core.TypesContext;
import fr.compil.jej.core.Type;
import fr.compil.jej.core.Utils;
import fr.compil.jej.errors.BadTokenError;
import fr.compil.jej.errors.SyntaxError;
import fr.compil.jej.nodes.expressions.ExpressionNode;
import fr.compil.jej.nodes.expressions.logicparse.LogicParser;
import fr.jamailun.jamlogger.JamLogger;

import java.util.Collections;
import java.util.List;
import java.util.Map;

public class IfStatement extends BlockStatements {
	
	private final ExpressionNode condition;
	private BlockStatements child;
	
	public IfStatement(ExpressionNode condition, List<StatementNode> statements) {
		super(statements);
		this.condition = condition;
	}
	
	public boolean hasChild() {
		return child != null;
	}
	
	public BlockStatements getChild() {
		return child;
	}
	
	public void setChild(BlockStatements child) {
		this.child = child;
	}
	
	public ExpressionNode getCondition() {
		return condition;
	}
	
	@Override
	protected boolean validateSelfType(TypesContext context) {
		TypesContext clone = context.inherit();
		if(!condition.validateType(context))
			return false;
		if(!condition.getType().equals(Type.Primitive.BOOLEAN.asType())) {
			JamLogger.error("IF statement must have a boolean expression as a condition. Current type : " + condition.getType());
			return false;
		}
		if(hasChild())
			return getChild().validateTypes(clone);
		return true;
	}

	@PreviousToken(allowed = {TokenType.IF})
	public static IfStatement generate(StreamTokens tokens) throws BadTokenError, SyntaxError {
		tokens.junkOrThrow(TokenType.LEFT_PAREN, "Expected a '(' after an 'if'.");
		ExpressionNode ifCondition = LogicParser.parse(tokens);
		tokens.junkOrThrow(TokenType.RIGHT_PAREN, "Expected a ')' after a if condition.");
		
		List<StatementNode> ifContent;
		if(tokens.isFuture(TokenType.LEFT_BRACES)) {
			// Cas de groupe
			tokens.junk();
			ifContent = readStatementsBlock(tokens);
		} else {
			ifContent = Collections.singletonList(StatementNode.generate(tokens));
		}
		
		IfStatement ifStatement = new IfStatement(ifCondition, ifContent);
		
		if(tokens.isFuture(TokenType.ELSE)) {
			tokens.junk();
			ElseStatement elseStatement = ElseStatement.generate(tokens);
			ifStatement.setChild(elseStatement);
		}
		return ifStatement;
	}
	
	@Override
	public String niceString(int indent) {
		String ifString = Utils.appendStuff(
				indent,
				"IF",
				"condition="+getCondition(),
				Map.entry("content", super.getStatements())
		);
		if(hasChild())
			ifString += " " + getChild().niceString(indent);
		return ifString;
	}
	
	@Override
	public void accept(Visiteur visiteur) {
		visiteur.acceptIfStatement(this);
	}
}
