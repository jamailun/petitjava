package fr.compil.jej.nodes.statements;

import fr.compil.jej.compilers.Visiteur;
import fr.compil.jej.core.*;
import fr.compil.jej.errors.BadTokenError;
import fr.compil.jej.errors.SyntaxError;
import fr.compil.jej.nodes.Node;
import fr.compil.jej.nodes.Variable;
import fr.compil.jej.nodes.expressions.*;

import java.util.LinkedList;
import java.util.List;

public abstract class StatementNode extends Node {
	
	public abstract boolean validateTypes(TypesContext context);
	
	private static DeclarationStatement generateDeclaration(boolean isFinal, Type.Primitive primitive, StreamTokens tokens) throws BadTokenError, SyntaxError {
		return generateDeclaration(isFinal, new Type(primitive), tokens);
	}
	
	private static DeclarationStatement generateDeclaration(boolean isFinal, Type type, StreamTokens tokens) throws BadTokenError, SyntaxError {
		if(tokens.isFutureConsume(TokenType.LEFT_BRACKET)) {
			type = type.toArray();
			tokens.junkOrThrow(TokenType.RIGHT_BRACKET, "Expected a ']' after 'TYPE['.");
		}
		while (tokens.isFutureConsume(TokenType.LEFT_BRACKET)) {
			((ArrayType)type).addDimension();
			tokens.junkOrThrow(TokenType.RIGHT_BRACKET, "Expected a ']' after 'TYPE['.");
		}
		String name = tokens.readIdentifier();
		// Pour le moment, on se contente de dire que ça suffit avec un "TYPE NAME ;"
		TokenType nextType = tokens.next().getType();
		if(nextType == TokenType.EQUAL) {
			// = : assignment declaration
			ExpressionNode expression = ExpressionNode.generate(tokens);
			if(tokens.isFuture(TokenType.DOT)) {
				tokens.junk();
				ExpressionNode suite = ExpressionNode.generate(tokens);
				if(suite instanceof CallableNode) {
					CallableNode rightOperator = (CallableNode) suite;
					rightOperator.setCallable(expression);
					expression =  rightOperator;
				} else if(suite instanceof ObjectRefExpression) {
					CallableNode rightOperator = new FieldCallExpression(suite.getName());
					rightOperator.setCallable(expression);
					expression = rightOperator;
				} else {
					throw new BadTokenError(tokens.peek(), "After <DECLARATION = <A>.<B>... expected field or method call.");
				}
			}
			tokens.junkOrThrow(TokenType.SEMICOLON, "Expected semicolon after declaration.");
			return new DeclarationAssignmentStatement(new DeclarationStatement(isFinal, type, name), expression);
		} else if(nextType == TokenType.SEMICOLON) {
			// ; : declaration.
			return new DeclarationStatement(isFinal, type, name);
		}
		throw new BadTokenError(tokens.peek(), "Unexpected token after declaration of variable '" + name + "'.");
	}
	
	public static List<StatementNode> readStatementsBlock(StreamTokens tokens) throws BadTokenError, SyntaxError {
		List<StatementNode> statements = new LinkedList<>();
		// on part du principe que le "{" a déjà été lu !
		while(tokens.isNotFuture(TokenType.RIGHT_BRACES)) {
			statements.add(generate(tokens));
		}
		tokens.junkOrThrow(TokenType.RIGHT_BRACES, "expected '}' after method content.");
		return statements;
	}
	
	public static StatementNode generate(StreamTokens tokens) throws BadTokenError, SyntaxError {
		Token token = tokens.next();
		switch (token.getType()) {
			case FINAL: // On ignore les 'FINAL' depuis la generation : la donnée sera récupérable a posteriori.
				return generate(tokens);
			case LEFT_BRACES:
				return new BlockStatements(readStatementsBlock(tokens));
			case TYPE_INT:
			case TYPE_BOOLEAN:
			case TYPE_DOUBLE:
			case TYPE_STRING:
			case TYPE_CHAR:
				boolean isPreviousFinal = tokens.isPostPreviousFinal();
				if(tokens.isFuture(TokenType.DOUBLE_ADD) || tokens.isFuture(TokenType.DOUBLE_SUB)) {
					if(isPreviousFinal)
						throw new BadTokenError(token, "Unexpected token after a FINAL keyword.");
					TokenType operator = tokens.next().getType();
					ExpressionNode next = ExpressionNode.generate(tokens);
					tokens.junkOrThrow(TokenType.SEMICOLON, "Expected semicolon after double operator " + next + " " + operator);
					return new VoidExecuteStatement(new DoubleOperatedExpression(operator, next, false));
				}
				return generateDeclaration(isPreviousFinal, Type.Primitive.from(token), tokens);
			case IF:
				return IfStatement.generate(tokens);
			case RETURN:
				return ReturnStatement.generate(tokens);
			case NEW:
				return NewStatement.generate(tokens);
			case DOUBLE_ADD:
			case DOUBLE_SUB:
				VoidExecuteStatement wrapperStt = new VoidExecuteStatement(ExpressionNode.generate(token, tokens));
				tokens.junkOrThrow(TokenType.SEMICOLON, "Expected a semicolon after " + wrapperStt);
				return wrapperStt;
			case IDENTIFIER:
				// "NAME++"
				boolean finalBefore = tokens.isPostPreviousFinal();
				if(tokens.isFuture(TokenType.DOUBLE_ADD) || tokens.isFuture(TokenType.DOUBLE_SUB)) {
					if(finalBefore)
						throw new BadTokenError(token, "Unexpected token after a FINAL keyword.");
					TokenType operator = tokens.next().getType();
					if(!tokens.isFuture(TokenType.RIGHT_PAREN))
						tokens.junkOrThrow(TokenType.SEMICOLON, "Expected semicolon after double operator " + token.getValString() + " " + operator);
					return new VoidExecuteStatement(new DoubleOperatedExpression(operator, new ObjectRefExpression(token.getValString()), false));
				}
				// "TYPE NAME ..." OU "TYPE[] NAME..."
				if(tokens.doesFollow(TokenType.LEFT_BRACKET, TokenType.RIGHT_BRACKET, TokenType.IDENTIFIER) || tokens.isFuture(TokenType.IDENTIFIER)) {
					Type declarationType = new Type(token.getValString());
					return generateDeclaration(finalBefore, declarationType, tokens);
				}
				ExpressionNode leftExpr = ExpressionNode.generate(token, tokens);
				//JamLogger.log("identifier expression on left : " + leftExpr);
				return buildOnLeft(leftExpr, tokens);
			case THIS:
				//TODO '(' == appeler un autre constructeur
				if(tokens.isFutureConsume(TokenType.DOT)) {
					ExpressionNode next = ExpressionNode.generate(tokens);
					CallableNode nextThis = ExpressionNode.insertAtCallableSource(next, new ThisExpression());
					return buildOnLeft(nextThis, tokens);
				}
				throw new BadTokenError(tokens.peek(), "Unexpected token after a 'this' at new statement.");
			case WHILE:
				return WhileStatement.generateWhile(tokens);
			case DO:
				return WhileStatement.generateDo(tokens);
			case FOR:
				return ForStatement.generate(tokens);
			case TRY:
				return TryCatchStatement.generate(tokens);
			case THROW:
				ExpressionNode throwedExpression = ExpressionNode.generate(tokens);
				tokens.junkOrThrow(TokenType.SEMICOLON, "Expected a ';' after a THROW statement.");
				return new ThrowStatement(throwedExpression);
		}
		throw new BadTokenError(token, "Unexpected token for a statement.");
	}
	
	private static StatementNode buildOnLeft(ExpressionNode leftExpr, StreamTokens tokens) throws BadTokenError, SyntaxError {
		/// TRUC = ...
		if(tokens.isFutureConsume(TokenType.EQUAL)) {
			Token debugError = tokens.peek();
			ExpressionNode rightExpr = ExpressionNode.generate(tokens);

			//JamLogger.log("rightExpr = " + rightExpr);

			if(!tokens.isFuture(TokenType.RIGHT_PAREN) && !tokens.isFuture(TokenType.DOT))
				tokens.junkOrThrow(TokenType.SEMICOLON, "Expected semicolon after an assignment.");
			if(leftExpr instanceof ObjectRefExpression)
				return new AssignmentStatement(
						new Variable(null, leftExpr.getName()),
						rightExpr
				);
			if(leftExpr instanceof FieldCallExpression)
				return new AssignmentFieldStatement((FieldCallExpression) leftExpr, rightExpr);
			if(leftExpr instanceof ArrayGetterExpression) {
				return new ArrayAssignmentStatement((ArrayGetterExpression) leftExpr, rightExpr);
			}
			throw new BadTokenError(debugError, "Cannot assign expression other than ObjectRef or fieldRef.");
		}
		// "TRUC." ou "TRUC[" ou "TRUC("
		VoidExecuteStatement st = new VoidExecuteStatement(leftExpr);
		tokens.junkOrThrow(TokenType.SEMICOLON, "Expected a semicolon after " + st);
		return st;
	}


	@Override
	public void accept(Visiteur visiteur) {
		throw new RuntimeException("pas encore implémenté pour " + getClass().getSimpleName() + ".");
	}
	
}
