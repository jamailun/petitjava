package fr.compil.jej.nodes.statements;

import fr.compil.jej.compilers.Visiteur;
import fr.compil.jej.core.PreviousToken;
import fr.compil.jej.core.StreamTokens;
import fr.compil.jej.core.TokenType;
import fr.compil.jej.errors.BadTokenError;
import fr.compil.jej.errors.SyntaxError;

import java.util.Collections;
import java.util.List;

public class ElseStatement extends BlockStatements {
	
	public ElseStatement(List<StatementNode> statements) {
		super(statements);
	}
	
	@PreviousToken(allowed = {TokenType.ELSE})
	public static ElseStatement generate(StreamTokens tokens) throws BadTokenError, SyntaxError {
		List<StatementNode> elseContent;
		if(tokens.isFuture(TokenType.LEFT_BRACES)) {
			tokens.junk();
			elseContent = readStatementsBlock(tokens);
		} else {
			elseContent = Collections.singletonList(StatementNode.generate(tokens));
		}
		return new ElseStatement(elseContent);
	}
	
	@Override
	public void accept(Visiteur visiteur) {
		visiteur.acceptElseStatement(this);
	}
}
