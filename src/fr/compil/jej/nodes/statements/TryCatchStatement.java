package fr.compil.jej.nodes.statements;

import fr.compil.jej.compilers.Visiteur;
import fr.compil.jej.core.PreviousToken;
import fr.compil.jej.core.StreamTokens;
import fr.compil.jej.core.TokenType;
import fr.compil.jej.core.TypesContext;
import fr.compil.jej.core.Utils;
import fr.compil.jej.errors.BadTokenError;
import fr.compil.jej.errors.SyntaxError;
import fr.jamailun.jamlogger.JamLogger;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class TryCatchStatement extends BlockStatements {
	
	private final List<CatchStatement> catches;
	private final List<StatementNode> finallyContent;
	
	public TryCatchStatement(List<StatementNode> tryContent, List<CatchStatement> catches, List<StatementNode> finallyContent) {
		super(tryContent);
		this.catches = catches;
		this.finallyContent = finallyContent;
	}
	
	public List<CatchStatement> getCatches() {
		return catches;
	}
	
	public List<StatementNode> getFinallyContent() {
		return finallyContent;
	}
	
	@Override
	protected boolean validateSelfType(TypesContext context) {
		Set<String> takenExceptions = new HashSet<>();
		for(CatchStatement cs : catches) {
			TypesContext childContext = context.inherit();
			if(!cs.validateTypes(childContext)) {
				JamLogger.error("Catch block invalid in a try-catch.");
				return false;
			}
			if(!takenExceptions.add(cs.getExceptionName())) {
				JamLogger.error("In a multiple catch-blocks, handle exception '" + cs.getExceptionName() +"' multiple times.");
				return false;
			}
		}
		// Test si le block finally est intrinsèquement valide.
		TypesContext childContext = context.inherit();
		if(!finallyContent.stream().allMatch(c -> c.validateTypes(childContext))) {
			JamLogger.error("Invalid 'finally' block in a try-catch statement.");
			return false;
		}
		return true;
	}

	@PreviousToken(allowed = {TokenType.TRY})
	public static TryCatchStatement generate(StreamTokens tokens) throws BadTokenError, SyntaxError {
		tokens.junkOrThrow(TokenType.LEFT_BRACES, "Expected a '{' after an 'try'.");
		List<StatementNode> tryContent = StatementNode.readStatementsBlock(tokens);
		// Create all the CATCH
		List<CatchStatement> catches = new ArrayList<>();
		tokens.junkOrThrow(TokenType.CATCH, "Need a 'catch' statement after a 'try' keyword.");
		do {
			tokens.junkOrThrow(TokenType.LEFT_PAREN, "A 'catch' must be followed by an in-parenthesis expression");
			String exceptionName = tokens.readIdentifier();
			String variableName = tokens.readIdentifier();
			tokens.junkOrThrow(TokenType.RIGHT_PAREN, "Expected ')' after \"catch " + exceptionName + " " + variableName + "\".");
			tokens.junkOrThrow(TokenType.LEFT_BRACES, "Expected a '{' after an 'catch' expression.");
			List<StatementNode> catchContent = StatementNode.readStatementsBlock(tokens);
			catches.add(new CatchStatement(exceptionName, variableName, catchContent));
		} while(tokens.isFutureConsume(TokenType.CATCH));
		// Try get the FINALLY
		List<StatementNode> finallyContent = new LinkedList<>();
		if(tokens.isFutureConsume(TokenType.FINALLY)) {
			tokens.junkOrThrow(TokenType.LEFT_BRACES, "Expected a '{' after an 'finally' keyword.");
			finallyContent.addAll(StatementNode.readStatementsBlock(tokens));
		}
		// Create the instance and return it.
		return new TryCatchStatement(tryContent, catches, finallyContent);
	}
	
	@Override
	public String niceString(int indent) {
		StringBuilder niceString = new StringBuilder(Utils.appendStuff(
				indent,
				"TRY",
				Map.entry("content", super.getStatements())
		));
		for(CatchStatement ca : catches)
			niceString.append(" ").append(ca.niceString(indent));
		if(!finallyContent.isEmpty())
			niceString.append(new BlockStatements(finallyContent).niceString(indent));
		return niceString.toString();
	}
	
	@Override
	public void accept(Visiteur visiteur) {
		visiteur.acceptTryCatchStatement(this);
	}
}
