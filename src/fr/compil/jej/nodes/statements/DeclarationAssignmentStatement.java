package fr.compil.jej.nodes.statements;

import fr.compil.jej.compilers.Visiteur;
import fr.compil.jej.core.TypesContext;
import fr.compil.jej.nodes.expressions.ExpressionNode;
import fr.compil.jej.nodes.expressions.NullExpression;
import fr.jamailun.jamlogger.JamLogger;

public class DeclarationAssignmentStatement extends DeclarationStatement {
	
	private final ExpressionNode assignation;
	
	public DeclarationAssignmentStatement(DeclarationStatement declaration, ExpressionNode assignation) {
		super(declaration.getDeclaredVariable());
		this.assignation = assignation;
	}
	
	public ExpressionNode getAssignation() {
		return assignation;
	}
	
	@Override
	public String toString() {
		return "Declaration&Assignment{" + getDeclaredVariable() + " <-- " + assignation + "}";
	}
	
	@Override
	public boolean validateTypes(TypesContext context) {
		if (!super.validateTypes(context)) { // on ajoute le type déclaré
			JamLogger.error("Declaration&Assignment could not validate on SUPER.");
			return false;
		}
		if(!assignation.validateType(context)) { // on ajoute vérifie que l'expression utilise des champs connus
			JamLogger.error("Declaration&Assignment could not validate assignation " + assignation + ".");
			return false;
		}
		// on vérifie que l'expression ait le bon type
		if(assignation != NullExpression.NULL) {
			if(!assignation.getType().equals(getDeclaredVariable().getType())) {
				JamLogger.error("Type of assignation " + assignation + " should be " + getDeclaredVariable().getType() + ", but is " + assignation.getType());
				return false;
			}
		}
		if(getDeclaredVariable().isFinal()) {
			JamLogger.error("Variable " + getDeclaredVariable().getName() + " has been declared as 'final', but an assignment exists : " + this + ".");
			return false;
		}
		return true;
	}

	@Override
	public void accept(Visiteur visiteur) {
		visiteur.acceptDeclarationAssignmentStatement(this);
	}
}
