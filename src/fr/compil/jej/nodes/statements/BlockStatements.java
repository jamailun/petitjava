package fr.compil.jej.nodes.statements;

import fr.compil.jej.core.Printable;
import fr.compil.jej.core.TypesContext;
import fr.compil.jej.core.Utils;

import java.util.List;
import java.util.Map;

public class BlockStatements extends StatementNode implements Printable {
	
	private final List<StatementNode> statements;
	
	public BlockStatements(List<StatementNode> statements) {
		this.statements = statements;
	}
	
	public List<StatementNode> getStatements() {
		return statements;
	}
	
	protected boolean validateSelfType(TypesContext context) {
		return true;
	}
	
	@Override
	public final boolean validateTypes(TypesContext context) {
		TypesContext childContext = context.inherit();
		if(!validateSelfType(childContext))
			return false;
		return statements.stream().allMatch(s -> s.validateTypes(childContext));
	}
	
	@Override
	public String toString() {
		return niceString(0);
	}
	
	@Override
	public String niceString(int indent) {
		return Utils.appendStuff(
				indent,
				getClass().getSimpleName(),
				Map.entry("content", getStatements())
		);
	}
}
