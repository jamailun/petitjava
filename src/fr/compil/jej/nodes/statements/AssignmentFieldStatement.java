package fr.compil.jej.nodes.statements;

import fr.compil.jej.compilers.Visiteur;
import fr.compil.jej.core.TypesContext;
import fr.compil.jej.nodes.expressions.ExpressionNode;
import fr.compil.jej.nodes.expressions.FieldCallExpression;
import fr.compil.jej.nodes.expressions.NullExpression;
import fr.jamailun.jamlogger.JamLogger;

public class AssignmentFieldStatement extends StatementNode {
	
	private final FieldCallExpression fieldCall;
	private final ExpressionNode assignation;
	
	public AssignmentFieldStatement(FieldCallExpression fieldCall, ExpressionNode assignation) {
		this.fieldCall = fieldCall;
		this.assignation = assignation;
	}
	
	public FieldCallExpression getAssignedFieldCall() {
		return fieldCall;
	}
	
	public ExpressionNode getAssignation() {
		return assignation;
	}
	
	@Override
	public String toString() {
		return "Assignment{field=" + fieldCall + " <-- " + assignation + "}";
	}
	
	@Override
	public boolean validateTypes(TypesContext context) {
		// Valider le champ owner que nous contenons.
		if(!fieldCall.validateType(context)) {
			JamLogger.error("Impossible de valider le type appelé sur " + this + ".");
			return false;
		}
		// Vérifier que nous ne sommes pas en train d'assigner un type final
		if(fieldCall.getLinkedField().getType().isFinal()) {
			JamLogger.error("Field " + fieldCall.getLinkedField().getOwningClass().getName() + "#" + fieldCall.getFieldName() + " has been declared as 'final', but an assignment exists : " + this + ".");
			return false;
		}
		// Vérifier que l'assignation en question a le bon type.
		if(!assignation.validateType(context))
			return false;
		if(assignation != NullExpression.NULL) {
			if(!assignation.getType().equals(fieldCall.getType())) {
				JamLogger.error("L'assignation " + this + " requiert un type " + fieldCall.getType() + ", mais le type fourni est " + assignation.getType() + ".");
				return false;
			}
		}
		return true;
	}

	@Override
	public void accept(Visiteur visiteur) {
		visiteur.acceptAssignmentFieldStatement(this);
	}
}
