package fr.compil.jej.nodes.statements;

import fr.compil.jej.compilers.Visiteur;
import fr.compil.jej.core.StreamTokens;
import fr.compil.jej.core.TokenType;
import fr.compil.jej.core.TypesContext;
import fr.compil.jej.errors.BadTokenError;
import fr.compil.jej.errors.SyntaxError;
import fr.compil.jej.nodes.expressions.ExpressionNode;
import fr.compil.jej.nodes.expressions.MethodCallExpression;
import fr.compil.jej.nodes.expressions.NewExpression;

public class NewStatement extends StatementNode {
	
	private final NewExpression newExpression;
	
	public NewStatement(NewExpression newExpression) {
		this.newExpression = newExpression;
	}
	
	public ExpressionNode getNewExpression() {
		return newExpression;
	}
	
	@Override
	public String toString() {
		return "NEW{" + newExpression + "}";
	}
	
	@Override
	public boolean validateTypes(TypesContext context) {
		return newExpression.validateType(context);
	}

	public static StatementNode generate(StreamTokens tokens) throws BadTokenError, SyntaxError {
		NewExpression newExpr = NewExpression.generate(tokens);
		ExpressionNode newAsWappred = getContextualized(newExpr, tokens);
		// Si on a la même instance, c'est que la node n'est pas wrappée et peut être placée dans un newStatement.
		if(newAsWappred == newExpr)
			return new NewStatement(newExpr);
		return new VoidExecuteStatement(newAsWappred);
	}
	
	/**
	 * Peut transformer une newExpression Callable !
	 */
	public static ExpressionNode getContextualized(NewExpression newExpression, StreamTokens tokens) throws BadTokenError, SyntaxError {
		if(tokens.isFuture(TokenType.SEMICOLON)) {
			tokens.junk();
			return newExpression;
		}
		if(tokens.isFuture(TokenType.DOT)) {
			tokens.junk();
			ExpressionNode suite = ExpressionNode.generate(tokens);
			tokens.junkOrThrow(TokenType.SEMICOLON, "Expected a semicolon");
			if(suite instanceof MethodCallExpression) {
				MethodCallExpression rightOperande = (MethodCallExpression) suite;
				rightOperande.setCallable(newExpression);
				return rightOperande;
			} else {
				throw new BadTokenError(tokens.peek(), "For an expression, cannot 'call' a field (" + suite + ").");
			}
		}
		throw new BadTokenError(tokens.peek(), "Unexpected token after a new statement.");
	}
	
	@Override
	public void accept(Visiteur visiteur) {
		visiteur.acceptNewStatement(this);
	}
}
