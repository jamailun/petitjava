package fr.compil.jej.nodes;

import fr.compil.jej.compilers.Visiteur;
import fr.compil.jej.core.Type;
import fr.compil.jej.core.Utils;
import fr.compil.jej.core.Visibility;
import fr.compil.jej.nodes.statements.StatementNode;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static java.util.Map.entry;

public class ConstructorNode extends MethodNode {
	
	private final int hashParams;
	
	public ConstructorNode(Visibility visibility, List<Variable> arguments, List<StatementNode> statements, ClassNode owner) {
		this(visibility, arguments, statements, Collections.emptyList(), owner);
	}
	public ConstructorNode(Visibility visibility, List<Variable> arguments, List<StatementNode> statements, List<String> throwableList, ClassNode owner) {
		super(visibility, false, new Type(owner), owner.getName(), arguments, statements, throwableList, owner);
		hashParams = Utils.hash(arguments);
	}

	public String cName() {
		return "new_" + getClassOwner().getName() + "_" + hashParams;
	}

	public boolean isMainMethod() {
		return false;
	}
	
	public String getDisplayName() {
		return getClassOwner().getName() + "_" + hashParams;
	}
	
	public boolean isConstructor() {
		return true;
	}
	
	@Override
	public String niceString(int indent) {
		List<Map.Entry<String, List<?>>> objects = List.of(entry("content", statements));
		String data = getVisibility() + ", " + getReturnType();
		return Utils.appendStuff(indent, "Constructor", data, objects);
	}

	@Override
	public void accept(Visiteur visiteur) {
		//visiteur.acceptMethod(this);
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		ConstructorNode m = (ConstructorNode) o;
		if(!getClassOwner().equals(m.getClassOwner()))
			return false;
		return hashParams == m.hashParams;
	}
	
	@Override
	public Type getReturnType() {
		return Type.TYPE_VOID;
	}
}
