package fr.compil.jej.nodes;

import fr.compil.jej.compilers.Visiteur;
import fr.compil.jej.core.MethodSignature;
import fr.compil.jej.core.PetitJavaEnvironment;
import fr.compil.jej.core.Printable;
import fr.compil.jej.core.StreamTokens;
import fr.compil.jej.core.TokenType;
import fr.compil.jej.core.Typable;
import fr.compil.jej.core.Utils;
import fr.compil.jej.core.Visibility;
import fr.compil.jej.errors.BadEnvironmentError;
import fr.compil.jej.errors.BadTokenError;
import fr.compil.jej.errors.SyntaxError;
import fr.compil.jej.nodes.expressions.ExpressionNode;
import fr.compil.jej.std.FakeObject;
import fr.jamailun.jamlogger.JamLogger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.Map.entry;

public class ClassNode extends MethodsOwner implements Printable {

	private final Visibility visibility;
	protected List<MethodNode> methods;
	protected List<FieldNode> fields;
	protected final boolean fake;
	private List<String> implementedInterfacesNames;
	protected final List<InterfaceNode> implementedInterfaces;
	
	protected ClassNode parentClass;
	protected final String parentClassName;
	
	public ClassNode(Visibility visibility, String name, ClassNode parentClass, boolean fake) {
		this(visibility, name, parentClass == null ? "NONE" : parentClass.getName(), fake);
		this.parentClass = parentClass;
	}
	
	public ClassNode(Visibility visibility, String name, String parentClassName, boolean fake) {
		this.visibility = visibility;
		this.name = name;
		this.parentClassName = parentClassName;
		implementedInterfaces = new LinkedList<>();
		this.fake = fake;
	}
	
	@Override
	public boolean isFake() {
		return fake;
	}
	
	protected void setFieldsAndMethods(List<FieldNode> fields, List<MethodNode> methods) {
		this.methods = Collections.unmodifiableList(methods);
		this.fields = Collections.unmodifiableList(fields);
	}
	
	public List<InterfaceNode> getImplementedInterfaces() {
		return implementedInterfaces;
	}
	
	protected void setImplementedInterfaces(List<String> implementedInterfaces) {
		this.implementedInterfacesNames = implementedInterfaces;
	}
	
	public boolean isSubclass(ClassNode clazz) {
		if(getName().equals(clazz.getName()))
			return true;
		if(getName().equals(FakeObject.FAKE_OBJECT.getName()))
			return false;
		if(parentClass.getName().equals(clazz.getName()))
			return true;
		return parentClass.isSubclass(clazz);
	}
	
	public void linkWithInterfaces(PetitJavaEnvironment env) throws BadEnvironmentError {
		for(String interfaceName : implementedInterfacesNames) {
			InterfaceNode parent = env.getInterface(this, interfaceName);
			implementedInterfaces.add(parent);
		}
	}
	
	public ClassNode getParentClass() {
		return parentClass;
	}
	
	public String getParentClassName() {
		return parentClassName;
	}
	
	public boolean shouldLinkParentClass() {
		return this != FakeObject.FAKE_OBJECT && parentClass == null;
	}
	
	public void setParentClass(ClassNode parentClass) {
		if(!shouldLinkParentClass())
			throw new RuntimeException("Cannot bind parent to class '" + getName() + "'.");
		this.parentClass = parentClass;
	}
	
	public Visibility getVisibility() {
		return visibility;
	}
	
	@Override
	public boolean isSubtype(MethodsOwner owner) {
		if(owner instanceof InterfaceNode) {
			if(implementedInterfaces.contains(owner))
				return true;
			if(getName().equals(FakeObject.FAKE_OBJECT.getName()))
				return false;
			return parentClass.isSubtype(owner);
		}
		ClassNode clazz = (ClassNode) owner;
		if(getName().equals(clazz.getName()))
			return true;
		if(getName().equals(FakeObject.FAKE_OBJECT.getName()))
			return false;
		if(parentClass.getName().equals(clazz.getName()))
			return true;
		return parentClass.isSubclass(clazz);
	}
	
	public MethodSignature getMethod(String name, List<? extends Typable> params) {
		MethodNode method = methods.stream()
				.filter(m -> !m.isConstructor() && m.getName().equals(name) && m.areParametersValid(params))
				.findFirst()
				.orElse(null);
		if(method == null && parentClass != null)
			return parentClass.getMethod(name, params);
		return method == null ? null : method.getSignature();
	}
	
	public MethodNode getMethod(MethodSignature signature) {
		JamLogger.success(signature.getName());
		MethodNode method = methods.stream()
				.filter(m ->
						! m.isConstructor()
						&& m.acceptsSignature(signature)
				)
				.findFirst()
				.orElse(null);
		if(method == null && parentClass != null)
			return parentClass.getMethod(signature);
		return method;
	}
	
	public ConstructorNode getConstructor(List<ExpressionNode> params) {
		ConstructorNode constructor = methods.stream()
				.filter(m -> m.isConstructor() && m.areParametersValid(params))
				.map(m -> (ConstructorNode) m)
				.findFirst()
				.orElse(null);
		if(constructor == null && parentClass != null)
			return parentClass.getConstructor(params);
		return constructor;
	}
	
	public void checkDefaultConstructor() {
		if(getConstructors().isEmpty()) {
			ConstructorNode c = new ConstructorNode(Visibility.PUBLIC, Collections.emptyList(), Collections.emptyList(),  Collections.emptyList(), this);
			List<MethodNode> ms = new ArrayList<>(this.methods);
			ms.add(c);
			methods = Collections.unmodifiableList(ms);
		}
	}
	
	public List<MethodNode> getMethods() {
		return methods.stream()
				.filter(m -> !m.isConstructor())
				.collect(Collectors.toList());
	}
	public List<MethodNode> getAllMethods() {
		return new ArrayList<>(methods);
	}
	public List<ConstructorNode> getConstructors() {
		return methods.stream()
				.filter(MethodNode::isConstructor)
				.map(m -> (ConstructorNode)m)
				.collect(Collectors.toList());
	}
	
	public FieldNode getField(String name) {
		FieldNode field =  fields.stream()
				.filter(f -> f.getName().equals(name))
				.findFirst()
				.orElse(null);
		if(field == null && parentClass != null)
			return parentClass.getField(name);
		return field;
	}
	
	public List<FieldNode> getFields() {
		// On fait ce truc étrange pour que l'ordre soit préservé dans les structures héritées en C./
		List<FieldNode> list = new LinkedList<>();
		if(parentClass != null) {
			list.addAll(parentClass.getFields());
			// Penser à retirer les champs qui sont redéfinis par l'enfant
			list.removeIf(f -> fields.stream().anyMatch(fl -> fl.getName().equals(f.getName())));
		}
		list.addAll(fields);
		return list;
	}
	
	public static ClassNode generate(StreamTokens tokens) throws BadTokenError, SyntaxError {
		Visibility visibility = Visibility.optionalFrom(tokens);
		//TODO final ?
		tokens.junkOrThrow(TokenType.CLASS, "expected 'class' or 'interface' after class visibility.");
		String name = tokens.readIdentifier();
		String parentClassName = FakeObject.FAKE_OBJECT.getName();
		if(tokens.isFutureConsume(TokenType.EXTENDS)) {
			parentClassName = tokens.readIdentifier();
		}
		List<String> implementedInterfaces = new LinkedList<>();
		if(tokens.isFutureConsume(TokenType.IMPLEMENTS)) {
			do {
				String implementedInterface = tokens.readIdentifier();
				implementedInterfaces.add(implementedInterface);
			} while(tokens.isFutureConsume(TokenType.COMMA));
		}
		tokens.junkOrThrow(TokenType.LEFT_BRACES, "expected '{' after class name.");
		
		List<MethodNode> methods = new LinkedList<>();
		List<FieldNode> fields = new LinkedList<>();
		
		ClassNode classNode = new ClassNode(visibility, name, parentClassName, false);
		while(tokens.isNotFuture(TokenType.RIGHT_BRACES)) {
			//System.out.println("1st : " + tokens.peek());
			if(tokens.canFindTokenBeforeAnother(TokenType.LEFT_PAREN, TokenType.SEMICOLON, TokenType.EQUAL)) {
				// '(' before ';' : it's a METHOD
				MethodNode method = MethodNode.generate(tokens, classNode);
				if(methods.contains(method)) { // avec le ".equals" surchargé
					throw new SyntaxError("a class cannot have 2 identical methods declaration (" + method.getDisplayName() + ").");
				}
				methods.add(method);
			} else {
				// ';' or '=' before '(' : it's a FIELD
				FieldNode field = FieldNode.generate(tokens, classNode);
				if(fields.stream().anyMatch(f -> f.getName().equals(field.getName()))) {
					throw new SyntaxError("a class cannot have 2 fields with the same name (" + name + "->" + field.getName() + ").");
				}
				fields.add(field);
			}
		}
		tokens.junk();
		
		classNode.setFieldsAndMethods(fields, methods);
		classNode.setImplementedInterfaces(implementedInterfaces);
		
		return classNode;
	}

	public List<MethodNode> getMethodsAsVtable() {
		List<MethodNode> classicMethods = getMethods();
		classicMethods.removeIf(MethodNode::isStatic);
		int maxVtable = classicMethods.stream().mapToInt(MethodNode::getVTableId).max().orElse(-1);
		if(maxVtable < 0) // pas de vraie méthode : pas la peine d'essayer de faire la suite.
			return Collections.emptyList();
		List<MethodNode> all = new ArrayList<>(maxVtable);
		for(int i = 0; i <= maxVtable; i++) {
			final int j = i;
			all.add(classicMethods.stream().filter(m -> m.getVTableId() == j).findFirst().orElse(null));
		}
		return all;
	}
	
	@Override
	public String toString() {
		return niceString(0);
	}
	
	@Override
	public String niceString(int indent) {
		List<Map.Entry<String, List<?>>> objects =List.of(
				entry("fields", fields),
				entry("methods", methods)
		);
		String extendsS = getParentClass() == null ? "'" + getParentClassName() + "'" : getParentClass().getName();
		return Utils.appendStuff(indent,
				"Class",
				"'" + name+"', visibility " + visibility + (this == FakeObject.FAKE_OBJECT ? "" : ", extends " + extendsS),
				objects
		);
	}

	@Override
	public void accept(Visiteur visiteur) {
		visiteur.acceptClass(this);
	}
}
