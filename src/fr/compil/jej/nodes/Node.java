package fr.compil.jej.nodes;

import fr.compil.jej.compilers.Visiteur;

/**
 * The core of the program-tree. They are created by the Parser.
 * A Node has a name, and can accept a Visiteur
 * @see fr.compil.jej.Parser
 * @see Visiteur
 */
public abstract class Node {

	protected String name;
	
	/**
	 * Get the name of this node.
	 * @return null if the node isn't named (a TRY for exemple doesn't need a name, but a method does).
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Accept a Visiteur.
	 * @param visiteur the visiteur to accept the node.
	 */
	public abstract void accept(Visiteur visiteur);
}
