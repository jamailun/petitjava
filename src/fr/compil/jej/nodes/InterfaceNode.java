package fr.compil.jej.nodes;

import fr.compil.jej.compilers.Visiteur;
import fr.compil.jej.core.MethodSignature;
import fr.compil.jej.core.PetitJavaEnvironment;
import fr.compil.jej.core.Printable;
import fr.compil.jej.core.StreamTokens;
import fr.compil.jej.core.TokenType;
import fr.compil.jej.core.Typable;
import fr.compil.jej.core.Utils;
import fr.compil.jej.core.Visibility;
import fr.compil.jej.errors.BadEnvironmentError;
import fr.compil.jej.errors.BadTokenError;
import fr.compil.jej.errors.SyntaxError;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.Map.entry;

public class InterfaceNode extends MethodsOwner implements Printable {
	
	private final Visibility visibility;
	protected List<MethodSignature> methods;
	
	protected final List<String> parentsInterfacesNames;
	protected final List<InterfaceNode> parentsInterfaces;
	
	public InterfaceNode(Visibility visibility, String name, List<String> parentInterfacesNames) {
		this.visibility = visibility;
		this.name = name;
		this.parentsInterfacesNames = parentInterfacesNames;
		parentsInterfaces = new LinkedList<>();
	}
	
	protected void setMethods(List<MethodSignature> methods) {
		this.methods = Collections.unmodifiableList(methods);
	}
	
	public boolean isSubInterface(InterfaceNode interfaceNode) {
		if(getName().equals(interfaceNode.getName()))
			return true;
		return parentsInterfaces.stream().anyMatch(i -> i.isSubInterface(interfaceNode));
	}
	
	public Visibility getVisibility() {
		return visibility;
	}
	
	public List<MethodSignature> getAllMethodsSignatures() {
		List<MethodSignature> sign = parentsInterfaces.stream()
				.flatMap(pi -> pi.getAllMethodsSignatures().stream())
				.collect(Collectors.toList());
		sign.addAll(methods);
		return sign;
	}
	
	public void linkWithInterfaces(PetitJavaEnvironment env) throws BadEnvironmentError {
		for(String interfaceName : parentsInterfacesNames) {
			InterfaceNode parent = env.getInterface(this, interfaceName);
			if(parent.isSubInterface(this))
				throw new BadEnvironmentError("Circular interface extends, " + interfaceName + " from " + name + ".");
			parentsInterfaces.add(parent);
		}
	}
	
	public static InterfaceNode generate(StreamTokens tokens) throws BadTokenError, SyntaxError {
		Visibility visibility = Visibility.optionalFrom(tokens);
		tokens.junkOrThrow(TokenType.INTERFACE, "Expected an 'interface' token.");
		String name = tokens.readIdentifier();
		List<String> extendedList = new ArrayList<>();
		if(tokens.isFutureConsume(TokenType.EXTENDS)) {
			do {
				String extendedName = tokens.readIdentifier();
				extendedList.add(extendedName);
			} while(tokens.isFutureConsume(TokenType.COMMA));
		}
		tokens.junkOrThrow(TokenType.LEFT_BRACES, "expected '{' after class name.");
		
		List<MethodSignature> methods = new LinkedList<>();
		
		InterfaceNode interfaceNode = new InterfaceNode(visibility, name, extendedList);
		while(tokens.isNotFuture(TokenType.RIGHT_BRACES)) {
			MethodSignature method = MethodSignature.generate(tokens, interfaceNode);
			if(methods.contains(method)) { // Avec le ".equals" surchargé
				throw new SyntaxError("a class cannot have 2 identical methods declaration (" + method.getName() + ").");
			}
			methods.add(method);
			tokens.junkOrThrow(TokenType.SEMICOLON, "In an interface, all method signature must be followed by a ';'.");
		}
		tokens.junk();
		
		interfaceNode.setMethods(methods);
		
		return interfaceNode;
	}

	@Override
	public String toString() {
		return niceString(0);
	}
	
	@Override
	public String niceString(int indent) {
		List<Map.Entry<String, List<?>>> objects =List.of(
				entry("method-signatures", methods)
		);
		String extendsS = parentsInterfacesNames.isEmpty() ? "" : ", extends [" + String.join(", ", parentsInterfacesNames) + "]";
		return Utils.appendStuff(indent,
				"Class",
				"'" + name+"', visibility " + visibility + extendsS,
				objects
		);
	}

	@Override
	public void accept(Visiteur visiteur) {
		//TODO interface
	}
	
	@Override
	public boolean isSubtype(MethodsOwner owner) {
		if(owner instanceof InterfaceNode)
			return isSubInterface((InterfaceNode) owner);
		return false;
	}
	
	@Override
	public MethodSignature getMethod(String name, List<? extends Typable> params) {
		for(MethodSignature ms : getAllMethodsSignatures())
			if(ms.areParametersValid(params) && ms.getName().equals(name))
				return ms;
		return null;
	}
}
