package fr.compil.jej.nodes.expressions;

import fr.compil.jej.compilers.Visiteur;
import fr.compil.jej.core.MethodSignature;
import fr.compil.jej.core.TypesContext;
import fr.compil.jej.core.Visibility;
import fr.compil.jej.nodes.ClassNode;
import fr.compil.jej.core.Type;
import fr.compil.jej.nodes.MethodsOwner;
import fr.compil.jej.std.FakeStringMethods;
import fr.jamailun.jamlogger.JamLogger;

import java.util.Arrays;
import java.util.List;

public class MethodCallExpression extends CallableNode {
	
	private final String methodName;
	private final List<ExpressionNode> params;
	private MethodSignature linkedMethod;
	
	public MethodCallExpression(String methodName, List<ExpressionNode> params) {
		this.methodName = methodName;
		this.params = params;
	}
	
	public List<ExpressionNode> getParams() {
		return params;
	}

	public MethodSignature getLinkedMethod() {
		return linkedMethod;
	}
	
	@Override
	public boolean validateType(TypesContext context) {
		if(params.stream().anyMatch(p -> ! p.validateType(context))) {
			JamLogger.error("Could not validate params for method " + methodName + ".");
			return false;
		}
		
		if(callable == null) {
			// pas de callable ? C'est une méthode dans la classe actuelle.
			ClassNode classNode = context.getCurrentClass();
			linkedMethod = classNode.getMethod(methodName, params);
			if(linkedMethod == null) {
				JamLogger.error("Class " + classNode.getName() + " does NOT have a method '"
						+ methodName + "' with those " + Arrays.toString(params.toArray()) + " arguments.");
				return false;
			}
			ThisExpression target = new ThisExpression();
			target.validateType(context); // cannot fail
			callable = target;
			return true;
		}
		
		// Valider le callable
		if(!callable.validateType(context))
			return false;
		if(callable.getType().isPrimitive() && callable.getType().getPrimitiveType()== Type.Primitive.STRING) {
			if(FakeStringMethods.STR_FAKE_METHS.containsKey(methodName)) {
				// Cas particulier, on appelle ue méthode spéciale de String
				linkedMethod = FakeStringMethods.STR_FAKE_METHS.get(methodName).getSignature();
				return true;
			}
		}
		// Vérifier qu'il ne soit pas d'un type primitif
		if(callable.getType().isPrimitive()) {
			JamLogger.error("Cannot call method '"+methodName+"' on primitive " + callable + ".");
			return false;
		}
		// Trouver la méthode à appeler
		MethodsOwner callableClazz = callable.getType().getLinkedMethodsOwner();
		if(callableClazz == null) {
			JamLogger.error("[MethodCall] Weird error, could not get callable TYPE for " + this + ". Callable type = " + callable.getType() + ", callable class : " + callable.getClass().getSimpleName() + ".");
			return false;
		}
		linkedMethod = callableClazz.getMethod(methodName, params);
		if(linkedMethod == null) {
			JamLogger.error("Class " + callableClazz.getName() + " does NOT have a method '" + methodName + "' with those " + params.size() + " arguments.");
			JamLogger.error("Arguments type = " +
					Arrays.toString(params.stream().map(en -> en.getType().isPrimitive() ? en.getType().getPrimitiveType() : en.getType().getLinkedMethodsOwner().getName()).toArray()) + ".");
			return false;
		}
		// Vérifier que cette méthode peut être appelée !
		if(linkedMethod.getVisibility() == Visibility.PRIVATE) {
			if(!context.getCurrentClass().equals(linkedMethod.getOwner())) {
				JamLogger.error("Call of a private method from class " + context.getCurrentClass().getName() + " to "
						+ linkedMethod.getOwner().getName() + "#" + linkedMethod.getName());
				return false;
			}
		}
		return true;
	}
	
	@Override
	public Type getType() {
		if(linkedMethod == null)
			return Type.TYPE_VOID;
		return linkedMethod.getReturnType();
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("MethodCall{");
		if(linkedMethod == null)
			sb.append("method_name='").append(methodName).append("', ");
		else
			sb.append("method=").append(linkedMethod.getDisplayName()).append(", ");
		if(callable != null)
			sb.append("target=").append(callable).append(", ");
		return sb.append("params=").append(Arrays.toString(params.toArray())).append('}').toString();
	}

	@Override
	public void accept(Visiteur visiteur) {
		visiteur.acceptMethodCall(this);
	}
}
