package fr.compil.jej.nodes.expressions;

import fr.compil.jej.compilers.Visiteur;
import fr.compil.jej.core.*;
import fr.compil.jej.errors.BadTokenError;
import fr.compil.jej.errors.SyntaxError;
import fr.compil.jej.nodes.ClassNode;
import fr.compil.jej.nodes.ConstructorNode;
import fr.jamailun.jamlogger.JamLogger;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class NewExpression extends ObjectRefExpression {
	
	private final Type type;
	private final List<ExpressionNode> params;
	private ConstructorNode linkedConstructor;
	
	private HardArrayExpression sameAsHardArray;
	
	public NewExpression(Type type, List<ExpressionNode> params) {
		super("new_"+type.getClassName());
		this.type = type;
		this.params = params;
	}
	
	public List<ExpressionNode> getParams() {
		if(params == null)
			throw new RuntimeException("Cannot get params for a hard array.");
		return params;
	}
	
	public boolean isArray() {
		return type.isArray();
	}
	
	public boolean isHardArray() {
		return isArray() && params != null;
	}
	
	public ConstructorNode getLinkedConstructor() {
		return linkedConstructor;
	}
	
	public List<ExpressionNode> getArraySize() {
		if(!isArray())
			throw new RuntimeException("Cannot get array size for a non-array type. Type = " + type);
		if (isHardArray()) {
			List<ExpressionNode> sizes = new LinkedList<>();
			sizes.add(new IntExpression(params.size()));
			return sizes;
		}
		return ((ArrayType)type).getLength() ;
	}
	
	@Override
	public Type getType() {
		return type;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("NewExpression{type=" + type);
		if(getType().isArray()) {
			sb.append(", size=").append(getArraySize());
			if (isHardArray()) {
				sb.append(", content=").append(Arrays.toString(getParams().toArray()));
			}
		} else {
			sb.append(", params=").append(Arrays.toString(getParams().toArray()));
		}
		return sb.append('}').toString();
	}
	
	@Override
	public boolean validateType(TypesContext context) {
		// Est-ce que le type est une classe qui doit être link ?
		if(getType().shouldBeLinked()) {
			Type linkedType = context.getType(getType().getClassName());
			if(linkedType == null) {
				JamLogger.warning("type="+type);
				JamLogger.error("Unknown type : " + getType().getClassName() + " for NEW expression.");
				return false;
			}
			ClassNode linkedClass = linkedType.getLinkedClass();
			if(linkedClass == null) {
				JamLogger.error("Type " + getType().getClassName() + " does not corresponds to any class for NEW expression.");
				return false;
			}
			getType().linkClass(linkedClass);
		}
		// Validation de tous les paramètres
		if(isHardArray()) {
			sameAsHardArray = new HardArrayExpression(params);
			if(!sameAsHardArray.validateType(context)) {
				JamLogger.error("NewExpression could not initialize own HardArray : " + sameAsHardArray);
				return false;
			}
			if(!sameAsHardArray.getType().equals(type)) {
				JamLogger.error("In new declaration, array is supposed to be " + getType() + ", but it's " + params.get(0).getType());
				return false;
			}
		} else if(type.isArray()) {
			for (ExpressionNode node : getArraySize()) {
				if (!node.validateType(context)) {
					JamLogger.error("Array must have a INT size. Here it's : " + getArraySize() + ", in array declaration " + this + ".");
					return false;
				}
			}
		} else {
			if(type.getLinkedMethodsOwner() == null) {
				JamLogger.error("Type " + type + " does NOT have a linked class. This = " + this + ".");
				return false;
			}
			if(params.stream().anyMatch(p -> !p.validateType(context)))
				return false;
			linkedConstructor = type.getLinkedClass().getConstructor(params);
			if(linkedConstructor == null) {
				JamLogger.error("Could not find a constructor for params " + Arrays.toString(params.toArray()));
				return false;
			}
			// Vérifier que ce constructeur peut être appelé !
			if(linkedConstructor.getVisibility() == Visibility.PRIVATE) {
				if(!context.getCurrentClass().equals(linkedConstructor.getClassOwner())) {
					JamLogger.error("Call of a private constructor from class " + context.getCurrentClass().getName() +
							" to " + linkedConstructor.getClassOwner().getName() + "#new");
					return false;
				}
			}
		}
		return true;
	}
	
	// on VIENT de cramer un 'new'
	@PreviousToken(allowed = {TokenType.NEW})
	public static NewExpression generate(StreamTokens tokens) throws BadTokenError, SyntaxError {
		Type newType;
		// Type (identifier ou primitive)
		if(tokens.isFuture(TokenType.IDENTIFIER)) {
			newType = new Type(tokens.readIdentifier());
		} else {
			newType = new Type(Type.Primitive.from(tokens.next()));
		}
		
		List<ExpressionNode> params = null;
		boolean arrayInitialize = true; // si true, les [] sont vide.
		// Si TYPE[ alors on veut faire un array.
		if(tokens.isFutureConsume(TokenType.LEFT_BRACKET)) {
			newType = newType.toArray();
			// On chope (ou pas) ce qu'il peut y avoir entre les brackets
			if (tokens.isNotFuture(TokenType.RIGHT_BRACKET)) {
				arrayInitialize = false;
				((ArrayType)newType).addLength(ExpressionNode.generate(tokens));
			}
			tokens.junkOrThrow(TokenType.RIGHT_BRACKET, "Expected a ']' after a 'TYPE[...'");
			while (tokens.isFutureConsume(TokenType.LEFT_BRACKET)) {
				if (!arrayInitialize)
					((ArrayType)newType).addLength(ExpressionNode.generate(tokens));
				else
					((ArrayType)newType).addDimension();
				tokens.junkOrThrow(TokenType.RIGHT_BRACKET, "Expected a ']' after a 'TYPE[...'");
			}
			// On donne directement le contenu de l'array.
			if(tokens.isFutureConsume(TokenType.LEFT_BRACES)) {
				if(arrayInitialize) {
					HardArrayExpression array = HardArrayExpression.generate(tokens);
					params = array.getExpressions();
				} else {
					throw new BadTokenError(tokens.next(), "Cannot specify array size AND array content.");
				}
			} else {
				if(arrayInitialize)
					throw new BadTokenError(tokens.next(), "You should EITHER specify array size OR array content.");
			}
		} else {
			// Sans tableaux, on peut que faire un NEW sur un identifier, pas une primitive
			tokens.junkOrThrow(TokenType.LEFT_PAREN, "Expected a '(' after 'new " + newType + "'.");
			// Parameters
			params = readParameters(tokens);
		}
		// Return the stuff
		return new NewExpression(newType, params);
	}
	
	@Override
	public void accept(Visiteur visiteur) {
		if(isHardArray())
			visiteur.acceptHardArrayExpression(sameAsHardArray);
		else
			visiteur.acceptNewExpression(this);
	}
	
	
}
