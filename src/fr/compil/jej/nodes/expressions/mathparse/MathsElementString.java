package fr.compil.jej.nodes.expressions.mathparse;

import fr.compil.jej.compilers.Visiteur;
import fr.compil.jej.core.Type;
import fr.compil.jej.core.TypesContext;
import fr.compil.jej.nodes.expressions.StringExpression;

public final class MathsElementString extends MathsElement {

    private final String rawString;

    public MathsElementString(String rawString) {
        this.rawString = rawString;
    }
    
    @Override
    public String toString() {
        return "\"" + rawString + "\"";
    }
    
    @Override
    public boolean validateType(TypesContext context) {
        return true;
    }
    
    @Override
    public Type getType() {
        return Type.Primitive.STRING.asType();
    }

    @Override
    public void accept(Visiteur visiteur) {
        visiteur.acceptStringExpression(new StringExpression(rawString));
    }
    
    @Override
    public boolean canBeString() {
        return true;
    }
    
    @Override
    public boolean isPureNumeric() {
        return false;
    }
    
    @Override
    public boolean isCoherent() {
        return true;
    }
}
