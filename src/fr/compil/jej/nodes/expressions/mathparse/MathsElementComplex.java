package fr.compil.jej.nodes.expressions.mathparse;

import fr.compil.jej.compilers.Visiteur;
import fr.compil.jej.core.Type;
import fr.compil.jej.core.TypesContext;
import fr.compil.jej.nodes.expressions.Operator;
import fr.jamailun.jamlogger.JamLogger;

public final class MathsElementComplex extends MathsElement {

    private final Operator operator;
    private final MathsElement left, right;

    public MathsElementComplex(Operator operator, MathsElement left, MathsElement right) {
        this.operator = operator;
        this.left = left;
        this.right = right;
    }

    public Operator getOperator() {
        return operator;
    }

    public MathsElement getLeft() {
        return left;
    }

    public MathsElement getRight() {
        return right;
    }

    @Override
    public String toString() {
        return "(" + left + " " + operator + " " + right + ")";
    }
    
    @Override
    public boolean validateType(TypesContext context) {
        if(!left.validateType(context) || !right.validateType(context)) {
            JamLogger.error("Could not validate type of expression " + this + ".");
            return false;
        }
        if(!isCoherent()) {
            JamLogger.error("Incoherent math parsing. Is it full numeric ? " + isPureNumeric() + ", Can it be string ? " + canBeString() + ". Element = " + this + ".");
            return false;
        }
        return true;
    }
    
    @Override
    public Type getType() {
        if(left.getType().equals(right.getType()))
            return left.getType();
        // Dans le cas où les parties gauches et droites sont différentes. Si on est considéré comme un string on renvoie
        // string, et sinon on renvoie le type le plus "général" (int <- double)
        if(canBeString())
            return Type.Primitive.STRING.asType();
        if(left.getType().equals(Type.Primitive.INT.asType()))
            return right.getType();
        return left.getType();
    }

    @Override
    public void accept(Visiteur visiteur) {
        visiteur.acceptMathElementComplexExpression(this);
    }
    
    @Override
    public boolean canBeString() {
        if(operator != Operator.ADD)
            return false;
        return left.canBeString() || right.canBeString();
    }
    
    @Override
    public boolean isPureNumeric() {
        return left.isPureNumeric() && right.isPureNumeric();
    }
    
    @Override
    public boolean isCoherent() {
        return isPureNumeric() || canBeString();
    }

}
