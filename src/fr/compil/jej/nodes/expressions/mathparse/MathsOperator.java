package fr.compil.jej.nodes.expressions.mathparse;

import fr.compil.jej.nodes.expressions.Operator;

final class MathsOperator implements MathsPart {

    private final Operator operator;

    public MathsOperator(Operator operator) {
        this.operator = operator;
    }

    public Operator getOperator() {
        return operator;
    }

    @Override
    public String toString() {
        return operator.toString();
    }
}
