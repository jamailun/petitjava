package fr.compil.jej.nodes.expressions.mathparse;

import fr.compil.jej.nodes.expressions.ExpressionNode;

public abstract class MathsElement extends ExpressionNode implements MathsPart {
	
	/**
	 * @return true if :
	 *  - at least one of the elements is a string
	 *  - the only operators is '+'
	 */
	public abstract boolean canBeString();
	
	/**
	 * @return true if the element can be considered as a numeric value.
	 */
	public abstract boolean isPureNumeric();
	
	/**
	 * @return true if this element (or his children) are OR string OR full numeric.
	 * @see #canBeString()
	 * @see #isPureNumeric()
	 */
	public abstract boolean isCoherent();
	
}
