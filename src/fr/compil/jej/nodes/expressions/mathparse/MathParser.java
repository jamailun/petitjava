package fr.compil.jej.nodes.expressions.mathparse;

import fr.compil.jej.core.StreamTokens;
import fr.compil.jej.core.Token;
import fr.compil.jej.core.TokenType;
import fr.compil.jej.errors.BadTokenError;
import fr.compil.jej.errors.SyntaxError;
import fr.compil.jej.nodes.expressions.DoubleExpression;
import fr.compil.jej.nodes.expressions.ExpressionNode;
import fr.compil.jej.nodes.expressions.IntExpression;
import fr.compil.jej.nodes.expressions.NewExpression;
import fr.compil.jej.nodes.expressions.Operator;
import fr.compil.jej.nodes.statements.NewStatement;

import java.util.LinkedList;
import java.util.List;

public final class MathParser {

    private static final List<TokenType> MATH_STOP = List.of(
            TokenType.COMMA,
            TokenType.RIGHT_PAREN,
            TokenType.SEMICOLON,
            TokenType.RIGHT_BRACKET,
            TokenType.RIGHT_BRACES
    );
    
    private final StreamTokens tokens;

    private MathParser(StreamTokens tokens) {
        this.tokens = tokens;
    }

    private MathsElement parse() throws BadTokenError, SyntaxError {
        return parseParenthesis();
    }
    
    private boolean continueParsing() {
        return tokens.hasNext() && ! MATH_STOP.contains(tokens.peek().getType());
    }

    private MathsElement parseParenthesis() throws BadTokenError, SyntaxError {
        List<MathsPart> list = new LinkedList<>();
        while( continueParsing() ) {
            Token token = this.tokens.next();
            switch (token.getType()) {
                case LEFT_PAREN:
                    StreamTokens stream = split();
                    MathsElement elem = new MathParser(stream).parse();
                    list.add(elem);
                    break;
                case VALUE_INT:
                    list.add(new MathsElementSimple(new IntExpression(token.getValInt())));
                    break;
                case VALUE_DOUBLE:
                    list.add(new MathsElementSimple(new DoubleExpression(token.getValDouble())));
                    break;
                case VALUE_STRING:
                    list.add(new MathsElementString(token.getValString()));
                    break;
                case VALUE_TRUE:
                case VALUE_FALSE:
                    list.add(new MathsElementBoolean(token.getType() == TokenType.VALUE_TRUE));
                    break;
                case MATH_ADD:
                case MATH_SUB:
                case MATH_MUL:
                case MATH_DIV:
                    list.add(new MathsOperator(Operator.from(token)));
                    break;
                case NEW:
                    NewExpression newExpression = NewExpression.generate(tokens);
                    ExpressionNode newExpressionWrapped = NewStatement.getContextualized(newExpression, tokens);
                    list.add(new MathsElementSimple(newExpressionWrapped));
                    if(tokens.isFuture(TokenType.RIGHT_BRACES))
                        tokens.reverseAndSelf(); // Si l'expression se termine par "NEW<truc>.();" alors le point virgule/parenthèse est bouffée.
                    break;
                case THIS:
                case IDENTIFIER:
                    ExpressionNode expression = ExpressionNode.generate(token, tokens);
                    list.add(new MathsElementSimple(expression));
                    break;
                default:
                    throw new BadTokenError(token, "Unexpected token in math expression.");
            }
        }
        return parseAddition(list);
    }

    private MathsElement parseAddition(List<MathsPart> parts) throws SyntaxError {
        List<List<MathsPart>> partList = new LinkedList<>();
        partList.add(new LinkedList<>());
        List<Operator> opeList = new LinkedList<>();
        for (MathsPart p : parts) {
            if (p instanceof MathsOperator) {
                Operator ope = ((MathsOperator) p).getOperator();
                switch (ope) {
                    case ADD:
                    case SUB:
                        opeList.add(ope);
                        partList.add(new LinkedList<>());
                        break;
                    default:
                        partList.get(partList.size() - 1).add(p);
                }
            } else {
                partList.get(partList.size() - 1).add(p);
            }
        }
        for(List<MathsPart> partsList : partList) {
            boolean number = true;
            for(MathsPart part : partsList) {
                if(part instanceof MathsOperator) {
                    if(number)
                        throw new SyntaxError("Two operators follow each other in a math expression.");
                    number = true;
                } else {
                    if(!number)
                        throw new SyntaxError("An operator is missing in a math expression.");
                    number = false;
                }
            }
        }
        if (partList.size() == 1)
            return parseMultiplication(partList.get(0));
        return makeMElem(partList, opeList);
    }

    private MathsElement makeMElem(List<List<MathsPart>> parts, List<Operator> operators) {
        MathsElement elem = parseMultiplication(parts.get(0));
        parts.remove(0);
        if (operators.size() == 0)
            return elem;
        Operator ope = operators.get(0);
        operators.remove(0);
        return new MathsElementComplex(ope, elem, makeMElem(parts, operators));
    }

    private MathsElement parseMultiplication(List<MathsPart> parts) {
        if (parts.size() == 1)
            return (MathsElement) parts.get(0);
        List<List<MathsPart>> partList = new LinkedList<>();
        partList.add(new LinkedList<>());
        List<Operator> opeList = new LinkedList<>();
        for (MathsPart p : parts) {
            if (p instanceof MathsOperator) {
                Operator ope = ((MathsOperator) p).getOperator();
                opeList.add(ope);
                partList.add(new LinkedList<>());
            } else {
                partList.get(partList.size() - 1).add(p);
            }
        }
        return makeMElem(partList, opeList);
    }

    private StreamTokens split() throws SyntaxError {
        List<Token> tokens = new LinkedList<>();
        int depth = 0;
        while(this.tokens.hasNext()) {
            Token token = this.tokens.next();
            if(token.getType() == TokenType.LEFT_PAREN) {
                depth++;
            } else if(token.getType() == TokenType.RIGHT_PAREN) {
                depth--;
                if(depth < 0) {
                    return new StreamTokens(tokens);
                }
            }
            tokens.add(token);
        }
        throw new SyntaxError("Missing closing parenthesis in math expression");
    }

    public static MathsElement parse(StreamTokens tokens) throws BadTokenError, SyntaxError {
        return new MathParser(tokens).parse();
    }
}
