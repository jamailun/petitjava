package fr.compil.jej.nodes.expressions.mathparse;

import fr.compil.jej.compilers.Visiteur;
import fr.compil.jej.core.Type;
import fr.compil.jej.core.TypesContext;
import fr.compil.jej.nodes.expressions.ExpressionNode;

public final class MathsElementSimple extends MathsElement {

    private final ExpressionNode wrapped;

    public MathsElementSimple(ExpressionNode wrapped) {
        this.wrapped = wrapped;
    }

    @Override
    public String toString() {
        return wrapped.toString();
    }
    
    @Override
    public boolean validateType(TypesContext context) {
        return wrapped.validateType(context);
    }
    
    @Override
    public boolean isCoherent() {
        return true;
    }
    
    @Override
    public Type getType() {
        return wrapped.getType();
    }

    @Override
    public void accept(Visiteur visiteur) {
        wrapped.accept(visiteur);
    }
    
    @Override
    public boolean canBeString() {
        return getType().equals(Type.Primitive.STRING.asType());
    }
    
    @Override
    public boolean isPureNumeric() {
        return getType().isPrimitive() && getType().getPrimitiveType().isNumeric();
    }
    
}
