package fr.compil.jej.nodes.expressions.mathparse;

import fr.compil.jej.compilers.Visiteur;
import fr.compil.jej.core.Type;
import fr.compil.jej.core.TypesContext;
import fr.compil.jej.nodes.expressions.BoolExpression;

public final class MathsElementBoolean extends MathsElement {

    private final boolean rawBoolean;

    public MathsElementBoolean(boolean rawBoolean) {
        this.rawBoolean = rawBoolean;
    }
    
    @Override
    public String toString() {
        return "{" + rawBoolean + "}";
    }
    
    @Override
    public boolean validateType(TypesContext context) {
        return true;
    }
    
    @Override
    public Type getType() {
        return Type.Primitive.BOOLEAN.asType();
    }

    @Override
    public void accept(Visiteur visiteur) {
        visiteur.acceptBoolExpression(new BoolExpression(rawBoolean));
    }
    
    @Override
    public boolean canBeString() {
        return true;
    }
    
    @Override
    public boolean isPureNumeric() {
        return false;
    }
    
    @Override
    public boolean isCoherent() {
        return true;
    }
}
