package fr.compil.jej.nodes.expressions;

import fr.compil.jej.compilers.Visiteur;
import fr.compil.jej.core.TypesContext;
import fr.compil.jej.core.Type;

public class StringExpression extends ExpressionNode {
	
	private final String rawValue;
	
	public StringExpression(String value) {
		this.rawValue = value;
	}
	
	public String getRawValue() {
		return rawValue;
	}
	
	@Override
	public String toString() {
		return "String(\"" + rawValue+ "\")";
	}
	
	@Override
	public Type getType() {
		return new Type(Type.Primitive.STRING);
	}
	
	@Override
	public boolean validateType(TypesContext context) {
		return true;
	}

	@Override
	public void accept(Visiteur visiteur) {
		visiteur.acceptStringExpression(this);
	}
}
