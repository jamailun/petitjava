package fr.compil.jej.nodes.expressions;

import fr.compil.jej.compilers.Visiteur;
import fr.compil.jej.core.TokenType;
import fr.compil.jej.core.Type;
import fr.compil.jej.core.TypesContext;
import fr.jamailun.jamlogger.JamLogger;

public class DoubleOperatedExpression extends ExpressionNode {

    private final boolean add;
    private final ExpressionNode target;
    private final boolean beforeExpression;

    public DoubleOperatedExpression(TokenType ope, ExpressionNode target, boolean beforeExpression) {
        if(ope != TokenType.DOUBLE_SUB && ope != TokenType.DOUBLE_ADD)
            throw new RuntimeException("Can only create DoubleOperatedExpression with '++' or '--', not " + ope + ".");
        this.add = ope == TokenType.DOUBLE_ADD;
        this.target = target;
        this.beforeExpression = beforeExpression;
    }

    public ExpressionNode getTarget() {
        return target;
    }

    public boolean isBeforeExpression() {
        return beforeExpression;
    }

    public String getOperator() {
        return add ? "++" : "--";
    }

    @Override
    public boolean validateType(TypesContext context) {
        if(!target.validateType(context))
            return false;
        if(!target.getType().equals(getType())) {
            JamLogger.error("Wrong tyope for DoubleOperatedExpression. Target '"+target+"' is of type " + target.getType());
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        if(isBeforeExpression())
            return getOperator() + target;
        return target + getOperator();
    }

    @Override
    public Type getType() {
        return Type.Primitive.INT.asType();
    }

    @Override
    public void accept(Visiteur visiteur) {
        visiteur.acceptDoubleOperatedExpression(this);
    }
}
