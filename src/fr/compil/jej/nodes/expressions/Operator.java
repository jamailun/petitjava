package fr.compil.jej.nodes.expressions;

import fr.compil.jej.core.Token;
import fr.compil.jej.core.TokenType;
import fr.compil.jej.errors.BadTokenError;
import fr.compil.jej.errors.SyntaxError;

import java.util.Arrays;

public enum Operator {
	
	ADD(TokenType.MATH_ADD, '+'),
	MUL(TokenType.MATH_MUL, '*'),
	SUB(TokenType.MATH_SUB, '-'),
	DIV(TokenType.MATH_DIV, '/');
	public final TokenType linkedType;
	private final char debugChar;
	Operator(TokenType linkedType, char debugChar) {
		this.linkedType = linkedType;
		this.debugChar = debugChar;
	}
	
	@Override
	public String toString() {
		return String.valueOf(debugChar);
	}
	
	public static Operator from(Token token) throws BadTokenError {
		return Arrays.stream(values())
				.filter(o -> o.linkedType == token.getType())
				.findAny()
				.orElseThrow(() -> new BadTokenError(token, "Bad operator token."));
	}
	public static Operator from(char c) throws SyntaxError {
		return Arrays.stream(values())
				.filter(o -> o.debugChar == c)
				.findAny()
				.orElseThrow(() -> new SyntaxError("Unknown operator '"+c+"'."));
	}
	
}
