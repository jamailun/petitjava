package fr.compil.jej.nodes.expressions.logicparse;

import fr.compil.jej.core.Token;
import fr.compil.jej.core.TokenType;
import fr.compil.jej.errors.BadTokenError;

import java.util.Arrays;

public enum LogicOperator {
	OR(TokenType.OR, "||", true),
	AND(TokenType.AND, "&&", true),
	
	EQUAL(TokenType.MATH_EQUAL, "=="),
	NOT_EQUAL(TokenType.MATH_NEQUAL, "!="),
	GET(TokenType.MATH_GET, ">="),
	GT(TokenType.MATH_GT, ">"),
	LET(TokenType.MATH_LET, "<="),
	LT(TokenType.MATH_LT, "<");
	
	private final TokenType tokenType;
	private final String printString;
	private final boolean combine;
	
	LogicOperator(TokenType tokenType, String printString) {
		this(tokenType, printString, false);
	}
	LogicOperator(TokenType tokenType, String printString, boolean combine) {
		this.tokenType = tokenType;
		this.printString = printString;
		this.combine = combine;
	}
	
	@Override
	public String toString() {
		return printString;
	}
	
	public boolean isCombine() {
		return combine;
	}
	
	public static LogicOperator from(Token token) throws BadTokenError {
		return Arrays.stream(values())
				.filter(o -> o.tokenType == token.getType())
				.findAny()
				.orElseThrow(() -> new BadTokenError(token, "Bad logic operator token."));
	}
}
