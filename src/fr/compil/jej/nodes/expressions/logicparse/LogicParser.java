package fr.compil.jej.nodes.expressions.logicparse;

import fr.compil.jej.core.StreamTokens;
import fr.compil.jej.core.Token;
import fr.compil.jej.core.TokenType;
import fr.compil.jej.errors.BadTokenError;
import fr.compil.jej.errors.SyntaxError;
import fr.compil.jej.nodes.expressions.BoolExpression;
import fr.compil.jej.nodes.expressions.ExpressionNode;
import java.util.LinkedList;
import java.util.List;

public final class LogicParser {
	
	private final StreamTokens tokens;
	private LogicParser(StreamTokens tokens) {
		this.tokens = tokens;
	}
	
	private LogicExpression parse() throws BadTokenError, SyntaxError {
		return createLogicExpression();
	}
	
	private LogicExpression createLogicExpression() throws BadTokenError, SyntaxError {
		List<LogicExpression> elements = new LinkedList<>();
		List<LogicOperator> operators = new LinkedList<>();
		boolean not = false;
		while(tokens.isNotFuture(TokenType.RIGHT_PAREN) && tokens.isNotFuture(TokenType.SEMICOLON)) {
			Token token = tokens.next();
			switch (token.getType()) {
				case LEFT_PAREN:
					LogicExpression blocExpression = createLogicExpression();
					tokens.junkOrThrow(TokenType.RIGHT_PAREN, "In logicalExpression, expected a ')' after a '(...'");
					if(not) {
						not = false;
						blocExpression.setNegative();
					}
					elements.add(blocExpression);
					break;
				case NOT:
					if(not)
						throw new SyntaxError(token.getLinePos(), "Unexpected pattern '!!'.");
					not = true;
					break;
				case VALUE_TRUE:
				case VALUE_FALSE:
					LogicExpression boolExpression = new LogicElementSimple(new BoolExpression(token.getType() == TokenType.VALUE_TRUE));
					if(not) {
						not = false;
						boolExpression.setNegative();
					}
					elements.add(boolExpression);
					break;
				case AND:
				case OR:
					LogicOperator operatorCombine = LogicOperator.from(token);
					if(not)
						throw new SyntaxError(token.getLinePos(), "Unexpected pattern '!" + operatorCombine + "'.");
					operators.add(operatorCombine);
					break;
				case MATH_EQUAL:
				case MATH_NEQUAL:
				case MATH_GET:
				case MATH_GT:
				case MATH_LET:
				case MATH_LT:
					//JamLogger.info("elements : "+Arrays.toString(elements.toArray()));
					if(elements.isEmpty())
						throw new SyntaxError(token.getLinePos(),"Token " + token + " needs an element at his left.");
					ExpressionNode left = elements.get(elements.size() - 1);
					while(left instanceof LogicElementSimple)
						left = ((LogicElementSimple)left).getWrapped();
					LogicOperator operator = LogicOperator.from(token);
					ExpressionNode right = ExpressionNode.generate(tokens);
					LogicElementEquality equality = new LogicElementEquality(left, operator, right);
					//JamLogger.warning("add " + equality + "next token : " + tokens.peek());
					elements.remove(elements.size() - 1);
					elements.add(equality);
					break;
				default:
					ExpressionNode expression = ExpressionNode.generate(token, tokens, false);
					//JamLogger.warning("expression : " + expression +", next token = " +tokens.peek());
					LogicElementSimple expressionWrapped = new LogicElementSimple(expression);
					if(not) {
						not = false;
						expressionWrapped.setNegative();
					}
					elements.add(expressionWrapped);
					break;
			}
		}
		if(elements.isEmpty())
			throw new BadTokenError(tokens.peek(), "Cannot have an empty logical expression");
		if(elements.size() == 1)
			return new LogicElementSimple(elements.get(0));
		return new LogicElementComplex(elements, operators);
	}
	
	
	public static LogicExpression parse(StreamTokens tokens) throws BadTokenError, SyntaxError {
		return new LogicParser(tokens).parse();
	}
	
}
