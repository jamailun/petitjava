package fr.compil.jej.nodes.expressions.logicparse;

import fr.compil.jej.compilers.Visiteur;
import fr.compil.jej.core.TypesContext;
import fr.compil.jej.errors.SyntaxError;
import fr.compil.jej.nodes.expressions.ExpressionNode;
import fr.compil.jej.nodes.expressions.NullExpression;
import fr.jamailun.jamlogger.JamLogger;

public final class LogicElementEquality extends LogicExpression {
	
	private final ExpressionNode left, right;
	private final LogicOperator operator;
	
	public LogicElementEquality(ExpressionNode left, LogicOperator operator, ExpressionNode right) throws SyntaxError {
		this.left = left;
		this.right = right;
		this.operator = operator;
		if(operator.isCombine())
			throw new SyntaxError("Operator " + operator + " is combining and it shouldn't...");
	}
	
	public ExpressionNode getLeft() {
		return left;
	}
	
	public ExpressionNode getRight() {
		return right;
	}
	
	public LogicOperator getOperator() {
		return operator;
	}
	
	@Override
	public String toString() {
		return (isNegative() ? "NOT " : "") + "(" + left + " " + operator + " " + right + ")";
	}
	
	@Override
	public boolean validateType(TypesContext context) {
		if(!(left.validateType(context) && right.validateType(context)))
			return false;
		if(left != NullExpression.NULL && right != NullExpression.NULL) {
			if(!left.getType().equals(right.getType())) {
				JamLogger.error("Equality '" + this + "' needs to compare two elements of the same types. But " + left.getType() + " != " + right.getType() + ".");
				return false;
			}
		}
		return true;
	}
	
	@Override
	public void accept(Visiteur visiteur) {
		visiteur.acceptLogicElementEqualityExpression(this);
	}
}
