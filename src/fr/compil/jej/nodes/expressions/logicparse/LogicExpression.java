package fr.compil.jej.nodes.expressions.logicparse;

import fr.compil.jej.core.Type;
import fr.compil.jej.nodes.expressions.ExpressionNode;

public abstract class LogicExpression extends ExpressionNode implements LogicPart {
	
	protected boolean negative = false;
	
	public boolean isNegative() {
		return negative;
	}
	
	public void setNegative() {
		negative = true;
	}
	
	@Override
	public Type getType() {
		return Type.Primitive.BOOLEAN.asType();
	}
	
}
