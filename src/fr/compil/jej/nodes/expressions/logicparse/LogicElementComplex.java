package fr.compil.jej.nodes.expressions.logicparse;

import fr.compil.jej.compilers.Visiteur;
import fr.compil.jej.core.TypesContext;
import fr.compil.jej.errors.SyntaxError;

import java.util.List;

public final class LogicElementComplex extends LogicExpression {
	
	private final List<LogicExpression> wrapped;
	private final List<LogicOperator> operators;
	
	public LogicElementComplex(List<LogicExpression> wrapped, List<LogicOperator> operators) throws SyntaxError {
		if(wrapped.size() <= 1)
			throw new SyntaxError("Empty or mono-formed LogicElementComplex.");
		this.wrapped = wrapped;
		this.operators = operators;
		if(!operators.stream().allMatch(LogicOperator::isCombine))
			throw new SyntaxError("Bad operator between two predicates.");
		if(wrapped.size() - 1 != operators.size())
			throw new SyntaxError("A predicate should be formed of (element size) == (operator size - 1)");
	}
	
	public List<LogicExpression> getWrapped() {
		return wrapped;
	}
	
	public List<LogicOperator> getOperators() {
		return operators;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		if(isNegative())
			sb.append("NOT ");
		for(int i = 0; i < wrapped.size() - 1; i++) {
			sb.append(wrapped.get(i)).append(" ").append(operators.get(i)).append(" ");
		}
		return "(" + sb.append(wrapped.get(wrapped.size() - 1)) + ")";
	}
	
	@Override
	public boolean validateType(TypesContext context) {
		return wrapped.stream().allMatch(w -> w.validateType(context));
	}
	
	@Override
	public void accept(Visiteur visiteur) {
		visiteur.acceptLogicElementComplexExpression(this);
	}
}
