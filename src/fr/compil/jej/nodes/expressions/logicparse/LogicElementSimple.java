package fr.compil.jej.nodes.expressions.logicparse;

import fr.compil.jej.compilers.Visiteur;
import fr.compil.jej.core.TypesContext;
import fr.compil.jej.nodes.expressions.ExpressionNode;

public final class LogicElementSimple extends LogicExpression {
	
	private final ExpressionNode wrapped;
	
	public LogicElementSimple(ExpressionNode wrapped) {
		this.wrapped = wrapped;
	}
	
	public ExpressionNode getWrapped() {
		return wrapped;
	}
	
	@Override
	public String toString() {
		return (isNegative() ? "NOT " : "") + wrapped.toString();
	}
	
	@Override
	public boolean validateType(TypesContext context) {
		return wrapped.validateType(context);
	}
	
	@Override
	public void accept(Visiteur visiteur) {
		visiteur.acceptLogicElementSimpleExpression(this);
	}
}
