package fr.compil.jej.nodes.expressions;

import fr.compil.jej.compilers.Visiteur;
import fr.compil.jej.core.ArrayType;
import fr.compil.jej.core.TypesContext;
import fr.compil.jej.core.Visibility;
import fr.compil.jej.nodes.ClassNode;
import fr.compil.jej.nodes.FieldNode;
import fr.compil.jej.core.Type;
import fr.compil.jej.std.FakeLengthArray;
import fr.jamailun.jamlogger.JamLogger;

public class FieldCallExpression extends CallableNode {
	
	private final String fieldName;
	private FieldNode linkedField;
	
	public FieldCallExpression(String fieldName) {
		this.fieldName = fieldName;
	}
	
	public String getFieldName() {
		return fieldName;
	}
	
	public FieldNode getLinkedField() {
		return linkedField;
	}
	
	@Override
	public boolean validateType(TypesContext context) {
		if(callable == null) {
			// pas de callable ? C'est une méthode dans la classe actuelle.
			ClassNode classNode = context.getCurrentClass();
			linkedField = classNode.getField(fieldName);
			if(linkedField == null) {
				JamLogger.error("Class " + classNode.getName() + " does NOT have a field '" + fieldName + "'.");
				return false;
			}
			return true;
		}
		// Valider le callable
		if(!callable.validateType(context))
			return false;
		if(callable.getType().isArray() && fieldName.equals(FakeLengthArray.FIELD_NAME)) {
			// Cas particulier, on appelle le champs 'field' des tableaux
			linkedField = new FakeLengthArray((ArrayType) callable.getType());
			return true;
		}
		// Vérifier qu'il ne soit pas d'un type primitif
		else if(callable.getType().isPrimitive()) {
			JamLogger.error("Cannot call field '"+ fieldName + "' on primitive " + callable + ".");
			return false;
		}
		// Trouver le field à appeler
		ClassNode callableClazz = callable.getType().getLinkedClass();
		if(callableClazz == null) {
			JamLogger.error("[FieldCall] Weird error, could not get callable TYPE for " + this + ". Callable type = " + callable.getType());
			return false;
		}
		linkedField = callableClazz.getField(fieldName);
		if(linkedField == null) {
			JamLogger.error("Class " + callableClazz.getName() + " does NOT have a field '" + fieldName + "'.");
			return false;
		}
		// Vérifier que ce field peut être appelé !
		if(linkedField.getVisibility() == Visibility.PRIVATE) {
			// Le private ne fonctionne que si on est dans la classe qui est ciblée.
			// Dans le cas contraire, on lance une erreur
			if(!context.getCurrentClass().equals(linkedField.getOwningClass())) {
				JamLogger.error("Call of a private field from class " + context.getCurrentClass().getName() + " to "
						+ linkedField.getOwningClass().getName() + "#" + linkedField.getName());
				return false;
			}
		}
		return true;
	}
	
	@Override
	public Type getType() {
		if(linkedField == null)
			return Type.TYPE_VOID;
		return linkedField.getType();
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("FieldCall{");
		if(linkedField == null)
			sb.append("field_name='").append(fieldName).append("'");
		else
			sb.append("field=").append(linkedField.getName());
		if(callable != null)
			sb.append(", ").append("target=").append(callable);
		return sb.append('}').toString();
	}

	@Override
	public void accept(Visiteur visiteur) {
		visiteur.acceptFieldCallExpression(this);
	}
}
