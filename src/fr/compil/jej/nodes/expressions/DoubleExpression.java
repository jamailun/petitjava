package fr.compil.jej.nodes.expressions;

import fr.compil.jej.core.Type;

public class DoubleExpression extends NumberExpression {
	
	private final double rawValue;
	
	public DoubleExpression(double value) {
		this.rawValue = value;
	}
	
	@Override
	public String toString() {
		return "Double(" + rawValue + ')';
	}
	
	public double getRaw() {
		return rawValue;
	}
	
	@Override
	public Type getType() {
		return new Type(Type.Primitive.DOUBLE);
	}
}
