package fr.compil.jej.nodes.expressions;

import fr.compil.jej.compilers.Visiteur;
import fr.compil.jej.core.TypesContext;

public abstract class NumberExpression extends ExpressionNode {

	@Override
	public boolean validateType(TypesContext context) {
		return true;
	}

	@Override
	public void accept(Visiteur visiteur) {
		visiteur.acceptNumberExpression(this);
	}
}
