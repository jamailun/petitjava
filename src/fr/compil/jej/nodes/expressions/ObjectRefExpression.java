package fr.compil.jej.nodes.expressions;

import fr.compil.jej.compilers.Visiteur;
import fr.compil.jej.core.TypesContext;
import fr.compil.jej.core.Type;
import fr.compil.jej.nodes.FieldNode;
import fr.jamailun.jamlogger.JamLogger;

public class ObjectRefExpression extends ExpressionNode {
	
	private Type knownType;
	private final String name;

	private FieldNode staticFieldRef;
	private FieldCallExpression sameAsFieldCall;
	
	public ObjectRefExpression(String name) {
		this.name = name;
	}
	
	public boolean isTypeKnown() {
		return knownType != null;
	}
	
	public String getName() {
		return name;
	}
	
	@Override
	public boolean validateType(TypesContext context) {
		knownType = context.getType(name);
		if(knownType == null) {
			JamLogger.error("Unknown variable : '" + name + "'.");
			return false;
		}

		if(context.isField(name)) {
			if(context.isStaticField(name)) {
				staticFieldRef = context.getStaticFieldRef(name);
			} else {
				sameAsFieldCall = new FieldCallExpression(name);
				sameAsFieldCall.setCallable(new ThisExpression());
				if(!sameAsFieldCall.validateType(context)) {
					JamLogger.error("Could not validate mutated objectRef from " + this + " to " + sameAsFieldCall + ".");
					return false;
				}
			}
		}

		return true;
	}
	
	public FieldNode getStaticFieldRef() {
		return staticFieldRef;
	}
	
	@Override
	public Type getType() {
		return knownType;
	}
	
	@Override
	public String toString() {
		return "Ref{'" + name + "'" + (isTypeKnown() ? ", type=" + getType() : ", ?") + "}";
	}

	@Override
	public void accept(Visiteur visiteur) {
		if(sameAsFieldCall != null) {
			visiteur.acceptFieldCallExpression(sameAsFieldCall);
		} else {
			visiteur.acceptObjectReferenceExpression(this);
		}
	}
}
