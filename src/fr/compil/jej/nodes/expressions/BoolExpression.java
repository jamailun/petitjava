package fr.compil.jej.nodes.expressions;

import fr.compil.jej.compilers.Visiteur;
import fr.compil.jej.core.TypesContext;
import fr.compil.jej.core.Type;

public class BoolExpression extends ExpressionNode {
	
	private final boolean wrappedValue;
	
	public BoolExpression(boolean value) {
		this.wrappedValue = value;
	}
	
	public boolean getRaw() {
		return wrappedValue;
	}
	
	@Override
	public String toString() {
		return String.valueOf(wrappedValue);
	}
	
	@Override
	public Type getType() {
		return new Type(Type.Primitive.BOOLEAN);
	}
	
	@Override
	public boolean validateType(TypesContext context) {
		return true;
	}

	@Override
	public void accept(Visiteur visiteur) {
		visiteur.acceptBoolExpression(this);
	}
}
