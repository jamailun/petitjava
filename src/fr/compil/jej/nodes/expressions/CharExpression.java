package fr.compil.jej.nodes.expressions;

import fr.compil.jej.core.Type;

public class CharExpression extends NumberExpression {
	
	private final String rawValue;
	
	public CharExpression(String value) {
		this.rawValue = value;
	}
	
	@Override
	public String toString() {
		return "Char(" + rawValue + ')';
	}
	
	public String getRaw() {
		return rawValue;
	}
	
	@Override
	public Type getType() {
		return new Type(Type.Primitive.CHAR);
	}

}
