package fr.compil.jej.nodes.expressions;

import fr.compil.jej.compilers.Visiteur;
import fr.compil.jej.core.PreviousToken;
import fr.compil.jej.core.StreamTokens;
import fr.compil.jej.core.Token;
import fr.compil.jej.core.TokenType;
import fr.compil.jej.core.Typable;
import fr.compil.jej.core.TypesContext;
import fr.compil.jej.core.Utils;
import fr.compil.jej.errors.BadTokenError;
import fr.compil.jej.errors.SyntaxError;
import fr.compil.jej.nodes.Node;
import fr.compil.jej.nodes.expressions.logicparse.LogicElementComplex;
import fr.compil.jej.nodes.expressions.logicparse.LogicElementEquality;
import fr.compil.jej.nodes.expressions.logicparse.LogicElementSimple;
import fr.compil.jej.nodes.expressions.logicparse.LogicExpression;
import fr.compil.jej.nodes.expressions.logicparse.LogicOperator;
import fr.compil.jej.nodes.expressions.logicparse.LogicParser;
import fr.compil.jej.nodes.expressions.mathparse.MathParser;
import fr.compil.jej.nodes.expressions.mathparse.MathsElement;
import fr.compil.jej.nodes.expressions.mathparse.MathsElementComplex;
import fr.compil.jej.nodes.expressions.mathparse.MathsElementSimple;
import fr.compil.jej.nodes.expressions.mathparse.MathsElementString;

import java.util.*;

public abstract class ExpressionNode extends Node implements Typable {
	
	public abstract boolean validateType(TypesContext context);

	private final String varName = "var_" + Utils.CREATE_ID();
	@Override
	public String getName() {
		return varName;
	}
	
	/**
	 * Imaginons qu'on ait à parser 'a.b.c'.
	 * On cherche à obtenir quelque chose de la forme FieldCall("c", target=FieldCall("b", target=ObjectRef("a"))).
	 * Pour ça, comme on lit de gauche à droite, on va se retrouver avec une queue du style ['a', 'b', 'c'].
	 */
	private static ExpressionNode createFromRefs(Deque<Utils.Ref> refs) {
		if(refs.isEmpty()) // on n'est pas censé envoyer une dequeue vide.
			throw new RuntimeException("Empty refs");
		Utils.Ref polled = refs.pollLast();
		CallableNode callableNode = polled.convert();
		// Le callable est SOIT un autre field call, soit un object ref
		if(refs.isEmpty()) {
			if(polled.params == null)
				return new ObjectRefExpression(polled.name);
			return new MethodCallExpression(polled.name, polled.params);
		} if (refs.size() == 1) {
			Utils.Ref last = refs.pollLast();
			if(last.params == null)
				callableNode.setCallable(new ObjectRefExpression(last.name));
			else
				callableNode.setCallable(new MethodCallExpression(last.name, last.params));
		} else {
			callableNode.setCallable(createFromRefs(refs));
		}
		return callableNode;
	}

	// a[x].b.c
	@PreviousToken(allowed = {TokenType.IDENTIFIER, TokenType.THIS})
	public static ExpressionNode createCaller(Token token, StreamTokens tokens, Deque<Utils.Ref> refs) throws BadTokenError, SyntaxError {
		if(token.getType() != TokenType.IDENTIFIER)
			throw new BadTokenError(token, "Expected an identifier after a dot.");
		List<ExpressionNode> params = null;
		if(tokens.isFutureConsume(TokenType.LEFT_PAREN)) {
			params = ExpressionNode.readParameters(tokens);
		}
		List<ExpressionNode> indexes = new LinkedList<>();
		refs.offerLast(new Utils.Ref(token.getValString(), params, indexes));
		boolean array = false;
		while(tokens.isFutureConsume(TokenType.LEFT_BRACKET)) {
			array = true;
			ExpressionNode arrayIndex = ExpressionNode.generate(tokens);
			tokens.junkOrThrow(TokenType.RIGHT_BRACKET, "Expected a  ']' after a 'NAME[...'.");
			indexes.add(arrayIndex);
		}
		if (array) refs.offerLast(new Utils.Ref(null, null, indexes));
		if(tokens.isFutureConsume(TokenType.DOT)) {
			return createCaller(tokens.next(), tokens, refs);
		}
		return createFromRefs(refs);
	}
	
	/**
	 * Lire des arguments entre parenthèse. Requiert un '(' avant. Renvoie SANS le ')'.
	 */
	@PreviousToken(allowed = {TokenType.LEFT_PAREN})
	public static List<ExpressionNode> readParameters(StreamTokens tokens) throws BadTokenError, SyntaxError {
		List<ExpressionNode> params = new LinkedList<>();
		boolean firstParam = true;
		while(tokens.isNotFuture(TokenType.RIGHT_PAREN)) {
			if(firstParam) {
				firstParam = false;
			} else {
				tokens.junkOrThrow(TokenType.COMMA, "expected ',' between two parameters.");
			}
			ExpressionNode param = ExpressionNode.generate(tokens);
			params.add(param);
		}
		tokens.junk();
		return params;
	}
	
	public static ExpressionNode generate(StreamTokens tokens, boolean allowed) throws BadTokenError, SyntaxError {
		return generate(tokens.next(), tokens, allowed);
	}
	
	public static ExpressionNode generate(StreamTokens tokens) throws BadTokenError, SyntaxError {
		return generate(tokens.next(), tokens);
	}
	
	public static ExpressionNode generate(Token token, StreamTokens tokens) throws BadTokenError, SyntaxError {
		return generate(token, tokens, true);
	}
	
	public static ExpressionNode tryAddOperators(ExpressionNode expressionNode, StreamTokens tokens, boolean allowed) throws BadTokenError, SyntaxError {
		// LOGIC OPERATORS
		if(allowed && tokens.isFutureALogicOperator()) {
			LogicExpression left = new LogicElementSimple(expressionNode);
			LogicOperator operator = LogicOperator.from(tokens.next());
			LogicExpression right = LogicParser.parse(tokens);
			if(operator.isCombine()) {
				return new LogicElementComplex(List.of(left, right), Collections.singletonList(operator));
			}
			return tryAddOperators(new LogicElementEquality(left, operator, right), tokens, true);
		}
		// MATHEMATICS OPERATORS
		if(allowed && tokens.isFutureAMathOperator()) {
			MathsElement left = expressionNode instanceof MathsElement ? (MathsElement) expressionNode : new MathsElementSimple(expressionNode);
			Operator operator = Operator.from(tokens.next());
			MathsElement right = MathParser.parse(tokens);
			return tryAddOperators(new MathsElementComplex(operator, left, right), tokens, true);
		}
		// NEUTRE
		return expressionNode;
	}
	
	public static ExpressionNode generate(Token token, StreamTokens tokens, boolean allowed) throws BadTokenError, SyntaxError {
		switch (token.getType()) {
			case NULL:
				return NullExpression.NULL;
			case FINAL:
				return generate(tokens, allowed);
			case LEFT_PAREN:
				ExpressionNode inParenthesis = generate(tokens, allowed);
				tokens.junkOrThrow(TokenType.RIGHT_PAREN, "Missing closing parenthesis.");
				if(tokens.isFutureConsume(TokenType.DOT)) {
					ExpressionNode next = generate(tokens, allowed);
					return tryAddOperators(insertAtCallableSource(next, inParenthesis), tokens, allowed);
				}
				return inParenthesis;
			case DOUBLE_ADD:
			case DOUBLE_SUB:
				ExpressionNode rightExpr = generate(tokens);
				DoubleOperatedExpression doe = new DoubleOperatedExpression(token.getType(), rightExpr, true);
				return tryAddOperators(doe, tokens, allowed);
			case VALUE_TRUE:
				return new BoolExpression(true);
			case VALUE_FALSE:
				return new BoolExpression(false);
			case VALUE_INT:
				return tryAddOperators(new IntExpression(token.getValInt()), tokens, allowed);
			case VALUE_DOUBLE:
				return new DoubleExpression(token.getValDouble());
			case VALUE_CHAR:
				return new CharExpression(token.getValString());
			case VALUE_STRING:
				return tryAddOperators(new MathsElementString(token.getValString()), tokens, allowed);
			case IDENTIFIER:
				ExpressionNode nodeIdentifier = createCaller(token, tokens, new ArrayDeque<>());
				return tryAddOperators(nodeIdentifier, tokens, allowed);
			case NEW:
				NewExpression newRef = NewExpression.generate(tokens);
				if(tokens.isFutureConsume(TokenType.DOT)) {
					ExpressionNode next = generate(tokens, false);
					return tryAddOperators(insertAtCallableSource(next, newRef), tokens, allowed);
				}
				return tryAddOperators(newRef, tokens, allowed);
			case THIS:
				ExpressionNode thisRef = new ThisExpression();
				if(tokens.isFutureConsume(TokenType.DOT)) {
					ExpressionNode next = generate(tokens, false);
					return tryAddOperators(insertAtCallableSource(next, new ThisExpression()), tokens, allowed);
				}
				return tryAddOperators(thisRef, tokens, allowed);
		}
		throw new BadTokenError(token, "Unexpected token.");
	}
	
	public static CallableNode insertAtCallableSource(ExpressionNode expression, ExpressionNode source) throws SyntaxError {
		// Si c'est une simple ref, il faut la transformer en FieldCallable
		if(expression instanceof ObjectRefExpression) {
			expression = new FieldCallExpression((expression).getName());
			((FieldCallExpression)expression).setCallable(source);
			return ((FieldCallExpression)expression);
		}
		if(source instanceof ThisExpression)
			return (CallableNode) expression;
		// Si c'est un callable, il faut choper le possesseur d'un object ref
		if(expression instanceof CallableNode) {
			CallableNode callable = (CallableNode) expression;
			// 1) Remonter jusqu'au dernier callable
			while(callable.getCallable() instanceof CallableNode)
				callable = (CallableNode) callable.getCallable();
			// 2) Changer le call de ce dernier sur un nouveau callable
			if(callable.getCallable() instanceof ObjectRefExpression) {
				FieldCallExpression newEnd = new FieldCallExpression((callable.getCallable()).getName());
				newEnd.setCallable(source);
				callable.setCallable(newEnd);
			} else if(callable instanceof MethodCallExpression) {
				callable.setCallable(source);
			} else  {
				throw new RuntimeException("Expected ObjectRefExpression or MethodCallExpression at source of a callable...");
			}
			return (CallableNode) expression;
		}
		
		throw new SyntaxError("Bad expression after " + source + ". Expected object reference or callable. Got " + expression.getClass().getSimpleName() +" : " + expression);
	}

	@Override
	public void accept(Visiteur visiteur) {
		throw new RuntimeException("pas encore implémenté pour " + getClass().getSimpleName() + ".");
	}

}
