package fr.compil.jej.nodes.expressions;

public abstract class CallableNode extends ExpressionNode {
	protected ExpressionNode callable;
	
	public ExpressionNode getCallable() {
		return callable;
	}
	
	public void setCallable(ExpressionNode callable) {
		// Set PARENT callable if it can be defined
		if(this.callable instanceof FieldCallExpression) {
			((FieldCallExpression)this.callable).setCallable(callable);
			return;
		} else if(this.callable instanceof MethodCallExpression) {
			((MethodCallExpression)this.callable).setCallable(callable);
			return;
		}
		// If parent of callable cannot be set / is null, it's our
		this.callable = callable;
	}
	
}
