package fr.compil.jej.nodes.expressions;

import fr.compil.jej.compilers.Visiteur;
import fr.compil.jej.core.Type;
import fr.compil.jej.core.TypesContext;

public class NullExpression extends ExpressionNode {
	
	public static final NullExpression NULL = new NullExpression();
	
	private NullExpression() {}
	
	@Override
	public Type getType() {
		return Type.TYPE_VOID;
	}
	
	@Override
	public boolean validateType(TypesContext context) {
		return true;
	}
	
	@Override
	public String toString() {
		return "<NULL>";
	}
	
	@Override
	public void accept(Visiteur visiteur) {
		visiteur.acceptNullExpression(this);
	}
}
