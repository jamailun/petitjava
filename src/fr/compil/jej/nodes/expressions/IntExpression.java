package fr.compil.jej.nodes.expressions;

import fr.compil.jej.core.Type;

public class IntExpression extends NumberExpression {
	
	private final int rawValue;
	
	public IntExpression(int value) {
		this.rawValue = value;
	}
	
	@Override
	public String toString() {
		return "Int(" + rawValue + ')';
	}
	
	public int getRaw() {
		return rawValue;
	}
	
	@Override
	public Type getType() {
		return new Type(Type.Primitive.INT);
	}

}
