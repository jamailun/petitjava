package fr.compil.jej.nodes.expressions;

import fr.compil.jej.compilers.Visiteur;
import fr.compil.jej.core.ArrayType;
import fr.compil.jej.core.Type;
import fr.compil.jej.core.TypesContext;
import fr.jamailun.jamlogger.JamLogger;

import java.util.Arrays;
import java.util.List;

public class ArrayGetterExpression extends CallableNode {

    private final List<ExpressionNode> indexes;
    private Type type;

    public ArrayGetterExpression(List<ExpressionNode> indexes) {
        this.indexes = indexes;
    }

    public List<ExpressionNode> getIndexes() {
        return indexes;
    }

    @Override
    public boolean validateType(TypesContext context) {
        if(callable == null)
            throw new RuntimeException("WTF array get without callable...");
        if(!callable.validateType(context))
            return false;
        if(!callable.getType().isArray()) {
            JamLogger.error("Cannot get array element for a non-array type " + callable.getType() + ".");
            return false;
        }
        for (ExpressionNode index : indexes) {
            if(!index.validateType(context))
                return false;
        }
        for (ExpressionNode index : indexes) {
            if(!index.getType().equals(Type.Primitive.INT.asType())) {
                JamLogger.error("Index of array '" + index + "' should be of type INT ! It's : " + index.getType() + ".");
                return false;
            }
        }
        if(indexes.size() > getDimension()) {
            JamLogger.error("Bad array getter : wants " + indexes.size() + " dimensions, but only contains " + getDimension() + " in " + callable.getType());
            return false;
        }
        type = callable.getType();
        for(int i = 0; i < indexes.size(); i++)
            type = type.toNotArray();
        return true;
    }

    @Override
    public Type getType() {
        if(callable == null)
            return Type.TYPE_VOID;
        return type;
    }

    public int getDimension() {
        return ((ArrayType)callable.getType()).getDimension();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("ArrayGET{target=");
        if(callable == null)
            sb.append("NULL");
        else
            sb.append(callable);
        return sb.append(", indexes=").append(Arrays.toString(indexes.toArray())).append('}').toString();
    }

    @Override
    public void accept(Visiteur visiteur) {
        visiteur.acceptArrayGetterExpression(this);
    }
}
