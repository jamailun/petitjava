package fr.compil.jej.nodes.expressions;

import fr.compil.jej.compilers.Visiteur;
import fr.compil.jej.core.Type;
import fr.compil.jej.core.TypesContext;
import fr.compil.jej.nodes.ClassNode;

public class ThisExpression extends ObjectRefExpression {

	private ClassNode refClass;
	private Type type;
	
	public ThisExpression() {
		super("this");
		type = Type.TYPE_VOID;
	}
	
	@Override
	public String toString() {
		return "THIS{" + (refClass == null ? "?" : refClass.getName()) + "}";
	}
	
	@Override
	public boolean validateType(TypesContext context) {
		refClass = context.getCurrentClass();
		type = new Type(refClass);
		return true;
	}
	
	@Override
	public Type getType() {
		return type;
	}

	@Override
	public void accept(Visiteur visiteur) {
		visiteur.acceptThisExpression(this);
	}
}
