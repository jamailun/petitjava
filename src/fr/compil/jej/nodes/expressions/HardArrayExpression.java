package fr.compil.jej.nodes.expressions;

import fr.compil.jej.compilers.Visiteur;
import fr.compil.jej.core.*;
import fr.compil.jej.errors.BadTokenError;
import fr.compil.jej.errors.SyntaxError;
import fr.jamailun.jamlogger.JamLogger;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class HardArrayExpression extends ExpressionNode {
	
	private final List<ExpressionNode> expressions;
	private Type type = Type.TYPE_VOID;
	
	public HardArrayExpression(List<ExpressionNode> expressions) {
		this.expressions = expressions;
	}
	
	public List<ExpressionNode> getExpressions() {
		return expressions;
	}
	
	public int getSize() {
		return expressions.size();
	}
	
	public Type getType() {
		return type;
	}
	
	@PreviousToken(allowed = {TokenType.LEFT_BRACES})
	public static HardArrayExpression generate(StreamTokens tokens) throws BadTokenError, SyntaxError {
		List<ExpressionNode> elems = new LinkedList<>();
		if (tokens.isFutureConsume(TokenType.LEFT_BRACES)) {
			elems.add(HardArrayExpression.generate(tokens));
		}
		while(tokens.isNotFuture(TokenType.RIGHT_BRACES)) {
			if(!elems.isEmpty())
				tokens.junkOrThrow(TokenType.COMMA, "Need a ',' between elements of array.");
			if (tokens.isFutureConsume(TokenType.LEFT_BRACES))
				elems.add(HardArrayExpression.generate(tokens));
			else
				elems.add(ExpressionNode.generate(tokens));
		}
		tokens.junkOrThrow(TokenType.RIGHT_BRACES, "expected '}' after method content.");
		return new HardArrayExpression(elems);
	}
	
	@Override
	public boolean validateType(TypesContext context) {
		if(!expressions.stream().allMatch(e -> e.validateType(context))) {
			JamLogger.error(this + " could not validate expressions.");
			return false;
		}
		for(ExpressionNode expression : expressions) {
			if(type == Type.TYPE_VOID) {
				type = expression.getType().toArray();
				if(expression.getType() instanceof ArrayType)
					((ArrayType)type).addDimension();
			} else if(!expression.getType().equals(type.toNotArray())) {
				JamLogger.error("In hard-written array, expression " + expression + " does NOT have the right type " + type + " but has type " + expression.getType());
				return false;
			}
		}
		type = type.toArray();
		return true;
	}
	
	@Override
	public String toString() {
		return "ARRAY{" + Arrays.toString(expressions.toArray()) + "}";
	}

	@Override
	public void accept(Visiteur visiteur) {
		visiteur.acceptHardArrayExpression(this);
	}
}
