package fr.compil.jej.nodes;

import fr.compil.jej.compilers.Visiteur;
import fr.compil.jej.core.MethodSignature;
import fr.compil.jej.core.Printable;
import fr.compil.jej.core.StreamTokens;
import fr.compil.jej.core.TokenType;
import fr.compil.jej.core.Typable;
import fr.compil.jej.core.Type;
import fr.compil.jej.core.TypesContext;
import fr.compil.jej.core.Utils;
import fr.compil.jej.core.Visibility;
import fr.compil.jej.core.VtableIdentifiable;
import fr.compil.jej.errors.BadTokenError;
import fr.compil.jej.errors.SyntaxError;
import fr.compil.jej.nodes.statements.StatementNode;
import fr.jamailun.jamlogger.JamLogger;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static java.util.Map.entry;

public class MethodNode extends Node implements Printable, VtableIdentifiable {

	protected final MethodSignature signature;
	protected final List<StatementNode> statements;
	protected final List<String> throwableList;
	private final ClassNode owner;
	
	protected MethodNode(Visibility visibility, boolean isStatic, Type returnType, String name, ClassNode owner) {
		signature = new MethodSignature(visibility, isStatic, returnType, name, owner);
		this.name = name;
		this.statements = new LinkedList<>();
		this.owner = owner;
		this.throwableList = new LinkedList<>();
	}
	
	public MethodNode(Visibility visibility, boolean isStatic, Type returnType, String name, List<Variable> arguments, List<StatementNode> statements, List<String> throwableList, ClassNode owner) {
		signature = new MethodSignature(visibility, isStatic, returnType, name, owner);
		signature.addArguments(arguments);
		this.name = name;
		this.statements = statements;
		this.owner = owner;
		this.throwableList = throwableList;
	}
	
	public List<String> getThrowableList() {
		return Collections.unmodifiableList(throwableList);
	}
	
	public MethodSignature getSignature() {
		return signature;
	}
	
	public boolean isMainMethod() {
		return signature.isMainMethod();
	}
	
	public boolean isConstructor() {
		return false;
	}
	
	public List<StatementNode> getStatements() {
		return statements;
	}
	
	public static MethodNode generate(StreamTokens tokens, ClassNode owner) throws BadTokenError, SyntaxError {
		Visibility visibility = Visibility.optionalFrom(tokens);
		boolean isStatic = false;
		boolean isConstructor = false;
		Type type = null;
		String name = null;
		if(tokens.doesFollow(TokenType.IDENTIFIER, TokenType.LEFT_PAREN)) {
			if(!tokens.readIdentifier().equals(owner.getName()))
				throw new BadTokenError(tokens.peek(), "Unexpected constructor format ?");
			isConstructor = true;
		} else {
			isStatic = tokens.isFutureConsume(TokenType.STATIC);
			type = Type.generate(tokens.next());
			if(tokens.isFutureConsume(TokenType.LEFT_BRACKET)) {
				type = type.toArray();
				tokens.junkOrThrow(TokenType.RIGHT_BRACKET, "Need a '[]' for type declaration.");
			}
			name = tokens.readIdentifier();
		}
		
		tokens.junkOrThrow(TokenType.LEFT_PAREN, "expected '(' after method name.");
		
		List<Variable> arguments = new LinkedList<>();
		boolean firstArg = true;
		while(tokens.isNotFuture(TokenType.RIGHT_PAREN)) {
			if(firstArg) {
				firstArg = false;
			} else {
				tokens.junkOrThrow(TokenType.COMMA, "expected ',' between two arguments.");
			}
			boolean isFinal = tokens.isFutureConsume(TokenType.FINAL);
			Type argType = Type.generate(tokens.next());
			if(tokens.isFutureConsume(TokenType.LEFT_BRACKET)) {
				argType = argType.toArray();
				tokens.junkOrThrow(TokenType.RIGHT_BRACKET, "Expected a ']' after 'TYPE['.");
			}
			String argName = tokens.readIdentifier();
			Variable arg = new Variable(argType, argName);
			if(isFinal)
				arg.forceFinal();
			arguments.add(arg);
		}
		tokens.junkOrThrow(TokenType.RIGHT_PAREN, "expected ')' after method arguments.");
		
		List<String> throwsList = new LinkedList<>();
		if(tokens.isFutureConsume(TokenType.THROWS)) {
			do {
				throwsList.add(tokens.readIdentifier());
			} while(tokens.isFutureConsume(TokenType.COMMA));
		}
		//No abstract method for now. So no ';' is waited.
		tokens.junkOrThrow(TokenType.LEFT_BRACES, "expected '{' after method arguments.");
		
		List<StatementNode> statements = StatementNode.readStatementsBlock(tokens);
		
		if(isConstructor)
			return new ConstructorNode(visibility, arguments, statements, throwsList, owner);
		return new MethodNode(visibility, isStatic, type, name, arguments, statements, throwsList, owner);
	}
	
	public String getDisplayName() {
		return signature.getDisplayName();
	}
	
	public String cName() {
		return (isStatic()?"stc_":"") + owner.getName() + "_" + getName() + "_" + signature.getHash();
	}
	
	public boolean isStatic() {
		return signature.isStatic();
	}
	
	public Visibility getVisibility() {
		return signature.getVisibility();
	}
	
	public Type getReturnType() {
		return signature.getReturnType();
	}
	
	public List<Variable> getArguments() {
		return signature.getArguments();
	}
	
	public ClassNode getClassOwner() {return owner;}
	
	public void setVtableId(int vtableId) {
		signature.setVtableId(vtableId);
	}
	public int getVTableId() {
		return signature.getVTableId();
	}
	
	public final boolean validateThrows(TypesContext context) {
		for(String exName : throwableList) {
			Type exType = context.getType(exName);
			if(exType == null) {
				JamLogger.error("Method " + getDisplayName() + " throws an unknown exception type : '" + exName + "'.");
				return false;
			}
			if(!exType.isValidExceptionType()) {
				JamLogger.error("THROWS '" + exName + "' for method " + getDisplayName() + " is not valid.");
				return false;
			}
		}
		return true;
	}
	
	public boolean validateArguments(TypesContext context) {
		for(Variable argument : getArguments()) {
			if(argument.getType().shouldBeLinked()) {
				Type linkedType = context.getType(argument.getType().getClassName());
				if(linkedType == null) {
					JamLogger.error("Could not identify argument type '" + argument.getType().getClassName() + "'.");
					return false;
				}
				ClassNode linkedClass = linkedType.getLinkedClass();
				if(linkedClass == null) {
					JamLogger.error("[MethodNode] Weird error, linked class not linked for type '" + argument.getType().getClassName() + "'.");
					return false;
				}
				argument.getType().linkClass(linkedClass);
			}
		}
		return true;
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		return Objects.equals(((MethodNode)o).signature, signature);
	}
	
	public boolean acceptsSignature(MethodSignature ms) {
		return Objects.equals(ms, signature);
	}
	
	public boolean areParametersValid(List<? extends Typable> parameters) {
		return signature.areParametersValid(parameters);
	}
	
	@Override
	public String toString() {
		return niceString(0);
	}
	
	@Override
	public String niceString(int indent) {
		List<Map.Entry<String, List<?>>> objects = List.of(entry("content", statements));
		String name = signature.isMainMethod() ? "@Main_Method" : "Method";
		String data = (isStatic()?"STATIC, ":"")+"'" + getDisplayName() + "', " + getVisibility() + ", " + getReturnType();
		if(!throwableList.isEmpty())
			data += ", THROWS " + Arrays.toString(throwableList.toArray());
		return Utils.appendStuff(indent, name, data, objects);
	}

	@Override
	public void accept(Visiteur visiteur) {
		visiteur.acceptMethod(this);
	}
}
