package fr.compil.jej.nodes;

import fr.compil.jej.core.StreamTokens;
import fr.compil.jej.core.TokenType;
import fr.compil.jej.core.Type;
import fr.compil.jej.core.Visibility;
import fr.compil.jej.errors.BadTokenError;
import fr.compil.jej.errors.SyntaxError;
import fr.compil.jej.nodes.expressions.ExpressionNode;

public class FieldNode {
	
	private final Visibility visibility;
	private final boolean isStatic;
	private final Type type;
	private ExpressionNode assignment;
	private final String name;
	private final ClassNode owner;
	
	private FieldNode(Visibility visibility, boolean isFinal, boolean isStatic, Type type, String name, ExpressionNode assignment, ClassNode owner) {
		this(visibility, isFinal, isStatic, type, name, owner);
		this.assignment = assignment;
	}
	
	public FieldNode(Visibility visibility, boolean isFinal, boolean isStatic, Type type, String name, ClassNode owner) {
		this.visibility = visibility;
		this.isStatic = isStatic;
		this.type = type;
		if(isFinal)
			this.type.setFinal();
		this.name = name;
		this.owner = owner;
	}

	public String getName() {
		return name;
	}

	public ClassNode getOwningClass() {
		return owner;
	}

	public String cName() {
		return (isStatic() ? "stc_" +  getOwningClass().getName() + "_" : "") + getName();
	}

	public boolean hasAssignment() {
		return assignment != null;
	}
	
	public ExpressionNode getAssignment() {
		return assignment;
	}
	
	public boolean isStatic() {
		return isStatic;
	}
	
	public Visibility getVisibility() {
		return visibility;
	}
	
	public Type getType() {
		return type;
	}
	
	public static FieldNode generate(StreamTokens tokens, ClassNode owner) throws BadTokenError, SyntaxError {
		Visibility visibility = Visibility.optionalFrom(tokens);
		boolean isStatic = tokens.isFutureConsume(TokenType.STATIC);
		boolean isFinal = tokens.isFutureConsume(TokenType.FINAL);
		if(!isStatic) isStatic = tokens.isFutureConsume(TokenType.STATIC);
		Type type = Type.generate(tokens.next());
		if(tokens.isFutureConsume(TokenType.LEFT_BRACKET)) {
			tokens.junkOrThrow(TokenType.RIGHT_BRACKET, "Expected a ']' after a '" + type + "]'.");
			type = type.toArray();
		}
		String name = tokens.readIdentifier();
		ExpressionNode assignment = null;
		if(tokens.isFuture(TokenType.EQUAL)) {
			tokens.junk();
			assignment = ExpressionNode.generate(tokens);
		}
		tokens.junkOrThrow(TokenType.SEMICOLON, "expected semi-colon after field  declaration.");
		return new FieldNode(visibility, isFinal, isStatic, type, name, assignment, owner);
	}
	
	@Override
	public String toString() {
		return "Field{ "
				+ (isStatic?"STATIC, ":"")
				+ (type.isFinal()?"FINAL, ":"")
				+ "'" + name + "', "
				+ visibility + ", "
				+ type
				+ (hasAssignment() ? ", assignment=" + getAssignment() : "")
				+ " }";
	}
}
