package fr.compil.jej.nodes;

import fr.compil.jej.core.MethodSignature;
import fr.compil.jej.core.Typable;

import java.util.List;

/**
 * Workaround to allow the use of Interfaces, which contains MethodSignature and not implementations.
 */
public abstract class MethodsOwner extends Node {
	
	/**
	 * Finalement on ne sert plus de cette méthode, mais le projet est pour dans quelques jours. Donc bon ça marche bien
	 * tel quel (même si ça ne fait pas grand chose et que le temps passé à écrire ce commentaire est le triple du temps
	 * que j'aurais mis à fixer ça donc bon voilà hein).
	 */
	public boolean isFake() {return false;}
	
	/**
	 * Test if this MethodOwner is a sub-type of an other one.
	 * @param parent the MethodOwner to test the parent-link with
	 * @return true if the inheritance link exists.
	 */
	public abstract boolean isSubtype(MethodsOwner parent);
	
	/**
	 * Get a specific method signature. If no method is found, the method will search in all the parents.
	 * @param name the name of the method
	 * @param params the list of parameters or arguments
	 * @return null if no method corresponds to the name and the parameters.
	 */
	public abstract MethodSignature getMethod(String name, List<? extends Typable> params);
	
}
