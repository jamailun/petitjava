package fr.compil.jej;

import fr.compil.jej.core.MethodSignature;
import fr.compil.jej.core.PetitJavaEnvironment;
import fr.compil.jej.core.Utils;
import fr.compil.jej.errors.BadEnvironmentError;
import fr.compil.jej.nodes.ClassNode;
import fr.compil.jej.nodes.FieldNode;
import fr.compil.jej.nodes.InterfaceNode;
import fr.compil.jej.nodes.MethodNode;
import fr.compil.jej.core.TypesContext;
import fr.compil.jej.core.Type;
import fr.compil.jej.nodes.statements.BlockStatements;
import fr.compil.jej.nodes.statements.ReturnStatement;
import fr.compil.jej.nodes.statements.StatementNode;
import fr.compil.jej.nodes.statements.ThrowStatement;
import fr.compil.jej.std.FakeRuntimeException;
import fr.compil.jej.std.STD;
import fr.jamailun.jamlogger.JamLogger;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public final class TypeValidation {
	
	private final PetitJavaEnvironment env;
	
	private TypeValidation(PetitJavaEnvironment env) {
		this.env = env;
	}
	
	/**
	 * Check if there is exactly ONE main method.
	 */
	private boolean checkIfMainPresent() {
		List<MethodNode> mains = env.getClasses().stream()
				.flatMap(cl -> cl.getMethods().stream())
				.filter(MethodNode::isMainMethod)
				.collect(Collectors.toList());
		if(mains.size() == 1)
			return true;
		if (mains.size() > 1) {
			JamLogger.warning("Multiple definitions of a main() method. Only the first one will be selected.");
			return true;
		}
		JamLogger.error("No definition of a main() method. This cannot be compiled.");
		return false;
	}
	
	/**
	 * Validation des types des champs
	 */
	private boolean checkTypesOfFields(TypesContext context) {
		for(FieldNode field : context.getCurrentClass().getFields()) {
			if(field.getType() == Type.TYPE_VOID) {
				JamLogger.error("Field '" + context.getCurrentClass().getName() + "_" + field.getName() + "' cannot have a VOID type.");
				return false;
			}
			if(field.getType().shouldBeLinked()) {
				Type linkedType = context.getType(field.getType().getClassName());
				if(linkedType == null) {
					JamLogger.error("Unknown return type for method " + field.getName() + " : '" + field.getType().getClassName() + "'.");
					return false;
				}
				field.getType().linkClass(linkedType.getLinkedMethodsOwner());
			}
		}
		return true;
	}
	
	private boolean areThrowsInvalid(String methodName, List<StatementNode> statements, List<String> allowedThrows) {
		for(StatementNode statement : statements) {
			if(statement instanceof BlockStatements) {
				if(areThrowsInvalid(methodName, ((BlockStatements) statement).getStatements(), allowedThrows))
					return true;
			} else if(statement instanceof ThrowStatement) {
				// On ignore les vérifications pour les RuntimeException (et ses sous-types)
				if(((ThrowStatement) statement).getThrowedExpression().getType().getLinkedClass().isSubtype(FakeRuntimeException.FAKE_RUNTIME_EXCEPTION))
					continue;
				String throwName = ((ThrowStatement) statement).getThrowedExpression().getType().getClassName();
				if(!allowedThrows.contains(throwName)) {
					JamLogger.error("Method " + methodName + " contains not-catch exception named '" + throwName + "'.");
					return true;
				}
			}
		}
		return false;
	}
	
	private boolean checkAllThrows(TypesContext context) {
		// Also checks constructors
		for(MethodNode method : context.getCurrentClass().getAllMethods()) {
			if(!method.validateThrows(context)) {
				JamLogger.error("Bad THROWS for method " + method.getDisplayName() + ".");
				return false;
			}
			if(areThrowsInvalid(method.getDisplayName(), method.getStatements(), method.getThrowableList())) {
				JamLogger.error("Invalid THROWS for method " + method.getDisplayName() + ".");
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Validation of fields assignments
	 */
	private boolean checkFieldsAssignments(TypesContext context) {
		for(FieldNode field : context.getCurrentClass().getFields()) {
			if(field.hasAssignment()) {
				if(!field.getAssignment().validateType(context)) {
					JamLogger.error("Could not validate type of assignment of field '" + context.getCurrentClass().getName() + "_" + field.getName() + "'.");
					return false;
				}
				if(!field.getAssignment().getType().equals(field.getType())) {
					JamLogger.error("Type of field '" + context.getCurrentClass().getName() + "_" + field.getName()
							+ "' (" + field.getType() + ") does not correspond to his assignment "
							+ field.getAssignment() + " (" + field.getAssignment().getType() + ").");
					return false;
				}
			}
		}
		return true;
	}
	
	private boolean checkReturnTypesOfMethod(MethodNode method, TypesContext context) {
		if(!method.validateArguments(context)) {
			JamLogger.error("Could not validate arguments for method " + method.getDisplayName() + ".");
			return false;
		}
		// Link the return type of the current method to it's corresponding class value
		if(method.getReturnType().shouldBeLinked()) {
			Type linkedType = context.getType(method.getReturnType().getClassName());
			if(linkedType == null) {
				JamLogger.error("Unknown return type for method " + method.getDisplayName() + " : '" + method.getReturnType().getClassName() + "'.");
				return false;
			}
			method.getReturnType().linkClass(linkedType.getLinkedMethodsOwner());
		}
		return true;
	}
	
	private boolean checkTypesInMethod(MethodNode method, TypesContext context) {
		// Add method properties to the context (already inherited)
		method.getArguments().forEach(arg -> context.register(arg.getName(), arg.getType(), TypesContext.VariableContext.DECLARATION));
		method.getClassOwner().getFields()
				.stream()
				.filter(f -> f.isStatic() == method.isStatic())
				.forEach(context::registerField);

		// Apply validation for every statement of the method
		for(StatementNode s : method.getStatements()) {
			if (!s.validateTypes(context)) {
				//JamLogger.error("Could not validate types for statement\n\t\t" + s + "\n\t\tof method " + method.getDisplayName() + ".");
				return false;
			}
		}
		// Check the return statement's type of in the method
		AtomicInteger returnsCount = new AtomicInteger(0);
		if(areReturnsInvalid(method, method.getStatements(), returnsCount)) {
			JamLogger.error("Could not validate all RETURN statements for method " + method.getName() + ".");
			return false;
		}
		// Check if a non-void method has at least 1 RETURN statement.
		if(returnsCount.get() == 0 && method.getReturnType() != Type.TYPE_VOID) {
			JamLogger.error("No RETURN provided for method " + method.getName() + ".");
			return false;
		}
		return true;
	}
	
	private boolean areReturnsInvalid(MethodNode method, List<StatementNode> statementNodes, AtomicInteger count) {
		for(StatementNode s : statementNodes) {
			if(s instanceof BlockStatements) {
				if(areReturnsInvalid(method, ((BlockStatements) s).getStatements(), count))
					return true;
			} else if(s instanceof ReturnStatement) {
				ReturnStatement rs = (ReturnStatement) s;
				count.incrementAndGet();
				if(method.getReturnType() == Type.TYPE_VOID) {
					if(!rs.isEmpty()) {
						JamLogger.error("VOID method " + method.getName() + " has an non-empty RETURN statement.");
						return true;
					}
				} else {
					if(rs.isEmpty()) {
						JamLogger.error("Non-void method " + method.getName() + " is returning nothing.");
						return true;
					} else if(!rs.getReturnedType().equals(method.getReturnType())) {
						JamLogger.error("Method " + method.getName() + " should return " + method.getReturnType() + ", but can return " + rs.getReturnedType() + ".");
						JamLogger.error("method : " + method);
						return true;
					}
				}
			}
		}
		return false;
	}
	
	private TypesContext buildContext(ClassNode clazz) {
		TypesContext context = new TypesContext(clazz);
		STD.load(context);
		context.registerClasses(env.getClasses());
		context.registerInterfaces(env.getInterfaces());
		return context;
	}
	
	private boolean checkAssignments() {
		for(ClassNode classNode : env.getClasses()) {
			classNode.checkDefaultConstructor();
			// Create a context for the class
			TypesContext allContext = buildContext(classNode);
			// Validate all fields
			if(!checkTypesOfFields(allContext))
				return false;
			// Validate all RETURNS of methods
			for(MethodNode m : classNode.getAllMethods()) {
				if( ! checkReturnTypesOfMethod(m, allContext.inherit())) {
					return false;
				}
			}
		}
		// Re-iterate for field validation
		for(ClassNode classNode : env.getClasses()) {
			// Create a context for the class
			TypesContext allContext = buildContext(classNode);
			// Validate all fields
			if(!checkFieldsAssignments(allContext))
				return false;
			// Validate all methods
			for(MethodNode m : classNode.getAllMethods()) {
				if( ! checkTypesInMethod(m, allContext.inherit())) {
					return false;
				}
			}
			// Validate all THROWS signatures
			if(!checkAllThrows(allContext))
				return false;
		}
		return true;
	}
	
	private boolean checkClassesInheritance() {
		List<ClassNode> classes = new LinkedList<>(env.getClasses());
		classes.addAll(STD.FAKE_CLASSES);
		for(ClassNode clazz : classes) {
			if(clazz.shouldLinkParentClass()) {
				String parentName = clazz.getParentClassName();
				ClassNode parent = classes.stream().filter(c -> c.getName().equals(parentName)).findFirst().orElse(null);
				if(parent == null) {
					JamLogger.error("Class '" + clazz.getName() + "' extends class '" + parentName + "', but this class doesn't exist.");
					return false;
				}
				clazz.setParentClass(parent);
			}
		}
		return true;
	}
	
	private boolean checkInterfaceImplemented() {
		for(ClassNode clazz : env.getClasses()) {
			for(InterfaceNode in : clazz.getImplementedInterfaces()) {
				for(MethodSignature ms : in.getAllMethodsSignatures()) {
					if(clazz.getMethod(ms) == null) {
						JamLogger.error("Class " + clazz.getName() + " doesn't implement method " + ms + " of interface " + in.getName());
						return false;
					}
				}
			}
		}
		return true;
	}
	
	private boolean start() throws BadEnvironmentError {
		env.linkInterfaces();
		return Utils.garantieTrue(checkClassesInheritance(), "Error during classes inheritance check.") &&
						Utils.garantieTrue(checkIfMainPresent(), "Error during main-existance check.") &&
						Utils.garantieTrue(checkAssignments(), "Error during type assignements check.") &&
						Utils.garantieTrue(checkInterfaceImplemented(), "Error during interface-implementation check.");
	}
	
	public static boolean validate(PetitJavaEnvironment env) {
		try {
			boolean ret = new TypeValidation(env).start();
			if(ret)
				MethodSignature.CALCULATE_VTABLEIDS();
			return ret;
		} catch(BadEnvironmentError e) {
			JamLogger.error("Could not type-check environment : " + e.getMessage());
			return false;
		}
	}
	
}
