package fr.compil.jej;

import fr.compil.jej.core.PetitJavaEnvironment;
import fr.compil.jej.core.StreamTokens;
import fr.compil.jej.core.Token;
import fr.compil.jej.core.TokenType;
import fr.compil.jej.errors.BadEnvironmentError;
import fr.compil.jej.errors.BadTokenError;
import fr.compil.jej.errors.SyntaxError;
import fr.compil.jej.nodes.ClassNode;
import fr.compil.jej.nodes.InterfaceNode;
import fr.jamailun.jamlogger.JamLogger;
import java.util.List;
import java.util.Map;

import static java.util.Map.entry;

public final class Parser {
    
    private final static Map<String, String> IDENTIFIERS_TRANSFORMATIONS = Map.ofEntries(
            entry("struct", "class"),
            entry("const", "static")
    );
    
    private final StreamTokens tokens;
    
    public static PetitJavaEnvironment parse(List<Token> tokens) {
        StreamTokens stream = new StreamTokens(tokens);
        try {
            return new Parser(stream).start();
        } catch (BadTokenError e) {
            JamLogger.error("Error during parsing :");
            e.printStackTrace();
        } catch (SyntaxError e) {
            JamLogger.error("Syntax error during parsing : " + e.getMessage());
        } catch (BadEnvironmentError e) {
            JamLogger.error("Environment error during parsing : " + e.getMessage());
        }
        return new PetitJavaEnvironment(stream);
    }

    private Parser(StreamTokens tokens) {
        this.tokens = tokens;
    }
    
    private PetitJavaEnvironment start() throws BadTokenError, SyntaxError, BadEnvironmentError {
        // Replace all c-keyword from java to java-keywords to c.
        tokens.changeAll(IDENTIFIERS_TRANSFORMATIONS);
        
        PetitJavaEnvironment env = new PetitJavaEnvironment(tokens);
        // Un document petit-java est formé d'une suite de classes.
        // Donc tant qu'il y a quelque chose à lire, on essaye de le transformer en classe.
        while(tokens.hasNext()) {
            if(tokens.canFindTokenBeforeAnother(TokenType.CLASS, TokenType.INTERFACE)) {
                env.addClasse(ClassNode.generate(tokens));
            } else {
                env.addInterface(InterfaceNode.generate(tokens));
            }
        }
        return env;
    }

}
