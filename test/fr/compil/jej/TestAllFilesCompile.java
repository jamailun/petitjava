package fr.compil.jej;

import fr.compil.jej.core.PetitJavaEnvironment;
import fr.compil.jej.core.StreamChars;
import fr.compil.jej.core.Token;
import fr.jamailun.jamlogger.JamLogger;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class TestAllFilesCompile {
	
	private static boolean test(String file) {
		StreamChars chars = new StreamChars(new File("tests/" + file));
		JamLogger.info("Opened file '" + file + "'. Tokenization started.");
		
		List<Token> tokens = Tokenizer.tokenize(chars);
		if(tokens.isEmpty())
			return false;
		JamLogger.info("Tokenization is successful. Loaded " + tokens.size() + " tokens.");
		
		// PARSING
		JamLogger.log("Parsing started.");
		PetitJavaEnvironment env = Parser.parse(tokens);
		if(env.isEmpty())
			return false;
		JamLogger.info("Parsing is successful. Parsed " + env.size() + " classes.");
		
		// TYPE CHECKING
		JamLogger.log("Type validation started.");
		if(!TypeValidation.validate(env))
			return false;
		JamLogger.info("Type validation is successful.");

		// TRANSPILING
		JamLogger.log("Type validation started.");
		if(!TypeValidation.validate(env))
			return false;
		try {
			Transcompiler.transcompile(env, null, true);
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		JamLogger.info("Transcompilation is successful.");
		
		return true;
	}
	
	@Test
	public void testFile_Array() {
		Assertions.assertTrue(test("Array.pj"));
	}
	
	@Test
	public void testFile_Attributes() {
		Assertions.assertTrue(test("Attributes.pj"));
	}
	
	@Test
	public void testFile_BinarySearch() {
		Assertions.assertTrue(test("BinarySearch.pj"));
	}
	
	@Test
	public void testFile_BinaryTree() {
		Assertions.assertTrue(test("BinaryTree.pj"));
	}
	
	@Test
	public void testFile_BubbleSort() {
		Assertions.assertTrue(test("BubbleSort.pj"));
	}
	
	@Test
	public void testFile_Extends() {
		Assertions.assertTrue(test("Extends.pj"));
	}
	
	@Test
	public void testFile_Factorial() {
		Assertions.assertTrue(test("Factorial.pj"));
	}
	
	@Test
	public void testFile_LinearSearch() {
		Assertions.assertTrue(test("LinearSearch.pj"));
	}
	
	@Test
	public void testFile_LinkedList() {
		Assertions.assertTrue(test("LinkedList.pj"));
	}
	
	@Test
	public void testFile_QuickSort() {
		Assertions.assertTrue(test("QuickSort.pj"));
	}
	
	@Test
	public void testFile_Shape() {
		Assertions.assertTrue(test("Shape.pj"));
	}
	
	@Test
	public void testFile_TreeVisitor() {
		Assertions.assertTrue(test("TreeVisitor.pj"));
	}
	
}
