# PetitJava

Un projet de compilation, dans le cadre de notre 4ème année à l'INSA de Rennes, en 2022.

Le but est de transformer un langage proche du Java *(c'est à dire avec des fonctionnalités en moins)* en un programme
strictement équivalent d'un point de vue comportemental en C.

## Auteurs

- Timothé Rosaz,
- Alexandre Sabbadin.

## Utilisation

Une fois les sources compilées, exécutez-le avec la commande suivante :
```bash
java -jar <jarExecutable.jar> source.pj -o sortie.c
# Puis, une fois le fichier transpilé, vous pouvez simplement le compiler en code machine
gcc sortie.c -o executable.o
```

Si vous utilisez - pour une raison que j'ignore - ce projet dans le vôtre, merci d'indiquer l'origine du code quelque part !

## À propos du langage

Dans les grandes lignes, voici les points manquant dans notre projet :
- Pas de classe abstraites ou de `default` dans une interface,
- Pas de classe ou structure anonyme,
- le garbage collector n'est pas implémenté,
- pas de type primitifs `float`, `long`, `short` ou `byte`.

Voici, en revanche, les éléments présents :
- Système de classes, avec héritage et implémentation d'interfaces.
- Système de constructeur par défaut et de méthode avec plusieurs signatures.
- Un système de visibilité basique. Mais comme tout est dans un seul fichier, seul le "private" a un sens.
- Tableaux (à N dimensions) pouvant stocker des nombres comme des objets quelconques.
- Manipulation et concaténation de chaînes de caractères.
- Système de gestion et capture d'erreurs. Pas de stacktrace de prévue cependant.
- STDIN basique, avec `hasNextLine()`,`nextLine()`,`hasNextInt()`,`nextInt()`, et sans Scanner, le tout sur `System.in`.

## Fonctionnement

Pour effectuer une transpilation, PetitJava fonctionne en quatre temps :
1) Tokenization,
2) Analyse syntaxique *(ou "Parsing")*,
3) Validation des types,
4) Transpilation

Voici une explication technique plus ou moins détaillée de chacune des étapes.

### 1. La tokenization

Le fichier à transpiler est dans un premier temps converti en un flux de caractères.
Ce dernier va être parcouru caractère par caractère pour former des "tokens".
Ainsi, tous les littéraux sont stockés dans un certain token, tous les mots clefs ont un token correspondant,
les commentaires sont ignorés et toute suite de caractère non reconnue est considérée comme un "identifiant".
En pratique, cela peut correspondre à un nom de classe ou de variable par exemple.

Pour voir la liste des tokens possibles, il faut voir l'énumération [TokenType](/src/fr/compil/jej/core/TokenType.java).
Le processus expliqué ci-dessus est accompli par la classe "statique" du [Tokenizer](/src/fr/compil/jej/Tokenizer.java).


### 2. L'analyse syntaxique

Cette étape est potentiellement la plus difficile. Elle consiste à produire un arbre de [classes](/src/fr/compil/jej/nodes/ClassNode.java) à
partir du flux de Token créé précédemment.
Chacune d'entre elle contient des [champs](/src/fr/compil/jej/nodes/FieldNode.java) et des [méthodes](/src/fr/compil/jej/nodes/MethodNode.java).
Ce sont les méthodes qui vont contenir des [statements](/src/fr/compil/jej/nodes/statements/StatementNode.java). Ces derniers représentent
une "ligne" de code comme une assignation, une déclaration, un `if` ou une quelconque structure du code.
Dans ces statements, on retrouve des [expressions](/src/fr/compil/jej/nodes/expressions/ExpressionNode.java).
Il s'agit une brique du code, comme une référence à un objet, un appel à une méthode ou autre. À cette étape-là du programme, on ne se préoccupe pas de mla cohérence des types.
La cohérence syntaxique suffit. Par exemple, une condition doit être de la forme `if(<EXPRESSION>) <CONTENU> [else <STATEMENT>]`.

Tout ce processus est dirigé par la classe "statique" [Parser](/src/fr/compil/jej/Parser.java), mais en général la création d'une certaine instance
est laissée à la classe en question (par des méthodes statiques `$generate()`).
Certains cas particuliers comme le traitement des expressions mathématiques ou logiques sont traitées différemment du reste.
Et notons que la différence entre la concaténation de Strings et les calculs sont effectuées dans l'étape suivante.

### 3. Validation de type

Le Java est, pour notre plus grand plaisir, statiquement et explicitement typé. Ainsi, les variables dans un statement n'ont que 3 sources possibles :
un champ associé à la classe ou à l'instance, un paramètre de la méthode en question ou une variable déclarée précédemment dans la méthode. Dans tous les
cas, le type est spécifié avant le nom, donc savoir de quel type est une variable et vérifier la cohérence entre les statement dans l'ordre du code est aisé.

C'est aussi durant cette étape que l'on cherche une méthode main, que l'on vérifie les retours des méthodes et que l'on vérifie les propriétés héritées.

Chaque nœud de l'arbre précédemment établit possède une méthode `#validateType(TypeContext)`, ce qui permet des appels récursifs au sein de la structure.
La classe dirigeant ces évènements, comme dans les cas précédents, le fait depuis sa propre classe [TypeValidation](/src/fr/compil/jej/TypeValidation.java).

### 4. Transpilation

Maintenant que notre arbre a été vérifié comme étant valide du point de vue typage et syntaxique, il faut le transformer en chaines de caractères.
Pour ce faire, nous avons implémenté le patron de conception du [visiteur](https://fr.wikipedia.org/wiki/Visiteur_(patron_de_conception)).
Cela nous semblait être la meilleure solution dans un contexte de programmation objet. Ainsi, tout le code concernant cette transpilation
se situe dans la classe [VisiteurC](/src/fr/compil/jej/compilers/VisiteurC.java) qui implémente l'interface [Visiteur](/src/fr/compil/jej/compilers/Visiteur.java).

### Notes additionnelles

Le but étant de concevoir ce projet comme étant un projet de long terme, nous avons mis en place des systèmes assez généraux
comme toute la [fake STD](/src/fr/compil/jej/std) ou un début de ["système d'imports"](/src/fr/compil/jej/compilers/NativeHandler.java).

Pour le système d'erreur, je me suis aidé de [Wikipédia](https://en.wikipedia.org/wiki/Setjmp.h) tout simplement. J'ai ensuite rajouté
quelques `define` pour éviter d'avoir du code impossible a débugger. Notons tout de même que ce rajout est à la
limite de ce qui est acceptable d'un point de vue code. 
